import { ContactViewAddedEventHandler } from './contact-view-added.event-handler';
import { ContactViewDeletedByQueryEventHandler } from './contact-view-deleted-by-query.event-handler';
import { ContactViewDeletedEventHandler } from './contact-view-deleted.event-handler';
import { ContactViewEditedEventHandler } from './contact-view-edited.event-handler';
import { UserAddedEventHandler } from './user-added.event-handler';
import { UserDeletedEventHandler } from './user-deleted.event-handler';

export const ContactViewsEventHandlers = [
  ContactViewAddedEventHandler,
  ContactViewEditedEventHandler,
  ContactViewDeletedByQueryEventHandler,
  ContactViewDeletedEventHandler,
  UserDeletedEventHandler,
  UserAddedEventHandler,
];
