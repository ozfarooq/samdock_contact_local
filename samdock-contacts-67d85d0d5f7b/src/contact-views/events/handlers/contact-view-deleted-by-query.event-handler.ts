import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';
import { ContactViewsDeletedByQueryEvent } from '@daypaio/domain-events/contact-views';

@EventsHandler(ContactViewsDeletedByQueryEvent)
export class ContactViewDeletedByQueryEventHandler
  implements IEventHandler<ContactViewsDeletedByQueryEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: ContactViewsRepository) {}
  async handle(event: ContactViewsDeletedByQueryEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { query, meta } = event;
    try {
      const result = await this.repository.deleteByQuery(query, meta);
      return result;
    } catch (error) {
      this.logger.error(`Cannot delete contact views by query ${query}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event possibly
    }
  }
}
