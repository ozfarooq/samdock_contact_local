import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';
import { ContactViewEditedEvent } from '@daypaio/domain-events/contact-views';

@EventsHandler(ContactViewEditedEvent)
export class ContactViewEditedEventHandler
  implements IEventHandler<ContactViewEditedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: ContactViewsRepository) { }

  async handle(event: ContactViewEditedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;

    try {
      await this.repository.edit(_id, data, meta);
    } catch (error) {
      this.logger.error(`Failed to edit inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
