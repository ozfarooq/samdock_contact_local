import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserDeletedEvent } from '@daypaio/domain-events/users';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ContactViewsService } from '../../services/contact-views.service';

@EventsHandler(UserDeletedEvent)
export class UserDeletedEventHandler
  implements IEventHandler<UserDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private contactViewsService: ContactViewsService,
  ) { }
  async handle(event: UserDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.contactViewsService.deleteByQuery({ createdBy: _id, isShared: false }, meta);
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
