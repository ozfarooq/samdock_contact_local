import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';
import { ContactViewAddedEvent } from '@daypaio/domain-events/contact-views';

@EventsHandler(ContactViewAddedEvent)
export class ContactViewAddedEventHandler
  implements IEventHandler<ContactViewAddedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: ContactViewsRepository) { }

  async handle(event: ContactViewAddedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;

    try {
      await this.repository.add(
        { ...data, createdAt: meta.timestamp },
        meta,
      );
    } catch (error) {
      this.logger.error(`Failed to create contact view of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
