import { Injectable, Logger } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { AddEditContactViewDTO, ContactViewDTO, ContactViewEntityEnum, DEFAULT_VIEW } from '../dtos/contact-view.dto';
import { BrowseContactViewsQuery } from '../queries/impl/browse-contact-views.query';
import { EditContactViewCommand } from '../commands/impl/edit-contact-view.command';
import { AddContactViewCommand } from '../commands/impl/add-contact-view.command';
import { DeleteContactViewCommand } from '../commands/impl/delete-contact-view.command';
import { BreadService } from '../../shared/services/bread.service';
import { ReadContactViewQuery } from '../queries/impl/read-contact-views.query';
import { DeleteContactViewsByQueryCommand } from '../commands/impl/delete-contact-views-by-query.command';
import { v4 } from 'uuid';
import { IUser } from '@daypaio/domain-events/users';

const FALLBACK_LANGUAGE = 'en';
const KNOWN_LANGUEGES = ['en', 'de'];
const DEFAULT_VIEW_NAMES = {
  en: {
    person: 'Persons',
    organization: 'Organizations',
  },
  de: {
    person: 'Personen',
    organization: 'Unternehmen',
  },
};

@Injectable()
export class ContactViewsService extends BreadService<ContactViewDTO> {
  protected logger: Logger;

  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<ContactViewDTO[]> {
    return await this.executeQuery(new BrowseContactViewsQuery(meta));
  }

  async edit(
    id: string,
    data: Partial<AddEditContactViewDTO>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditContactViewCommand(id, data, meta),
    );
  }

  async read(id: string, meta: EventMetaData): Promise<ContactViewDTO> {
    return await this.executeQuery(new ReadContactViewQuery(id, meta));
  }

  async add(data: AddEditContactViewDTO, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddContactViewCommand(data._id, data, meta));
  }

  async delete(
    id: string,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(new DeleteContactViewCommand(id, meta));
  }

  async deleteByQuery(query: any, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteContactViewsByQueryCommand(query, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    return this.deleteByQuery({ _tenantID: meta._tenantID }, meta);
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeleteContactViewCommand(id, meta));
    }));
  }

  async addDefaults(user: IUser, meta: EventMetaData): Promise<void> {
    let language = FALLBACK_LANGUAGE;
    if (user?.language && KNOWN_LANGUEGES.includes(user?.language)) {
      language = user?.language;
    } else if (user?.initialBrowserLanguage &&
      KNOWN_LANGUEGES.includes(user?.initialBrowserLanguage)) {
      language = user?.initialBrowserLanguage;
    }
    this.logger.log(`Creating default views with "${language}" language`);
    console.table(user);
    const personsView =
      this.createDefaultViewFor(ContactViewEntityEnum.Person, language, meta);
    await this.executeCommand(new AddContactViewCommand(
      personsView._id,
      personsView,
      meta,
    ));
    const organizationsView =
      this.createDefaultViewFor(ContactViewEntityEnum.Organization, language, meta);
    await this.executeCommand(new AddContactViewCommand(
      organizationsView._id,
      organizationsView,
      meta,
    ));
  }

  createDefaultViewFor(entity: ContactViewEntityEnum, lang: string, meta: EventMetaData) {
    const view = { ...DEFAULT_VIEW };
    view._id = v4();
    view.name = DEFAULT_VIEW_NAMES[lang][entity];
    view.entity = entity;
    view.isShared = false;
    view.createdAt = Date.now();
    view.createdBy =  meta._userID;
    view._tenantID =  meta._tenantID;
    return view;
  }
}
