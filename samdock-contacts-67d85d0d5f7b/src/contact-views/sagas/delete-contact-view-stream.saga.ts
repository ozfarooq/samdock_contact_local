import { ContactViewDeletedEvent } from '../events/impl';
import { Injectable } from '@nestjs/common';
import { DeleteStreamSaga } from '../../shared/sagas/delete-stream.saga';

@Injectable()
export class ContactViewStreamSaga extends DeleteStreamSaga {
  constructor() {
    super([ContactViewDeletedEvent], 'contact_views');
  }
}
