import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadContactViewQuery } from '../impl/read-contact-views.query';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';
import { ContactView } from '../../models/contact-view.model';

@QueryHandler(ReadContactViewQuery)
export class ReadContactViewsQueryHandler implements IQueryHandler<ReadContactViewQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: ContactViewsRepository) { }

  async execute(query: ReadContactViewQuery): Promise<ContactView> {
    this.logger.log('Query triggered');
    const { id, meta } = query;
    try {
      return this.repo.read(id, meta);
    } catch (error) {
      this.logger.error('Failed to read on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
