import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseContactViewsQuery } from '../impl/browse-contact-views.query';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';
import { ContactView } from '../../models/contact-view.model';

@QueryHandler(BrowseContactViewsQuery)
export class BrowseContactViewsQueryHandler implements IQueryHandler<BrowseContactViewsQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: ContactViewsRepository) { }

  async execute(query: BrowseContactViewsQuery): Promise<ContactView[]> {
    this.logger.log('Query triggered');
    const { meta } = query;
    try {
      return await this.repo.browse(meta);
    } catch (error) {
      this.logger.error('Failed to browse on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
