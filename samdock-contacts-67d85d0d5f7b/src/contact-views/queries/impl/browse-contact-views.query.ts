import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class BrowseContactViewsQuery implements IQuery {
  constructor(public meta: EventMetaData) {}
}
