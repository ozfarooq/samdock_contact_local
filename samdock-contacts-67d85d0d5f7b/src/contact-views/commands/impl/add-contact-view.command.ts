import { IContactView } from '@daypaio/domain-events/contact-views';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class AddContactViewCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly contactView: IContactView,
    public readonly meta: EventMetaData,
  ) { }
}
