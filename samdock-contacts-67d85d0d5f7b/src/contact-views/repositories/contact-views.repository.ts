import { ContactView } from '../models/contact-view.model';
import { BreadRepository } from '../../shared/bread.respository';
import { InjectModel } from 'nestjs-typegoose';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ReturnModelType } from '@typegoose/typegoose';
import {
  InternalServerErrorException,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { DeleteStreamCommand } from '../../shared/commands/impl/delete-stream.command';
import { CommandBus } from '@nestjs/cqrs';

export class ContactViewsRepository extends BreadRepository<ContactView>{
  constructor(
    @InjectModel(ContactView) contactViewModel: ReturnModelType<typeof ContactView>,
    private commandBus: CommandBus,
  ) {
    super(contactViewModel);
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE');
    data._tenantID = meta._tenantID;
    data.createdBy = meta._userID;
    data.createdAt = meta.timestamp;
    console.table(data);
    try {
      await this.model.create(data);
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browse(meta: EventMetaData): Promise<ContactView[]> {
    this.logger.verbose('FIND');
    try {
      return await this.model.find(
          { _tenantID: meta._tenantID, $or: [{ isShared: true }, { createdBy: meta._userID }] },
        );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async edit(id: string, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    data.updatedAt = meta.timestamp;
    data.updatedBy = meta._userID;
    console.table({ data, meta, _id: id });
    try {
      const item = await this.getItem(id, meta);
      const isShared = item.isShared;
      const isLocked = item.isLocked;
      const isCreator = item.createdBy === meta._userID;
      const isAdmin = meta?.isAdmin;

      const isEditingNotAllowedForLockedView =
        isShared &&
        isLocked &&
        !isAdmin &&
        !isCreator;
      // Explanation: https://samdock.atlassian.net/browse/SAM-1073?focusedCommentId=18930
      // and
      // https://samdock.atlassian.net/browse/SAM-1139
      if (!isShared && !isCreator) {
        throw new ForbiddenException(`User ${meta._userID} cannot edit private contact view id:${id} created by ${item.createdBy}`);
      }
      if (isEditingNotAllowedForLockedView) {
        throw new ForbiddenException(`User ${meta._userID} cannot edit public contact view id:${id} created by ${item.createdBy}`);
      }
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: id },
        { $set: data },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, data);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    this.logger.verbose('DELETE');
    console.table({ id, meta });
    try {
      const item = await this.getItem(id, meta);
      const isShared = item.isShared;
      const isLocked = item.isLocked;
      const isCreator = item.createdBy === meta._userID;
      const isAdmin = meta?.isAdmin;

      const isDeletingNotAllowedForLockedView =
        isShared &&
        isLocked &&
        !isAdmin &&
        !isCreator;
      // Explanation: https://samdock.atlassian.net/browse/SAM-1073?focusedCommentId=18930
      // and
      // https://samdock.atlassian.net/browse/SAM-1139
      if (!isShared && !isCreator) {
        throw new ForbiddenException(`User ${meta._userID} cannot remove private contact view id:${id} created by ${item.createdBy}`);
      }
      if (isDeletingNotAllowedForLockedView) {
        throw new ForbiddenException(`User ${meta._userID} cannot remove public contact view id:${id} created by ${item.createdBy}`);
      }
      const result = await this.model.deleteOne({
        _tenantID: meta._tenantID,
        _id: id,
      });
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${this.model.modelName} was found with id: ${id} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'delete', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async deleteByQuery(query, meta: EventMetaData): Promise<void> {
    this.logger.verbose('DELETE BY QUERY');
    console.table({ query });
    try {
      let ids = await this.model
      .find({ ...query, _tenantID: meta._tenantID })
      .select('_id').lean().exec();

      if (!ids.length) {
        throw new NotFoundException();
      }

      ids = ids.map(item => item._id);
      const result = await this.model.deleteMany({
        _tenantID: meta._tenantID,
        _id: { $in: ids },
      });
      ids.forEach(id => this.commandBus.execute(new DeleteStreamCommand(`contact_views-${id}`)));
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        return;
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${this.model.modelName} was found with query: ${query} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'deleteByQuery');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async getItem(id: string, meta: EventMetaData): Promise<ContactView> {
    const item = await this.model.findOne({ _tenantID: meta._tenantID, _id: id });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }
}
