import { modelOptions, prop } from '@typegoose/typegoose';
import { IContactView, ContactViewEntity } from '@daypaio/domain-events/contact-views';
import { v4 } from 'uuid';
import { ContactViewEntityEnum } from '../dtos/contact-view.dto';

@modelOptions({ schemaOptions: { collection: 'contact-views' } })
export class ContactView implements IContactView {

  @prop({ required: true, unique: true, default: v4() })
  _id: string;

  @prop({ required: true })
  _tenantID: string;

  @prop({ required: true })
  name: string;

  @prop({ required: true, enum: ContactViewEntityEnum })
  entity: ContactViewEntity;

  @prop({ required: true })
  isShared: boolean;

  @prop()
  isLocked?: boolean;

  @prop()
  filters?: any;

  @prop()
  sort?: any;

  @prop()
  columns?: any;

  @prop({ required: true })
  createdAt: number;

  @prop({ required: true })
  createdBy: string;

  @prop()
  updatedAt: number;

  @prop()
  updatedBy: string;
}
