import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { v4 } from 'uuid';
import {
  ContactViewEntity,
  IContactView,
} from '@daypaio/domain-events/contact-views';
import { ContactView } from '../models/contact-view.model';

export enum ContactViewEntityEnum {
  Person = 'person',
  Organization = 'organization',
  All = 'all',
}

export const DEFAULT_VIEW: ContactView = {
  _id: undefined,
  name: undefined,
  entity: undefined,
  isShared: false,
  columns: [
    {
      key: 'stage',
      entity: 'all',
      visible: true,
    },
    {
      key: 'won',
      entity: 'all',
      visible: true,
    },
    {
      key: 'lost',
      entity: 'all',
      visible: true,
    },
    {
      key: 'tasks',
      entity: 'all',
      visible: true,
    },
    {
      key: 'status',
      entity: 'all',
      visible: true,
    },
  ],
  _tenantID: undefined,
  createdBy: undefined,
  createdAt: undefined,
  updatedAt: undefined,
  updatedBy: undefined,
};

export class AddEditContactViewDTO implements IContactView {
  @ApiProperty({
    description: 'The id of the contact view',
    required: true,
    default: v4(),
  })
  _id: string;

  @ApiProperty({
    description: 'The name of the contact view',
    required: true,
  })
  name: string;

  @ApiProperty({
    description: 'The entity of the contact view',
    required: true,
    enum: ContactViewEntityEnum,
  })
  entity: ContactViewEntity;

  @ApiPropertyOptional({
    description: 'Is this contact view shared',
    required: true,
  })
  isShared: boolean;

  @ApiPropertyOptional({
    description: 'Is this contact view locked',
  })
  isLocked: boolean;

  @ApiProperty({
    description: 'Contact view filters',
  })
  filters?: any;

  @ApiProperty({
    description: 'Contact view columns',
  })
  columns?: any;
}

export class ContactViewDTO extends AddEditContactViewDTO {

  @ApiProperty({
    description: 'The id of the tenant',
  })
  _tenantID: string;

  @ApiProperty({
    description: 'Timestamp when contact view was created',
  })
  createdAt: number;

  @ApiProperty({
    description: 'The id of user who created contact view',
  })
  createdBy: string;

  @ApiProperty({
    description: 'Timestamp when contact view was updated last time',
  })
  updatedAt: number;

  @ApiProperty({
    description: 'The id of user who updated contact view last time',
  })
  updatedBy: string;
}
