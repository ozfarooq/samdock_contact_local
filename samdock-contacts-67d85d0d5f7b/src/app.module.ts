import { Module } from '@nestjs/common';
import { PersonsModule } from './persons/persons.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { OrganizationsModule } from './organizations/organizations.module';
import { HealthcheckModule } from './shared/healthcheck/healthcheck.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
import { DeleteStreamHandler } from './shared/commands/handlers/delete-stream.command-handler';
import { EventStoreCqrsModule } from 'nestjs-eventstore';
import { eventStoreBusConfig } from './event-bus.provider';
import { DraftsModule } from './drafts/drafts.module';
import { InboundSourcesModule } from './inbound-sources/inbound-sources.module';
import { ImportModule } from './import/import.module';
import { CoreModule } from './core/core.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PersonsService } from './persons/services/persons.service';
import { OrganizationsService } from './organizations/services/organizations.service';
import { InboundSourcesService } from './inbound-sources/services/inbound-sources.service';
import { DraftsService } from './drafts/services/drafts.service';
import { CustomFieldsModule } from './custom-fields/custom-fields.module';
import { ContactViewsModule } from './contact-views/contact-views.module';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    TypegooseModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({
        uri: config.get('database.mongoConnectionString'),
      }),
      inject: [ConfigService],
    }),
    EventStoreCqrsModule.forRootAsync(
      {
        useFactory: async (config: ConfigService) => {
          return {
            connectionSettings: config.get('eventstore.connectionSettings'),
            endpoint: config.get('eventstore.tcpEndpoint'),
          };
        },
        inject: [ConfigService],
      },
      eventStoreBusConfig,
    ),
    MailerModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        return config.get('nodemailer');
      },
      inject: [ConfigService],
    }),
    PersonsModule,
    OrganizationsModule,
    HealthcheckModule,
    DeleteStreamHandler,
    PersonsService,
    OrganizationsService,
    InboundSourcesService,
    DraftsService,
    InboundSourcesModule,
    DraftsModule,
    ImportModule,
    CoreModule,
    CustomFieldsModule,
    ContactViewsModule,
  ],
})
export class AppModule {}
