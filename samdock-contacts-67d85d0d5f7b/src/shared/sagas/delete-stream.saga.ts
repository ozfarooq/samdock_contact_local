import { Type } from '@nestjs/common';
import { Saga, ICommand, IEvent, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { DeleteStreamCommand } from '../commands/impl/delete-stream.command';

export class DeleteStreamSaga {
  constructor(private eventTypes: Type<IEvent>[], private streamPrefix: string) {}

  @Saga()
  entityDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      filter(event => this.eventTypes.some(eventInst => event instanceof eventInst)),
      map(
        (event: any) =>
          new DeleteStreamCommand(`${this.streamPrefix}-${event._id}`),
      ),
    );
  }
}
