import { Injectable, Logger } from '@nestjs/common';
import * as XLSX from 'xlsx';

const TEMPLATE_TEXT = {
  en: ['Explanation: ', 'Please do not change any data here!\n\nThese are all companies that you created in Samdock.\nIf any company is missing, go back to Samdock and create the missing ones.'],
  de: ['Erklärung: ', 'Bitte ändere hier keine Daten!\n\nDies sind alle Unternehmen, die du in Samdock erstellt hast. Wenn ein Unternehmen fehlt, gehe zurück und lege das fehlende Unternehmen an.'],
};

@Injectable()
export class XLSXService {
  protected logger = new Logger(this.constructor.name);

  public parseFileData(fileData: string) {
    const array = this.fileDataToArray(fileData);
    return this.arrayToObjects(array);
  }

  public getFileBuffer(objects, tableName) {
    const array = this.objectsToArray(objects);
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(array);
    XLSX.utils.book_append_sheet(wb, ws, tableName);
    const buffer = XLSX.write(wb, { type:'buffer', bookType: 'xlsx' });
    return buffer;
  }

  public getOrganizationsFileBuffer(customFields: string[], fileData: Buffer) {
    const wb: XLSX.WorkBook = XLSX.read(fileData, { type: 'buffer', cellStyles: true });
    const fileArray: any[] = XLSX.utils.sheet_to_json(
      wb.Sheets[wb.SheetNames[0]],
      { header: 1 },
    );

    const organizationsSheet = wb.Sheets[wb.SheetNames[0]];

    // insert custom fields headers
    customFields.forEach((field, index) => {
      const cellRef = XLSX.utils.encode_cell({ r:0, c:12 + index });
      const cell = { v: `customfield.${field}` };
      organizationsSheet[cellRef] = cell;
    });

    // replace wrong symbols
    const cellRef1 = XLSX.utils.encode_cell({ r:1, c:0 });
    organizationsSheet[cellRef1] = { v:organizationsSheet[cellRef1].v.replace(/\r/g, '') };
    const cellRef2 = XLSX.utils.encode_cell({ r:17, c:0 });
    organizationsSheet[cellRef2] = { v:organizationsSheet[cellRef2].v.replace(/\r/g, '') };

    // set right sheet range
    const range = { s:{ r: 0, c: 0 }, e: { r: 20, c: 11 + customFields.length } };
    organizationsSheet['!ref'] = XLSX.utils.encode_range(range);

    // set width of columns
    organizationsSheet['!cols'] = Array(fileArray[0].length + customFields.length).fill({ wch:20 });
    organizationsSheet['!cols'][0] = { wch: 42 };

    const newWb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(newWb, organizationsSheet, wb.SheetNames[0]);

    const buffer = XLSX.write(newWb, { type:'buffer', bookType: 'xlsx', cellStyles: true });
    return buffer;
  }

  public getPersonsFileBuffer(
    organizations: any[],
    fileData: Buffer,
    customFields: string[],
    lang: string,
  ) {
    const array = this.objectsToArray(organizations);
    array.forEach((item, index) => {
      if (index === 0) {
        array[index].unshift(TEMPLATE_TEXT[lang][0]);
      } else if (index === 1) {
        array[index].unshift(TEMPLATE_TEXT[lang][1]);
      } else {
        array[index].unshift('');
      }
    });
    const wb: XLSX.WorkBook = XLSX.read(fileData, { type: 'buffer', cellStyles: true });

    const personsArray: any[] = XLSX.utils.sheet_to_json(
      wb.Sheets[wb.SheetNames[0]],
      { header: 1 },
    );

    const personsSheet = wb.Sheets[wb.SheetNames[0]];

    // insert custom fields headers
    customFields.forEach((field, index) => {
      const cellRef = XLSX.utils.encode_cell({ r:0, c:17 + index });
      const cell = { v: `customfield.${field}` };
      personsSheet[cellRef] = cell;
    });

    // replace wrong symbols
    const cellRef = XLSX.utils.encode_cell({ r:1, c:0 });
    personsSheet[cellRef] = { v:personsSheet[cellRef].v.replace(/\r/g, '') };

    // set right table range
    const range = { s:{ r: 0, c: 0 }, e: { r: 30, c: 16 + customFields.length } };
    personsSheet['!ref'] = XLSX.utils.encode_range(range);

    // set columns width
    personsSheet['!cols'] = Array(personsArray[0].length + customFields.length).fill({ wch:20 });
    personsSheet['!cols'][0] = { wch: 40 };

    const organizationsSheet = XLSX.utils.aoa_to_sheet(array);
    organizationsSheet['!cols'] = Array(array[0].length).fill({ wch:40 });

    const newWb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(newWb, personsSheet, wb.SheetNames[0]);
    XLSX.utils.book_append_sheet(newWb, organizationsSheet, wb.SheetNames[1]);

    const buffer = XLSX.write(newWb, { type:'buffer', bookType: 'xlsx', cellStyles: true });
    return buffer;
  }

  /**
   * Parse file data to array of columns
   * @param {string} fileData
   * @return {array} of fields with type [[col, col],[col, col]]
   */
  private fileDataToArray(fileData) {
    const wb: XLSX.WorkBook = XLSX.read(fileData, { type: 'buffer' });

    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    let arrayResult: any[] = (XLSX.utils.sheet_to_json(ws, { header: 1 }));

    arrayResult = arrayResult.filter(row => row.length); // remove empty rows

    return arrayResult;
  }

  /**
   * Parse file data to array of columns
   * @param {array} of fields with type [[key, key],[value, value],[value, value]]
   * @return {array} of objects
   */
  private arrayToObjects(array) {
    const keys = array.splice(0, 1)[0];
    const result = [];
    for (let itemIndex = 0; itemIndex < array.length; itemIndex += 1) {
      const object = {};
      for (let keyIndex = 0; keyIndex < keys.length; keyIndex += 1) {
        if (array[itemIndex][keyIndex] !== '') object[keys[keyIndex]] = array[itemIndex][keyIndex];
      }
      result.push(object);
    }
    return result;
  }

  /**
   * Parse file data to array of columns
   * @param {array} of objects
   * @return {array} of fields with type [[key, key],[value, value],[value, value]]
   */
  private objectsToArray(objects) {
    const result = [];
    if (!objects[0]) {
      result.push([]);
    } else {
      result.push(Object.keys(objects[0]));
    }
    for (let itemIndex = 0; itemIndex < objects.length; itemIndex += 1) {
      result.push([]);
      const keys = Object.keys(objects[itemIndex]);
      for (let keyIndex = 0; keyIndex < keys.length; keyIndex += 1) {
        let column = result[0].indexOf(keys[keyIndex]);
        if (column === -1) {
          result[0].push(keys[keyIndex]);
          column = result[0].length - 1;
        }
        result[itemIndex + 1][column] = objects[itemIndex][keys[keyIndex]];
      }
    }
    return result;
  }
}
