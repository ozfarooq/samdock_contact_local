import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteStreamCommand } from '../impl/delete-stream.command';
import { Logger } from '@nestjs/common';
import { EventStore } from 'nestjs-eventstore';

@CommandHandler(DeleteStreamCommand)
export class DeleteStreamHandler
  implements ICommandHandler<DeleteStreamCommand> {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(private readonly eventStore: EventStore) {}

  async execute(command: DeleteStreamCommand) {
    const { streamName } = command;
    this.logger.log(`Deleting stream ${streamName}`);
    await this.eventStore.connection.deleteStream(streamName, -2);
  }
}
