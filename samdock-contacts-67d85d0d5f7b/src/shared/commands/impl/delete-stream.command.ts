export class DeleteStreamCommand {
  constructor(public readonly streamName: string) {}
}
