import { InboundSourceDeletedEvent } from '../events/impl';
import { Injectable } from '@nestjs/common';
import { DeleteStreamSaga } from '../../shared/sagas/delete-stream.saga';

@Injectable()
export class InboundSourceStreamSaga extends DeleteStreamSaga {
  constructor() {
    super([InboundSourceDeletedEvent], 'inbound_sources');
  }
}
