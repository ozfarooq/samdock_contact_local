import { Module } from '@nestjs/common';
import { InboundSourcesController } from './controllers/inbound-sources.controller';
import { InboundSourcesService } from './services/inbound-sources.service';
import { InboundSourcesCommandHandlers } from './commands/handlers';
import { InboundSourcesQueryHandlers } from './queries/handlers';
import { TypegooseModule } from 'nestjs-typegoose';
import { InboundSource } from './models/inbound-source.model';
import { InboundSourcesRepository } from './repositories/inbound-sources.repository';
import { InboundSourcesEventHandlers } from './events/handlers';
import { InboundSourceStreamSaga } from './sagas/delete-inbound-source-stream.saga';

@Module({
  imports: [
    TypegooseModule.forFeature([InboundSource]),
  ],
  controllers: [
    InboundSourcesController,
  ],
  providers: [
    InboundSourcesService,
    InboundSourcesRepository,
    ...InboundSourcesCommandHandlers,
    ...InboundSourcesQueryHandlers,
    ...InboundSourcesEventHandlers,
    InboundSourceStreamSaga,
  ],
  exports: [
    InboundSourcesRepository,
    InboundSourcesService,
  ],
})
export class InboundSourcesModule { }
