import { IInboundSource } from '@daypaio/domain-events/inbound-sources';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class AddInboundSourceCommand implements ICommand {
  constructor(
    public readonly inboundSource: IInboundSource,
    public readonly meta: EventMetaData,
  ) { }
}
