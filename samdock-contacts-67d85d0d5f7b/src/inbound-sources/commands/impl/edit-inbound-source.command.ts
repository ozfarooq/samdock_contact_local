import { ICommand } from '@nestjs/cqrs';
import { IInboundSource } from '@daypaio/domain-events/inbound-sources';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditInboudSourceCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly inboundSource: Partial<IInboundSource>,
    public readonly meta: EventMetaData,
  ) { }
}
