import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DeleteInboundSourceCommand } from '../impl/delete-inbound-source.command';
import { InboundSourceAggregate } from '../../models/inbound-source.aggregate';
import { InboundSourceDeletedEvent } from '../../events/impl';

@CommandHandler(DeleteInboundSourceCommand)
export class DeleteInboundSourcesCommandHandler
  implements ICommandHandler<DeleteInboundSourceCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: DeleteInboundSourceCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, meta } = command;
    const inboundSourceAggregate = this.publisher.mergeObjectContext(
      new InboundSourceAggregate({ _id }),
    );
    inboundSourceAggregate.apply(
      new InboundSourceDeletedEvent(
        _id,
        meta,
      ),
    );
    inboundSourceAggregate.commit();
  }
}
