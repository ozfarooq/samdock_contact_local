import { Injectable } from '@nestjs/common';
import { BreadService } from '../../shared/services/bread.service';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { BrowseInBoundSourceQuery } from '../queries/impl/browse-inbound-source.query';
import { ReadInboundSourceQuery } from '../queries/impl/read-inbound-sources.query';
import { EditInboudSourceCommand } from '../commands/impl/edit-inbound-source.command';
import { AddInboundSourceCommand } from '../commands/impl/add-inbound-source.command';
import { DeleteInboundSourceCommand } from '../commands/impl/delete-inbound-source.command';
import { InboundSourceDTO } from '../dtos/inbound-source.dto';

@Injectable()
export class InboundSourcesService extends BreadService<InboundSourceDTO> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<InboundSourceDTO[]> {
    return await this.executeQuery(new BrowseInBoundSourceQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<InboundSourceDTO> {
    return await this.executeQuery(new ReadInboundSourceQuery(id, meta));
  }

  async edit(
    id: string,
    inboundSource: Partial<InboundSourceDTO>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditInboudSourceCommand(id, inboundSource, meta),
    );
  }

  async add(inboundSource: InboundSourceDTO, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddInboundSourceCommand(inboundSource, meta));
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteInboundSourceCommand(id, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeleteInboundSourceCommand(id, meta));
    }));
  }
}
