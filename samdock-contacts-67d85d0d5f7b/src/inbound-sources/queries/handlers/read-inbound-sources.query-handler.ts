import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadInboundSourceQuery } from '../impl/read-inbound-sources.query';
import { InboundSourcesRepository } from '../../repositories/inbound-sources.repository';
import { InboundSource } from '../../models/inbound-source.model';

@QueryHandler(ReadInboundSourceQuery)
export class ReadInboundSourcesQueryHandler implements IQueryHandler<ReadInboundSourceQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: InboundSourcesRepository) { }

  async execute(query: ReadInboundSourceQuery): Promise<InboundSource> {
    this.logger.log('Query triggered');
    const { id, meta } = query;
    try {
      return this.repo.read(id, meta);
    } catch (error) {
      this.logger.error('Failed to read on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
