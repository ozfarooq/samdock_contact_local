import { BrowseInboundSourcesQueryHandler } from './browse-inbound-sources.query-handler';
import { ReadInboundSourcesQueryHandler } from './read-inbound-sources.query-handler';

export const InboundSourcesQueryHandlers = [
  BrowseInboundSourcesQueryHandler,
  ReadInboundSourcesQueryHandler,
];
