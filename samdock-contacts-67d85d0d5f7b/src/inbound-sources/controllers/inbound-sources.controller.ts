import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBody,
  ApiForbiddenResponse,
} from '@nestjs/swagger';
import { Controller, Logger, Get, Param, Patch, Body, Post, Delete, Render } from '@nestjs/common';
import { BreadController } from '../../shared/controllers/bread.controller';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { IInboundSource } from '@daypaio/domain-events/inbound-sources';
import { InboundSourcesService } from '../services/inbound-sources.service';
import { InboundSourceDTO } from '../dtos/inbound-source.dto';
import { EventMetaData } from '@daypaio/domain-events/shared';

@ApiTags('inbound sources')
@ApiBearerAuth()
@Controller('inbound-sources')
export class InboundSourcesController implements BreadController<InboundSourceDTO> {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: InboundSourcesService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse Inbound sources',
    description: 'Get all inbound sources',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: InboundSourceDTO,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<InboundSourceDTO[]> {
    this.logger.log(`BROWSE user:${user._id}`);
    return this.service.browse(user.generateMeta());
  }

  @Get('/:id')
  @ApiOperation({
    summary: 'Read an inbound source',
    description: 'Get an inbound source by id',
  })
  @ApiOkResponse({
    description: 'The inbound source has successfully been found',
    type: InboundSourceDTO,
  })
  @ApiNotFoundResponse({ description: 'The Inbound source was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<InboundSourceDTO> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return this.service.read(id, user.generateMeta());
  }

  @Get('/:tenantID/:id/form.js')
  @Render('form')
  @ApiOperation({
    summary: 'Get js snippet for inbound source form',
    description: 'Get js snippet for inbound source form',
  })
  @ApiOkResponse({
    description: 'The form has successfully been generated',
  })
  @ApiNotFoundResponse({ description: 'The Inbound source was not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async generateForm(
    @Param('id') id: string,
    @Param('tenantID') tenantID: string,
  ): Promise<any> {
    this.logger.log(`RENDERING FORM id:${id}`);
    const inboundSource = await this.service.read(id, new EventMetaData(tenantID, null));

    return {
      ...inboundSource.formSettings,
      _inboundSourceID: id,
      submitUrl: `https://${process.env.APP_URL}/api/contacts/drafts/${id}`,
    };
  }

  @Patch('/:id')
  @ApiBody({
    type: InboundSourceDTO,
  })
  @ApiOperation({
    summary: 'Edit Inbound Source',
    description: 'Edit an Inbound Source, id is not required in the body',
  })
  @ApiOkResponse({
    description:
      'The inbound source editing event has successfully been triggered',
  })
  @ApiForbiddenResponse(
    {
      description: 'Enum must of the correct type, active, testing or disabled',
    },
  )
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body() inboundSource: Partial<IInboundSource>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    return this.service.edit(id, inboundSource, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add InboundSource ',
    description: 'Create an organization',
  })
  @ApiOkResponse({
    description:
      'The inbound source adding event has successfully been triggered',
  })
  @ApiForbiddenResponse(
    {
      description: 'Enum must of the correct type, active, testing or disabled',
    },
  )
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() inboundSource: InboundSourceDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${inboundSource._id} user:${user._id}`);
    return this.service.add(inboundSource, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete an inbound source',
    description: 'Delete an inbound source',
  })
  @ApiOkResponse({
    description:
      'The inbound source deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    return this.service.delete(id, user.generateMeta());
  }

}
