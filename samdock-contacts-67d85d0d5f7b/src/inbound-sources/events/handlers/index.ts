import { InboundSourceAddedEventHandler } from './inbound-source-added.event-handler';
import { InboundSourceEditedEventHandler } from './inbound-source-edited.event-handler';
import { InboundSourceDeletedEventHandler } from './inbound-source-deleted.event-handler';

export const InboundSourcesEventHandlers = [
  InboundSourceAddedEventHandler,
  InboundSourceEditedEventHandler,
  InboundSourceDeletedEventHandler,
];
