import { InboundSourceDeletedEvent } from '../impl';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { InboundSourcesRepository } from '../../repositories/inbound-sources.repository';

@EventsHandler(InboundSourceDeletedEvent)
export class InboundSourceDeletedEventHandler
  implements IEventHandler<InboundSourceDeletedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: InboundSourcesRepository) { }

  async handle(event: InboundSourceDeletedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.repository.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
