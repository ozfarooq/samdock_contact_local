import { IInboundSource, IInboundSourceStatus, IDOISettings, InboundSourceType, IFormSettings } from '@daypaio/domain-events/inbound-sources';
import { ApiProperty } from '@nestjs/swagger';
import { v4 } from 'uuid';

export class InboundSourceDTO implements IInboundSource {
  @ApiProperty({
    description: 'the id of the inbound source',
    required: true,
    default: v4(),
  })
  _id: string;

  @ApiProperty({
    description: 'the status of the inbound source',
    required: true,
    enum: ['disabled', 'active', 'testing'],
    default: 'active',
  })
  status: IInboundSourceStatus;

  @ApiProperty({
    description: 'the status of the inbound source',
    required: true,
    enum: InboundSourceType,
    default: InboundSourceType.Api,
  })
  type: InboundSourceType;

  @ApiProperty({
    description: 'the name of the inbound source',
    required: true,
    default: 'Form',
  })
  name: string;

  @ApiProperty({
    description: 'the form settings of the source',
    required: false,
  })
  formSettings: IFormSettings;

  @ApiProperty({
    description: 'whether the lead should be created on the verification of drafts',
    required: true,
    default: true,
  })
  createLead: boolean;

  @ApiProperty({
    description: 'the id of the user to which the created lead be assigned to',
    required: true,
    default: 'an id of the user',
  })
  editor: string;

  @ApiProperty({
    description: 'the double-opt-in settings for the inbound source',
    required: false,
  })
  doi: IDOISettings;
}
