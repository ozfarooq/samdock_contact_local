import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { InboundSource } from '../inbound-sources/models/inbound-source.model';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';

export class DoiMailFieldExtension1597657698655 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.model = getModelForClass(InboundSource);
    this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    await this.model.updateMany(
      { 'doi.mail.imprintURL': { $exists: true } },
      {
        $set: {
          'doi.mail.imprintText': 'Impressum',
          'doi.mail.footerText': 'Wenn du diese E-Mail nicht angefordert hast, kannst du sie einfach ignorieren.',
        },
      },
    ).exec();
    this.logger.log('success up');
  }

  public async down(): Promise<void> {
    await this.model.updateMany(
      {},
      { $unset: { 'doi.mail.imprintText': '', 'doi.mail.footerText': '' } },
    ).exec();
    this.logger.log('success down');
  }

}
