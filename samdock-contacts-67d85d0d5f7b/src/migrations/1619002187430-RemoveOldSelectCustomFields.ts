import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { CustomFieldDeletedEvent } from '../custom-fields/events/impl';
import { OrganizationCustomField } from '../custom-fields/models/organization-custom-field.model';
import { PersonCustomField } from '../custom-fields/models/person-custom-field.model';
import * as esClient from 'node-eventstore-client';
import { v4 } from 'uuid';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RemoveOldSelectCustomFields1619002187430 implements MigrationInterface {

  private personCustomFields: mongoose.Model<InstanceType<any>>;
  private organizationCustomFields: mongoose.Model<InstanceType<any>>;

  esConnection: esClient.EventStoreNodeConnection;

  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.personCustomFields = getModelForClass(PersonCustomField);
    this.organizationCustomFields = getModelForClass(OrganizationCustomField);
    this.logger = new Logger(`${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    const connectionSettings = {
      defaultUserCredentials: {
        username: process.env.EVENT_STORE_USERNAME,
        password: process.env.EVENT_STORE_PASSWORD,
      },
    };

    const tcpEndpoint = {
      host: process.env.EVENT_STORE_TCP_HOST,
      port: parseInt(process.env.EVENT_STORE_TCP_PORT, 10),
    };

    this.esConnection = esClient.createConnection(connectionSettings, tcpEndpoint);
    await this.esConnection.connect();

    console.log(this.esConnection);

    const wrongPersonCustomFields = await this.personCustomFields.find(
      { $or: [{ type: 'select' }, { type: 'multiselect' }], options: { $exists: false } },
    ).exec();

    const wrongOrganizationCustomFields = await this.organizationCustomFields.find(
      { $or: [{ type: 'select' }, { type: 'multiselect' }], options: { $exists: false } },
    ).exec();

    console.log(wrongPersonCustomFields);

    console.log(wrongOrganizationCustomFields);

    wrongPersonCustomFields.forEach((field) => {

      const meta = new EventMetaData(field._tenantID, field.createdBy);

      const event = new CustomFieldDeletedEvent(
        field._id,
        field.key,
        'person',
        meta,
      );

      const payload: esClient.EventData = esClient.createEventData(
        v4(),
        event.constructor.name,
        true,
        Buffer.from(JSON.stringify(event)),
      );

      const magic = -2;

      this.logger.log(event);
      this.logger.log(payload);

      this.esConnection.appendToStream(event.streamName, magic, payload);
    });

    wrongOrganizationCustomFields.forEach((field) => {

      const meta = new EventMetaData(field._tenantID, field.createdBy);

      const event = new CustomFieldDeletedEvent(
        field._id,
        field.key,
        'organization',
        meta,
      );

      const payload: esClient.EventData = esClient.createEventData(
        v4(),
        event.constructor.name,
        true,
        Buffer.from(JSON.stringify(event)),
      );

      const magic = -2;

      this.esConnection.appendToStream(event.streamName, magic, payload);
    });
    this.logger.log('success up');
  }

  public async down(): Promise<void> {}

}
