import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { OrganizationCustomField } from '../custom-fields/models/organization-custom-field.model';
import { PersonCustomField } from '../custom-fields/models/person-custom-field.model';

export class RemoveIndexesCustomFields1621502358564 implements MigrationInterface {

  private personCustomFields: mongoose.Model<InstanceType<any>>;
  private organizationCustomFields: mongoose.Model<InstanceType<any>>;

  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.personCustomFields = getModelForClass(PersonCustomField);
    this.organizationCustomFields = getModelForClass(OrganizationCustomField);
    this.logger = new Logger(`${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    try {
      await this.organizationCustomFields.syncIndexes();

      await this.personCustomFields.syncIndexes();

      this.logger.log('success up');
    } catch (_error) {}
  }

  public async down(): Promise<void> {}

}
