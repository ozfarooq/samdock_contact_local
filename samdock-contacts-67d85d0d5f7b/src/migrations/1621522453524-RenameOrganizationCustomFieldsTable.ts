import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';

export class RenameOrganizationCustomFieldsTable1621522453524 implements MigrationInterface {

  private logger = new Logger(`${this.constructor.name}`);

  async initConnection() {
    setupDotenv();
    await mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
  }

  public async up(): Promise<void> {
    try {
      await this.initConnection();
      await mongoose.connection.db.collection('orgaization-custom-fields').rename('organization-custom-fields', { dropTarget: true });

      this.logger.log('success up');
      await mongoose.disconnect();
    } catch (_error) {}
  }

  public async down(): Promise<void> {
    await this.initConnection();
    await mongoose.connection.db.collection('organization-custom-fields').rename('orgaization-custom-fields', { dropTarget: true });

    this.logger.log('success down');
    await mongoose.disconnect();
  }

}
