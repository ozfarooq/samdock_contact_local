import { BrowseDraftHandler } from './browse-drafts.query-handler';
import { ReadDraftQueryHandler } from './read-person.query-handler';

export const ContactDraftsQueryHandlers = [BrowseDraftHandler, ReadDraftQueryHandler];
