import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { BrowseDraftQuery } from '../impl/browse-draft.query';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../../drafts/repositories/drafts.repository';
import { ContactDraft } from '../../../drafts/models/draft.model';

@QueryHandler(BrowseDraftQuery)
export class BrowseDraftHandler implements IQueryHandler<BrowseDraftQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repository: DraftsRepository) {}

  async execute(query: BrowseDraftQuery): Promise<ContactDraft[]> {
    this.logger.log('Browsing Contact Drafts');
    try {
      const result = await this.repository.browse(query.meta);
      return result;
    } catch (error) {
      this.logger.error('Failed to browse on Person');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
