import { Module } from '@nestjs/common';
import { DraftsController } from './controllers/drafts.controller';
import { DraftsService } from './services/drafts.service';
import { ContactDraftCommandHandlers } from './commands/handlers';
import { ContactDraftsEventHandlers } from './events/handlers';
import { ContactDraftsQueryHandlers } from './queries/handlers';
import { TypegooseModule } from 'nestjs-typegoose';
import { ContactDraft } from './models/draft.model';
import { DraftsRepository } from './repositories/drafts.repository';
import { ContactDraftSagas } from './sagas';
import { InboundSourcesModule } from '../inbound-sources/inbound-sources.module';

@Module({
  imports: [
    TypegooseModule.forFeature([ContactDraft]),
    InboundSourcesModule,
  ],
  controllers: [DraftsController],
  providers: [
    DraftsService,
    DraftsRepository,
    ...ContactDraftsQueryHandlers,
    ...ContactDraftCommandHandlers,
    ...ContactDraftsEventHandlers,
    ...ContactDraftSagas,
  ],
  exports: [
    DraftsService,
    DraftsRepository,
  ],
})
export class DraftsModule {}
