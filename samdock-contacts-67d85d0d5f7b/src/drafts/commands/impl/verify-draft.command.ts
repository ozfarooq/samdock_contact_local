import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class VerifyDraftCommand implements ICommand {
  constructor(
    public _id: string,
    public createLead: boolean,
    public meta: EventMetaData,
  ) {}
}
