import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftAggregate } from '../../models/draft.aggregate';
import { DraftVerifiedEvent } from '@daypaio/domain-events/contact-drafts';
import { VerifyDraftCommand } from '../impl/verify-draft.command';
import { DraftsRepository } from '../../repositories/drafts.repository';
import { throwError } from 'rxjs';

@CommandHandler(VerifyDraftCommand)
export class VerifyDraftCommandHandler
  implements ICommandHandler<VerifyDraftCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher, private repo: DraftsRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: VerifyDraftCommand) {
    this.logger.log('Verifying draft');
    const { _id, createLead, meta } = command;
    const data = await this.repo.read(_id, meta);
    data.verifiedAt = Date.now();
    const draftAggregate = this.publisher.mergeObjectContext(
      new DraftAggregate({ _id }),
    );
    if (createLead !== null) {
      data.createLead = createLead;
    }
    const event = new DraftVerifiedEvent(_id, data, meta);
    draftAggregate.apply(event);
    draftAggregate.commit();
  }
}
