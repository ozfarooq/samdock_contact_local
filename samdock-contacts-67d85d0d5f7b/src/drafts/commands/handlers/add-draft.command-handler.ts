import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddDraftCommand } from '../impl/add-draft.command';
import { Logger, ForbiddenException } from '@nestjs/common';
import { DraftAggregate } from '../../models/draft.aggregate';
import { DraftAddedEvent, IContactDraft, DraftDOIMailSentEvent } from '@daypaio/domain-events/contact-drafts';
import { InboundSourcesRepository } from '../../../inbound-sources/repositories/inbound-sources.repository';
import { IInboundSourceStatus } from '@daypaio/domain-events/inbound-sources';

@CommandHandler(AddDraftCommand)
export class AddDraftCommandHandler
  implements ICommandHandler<AddDraftCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
    private readonly inboundRepo: InboundSourcesRepository,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddDraftCommand) {
    this.logger.log('COMMAND TRIGGERED: AddCommandHandler...');
    const { draft, source, meta } = command;

    // check if the inbound source exists and fetch it
    const inboundSource = await this.inboundRepo.readById(source);

    // if the inbound source status is disabled
    // throw error
    if (inboundSource.status === IInboundSourceStatus.DISABLED) {
      throw new ForbiddenException('Cannot post to an inactive source');
    }

    await this.inboundRepo.updateSubmissionsData(source);

    // create the intersection between what is allowed to be in the draft
    // and what has been posted in order to create the draft
    const filteredDraft = {
      _id: draft._id,
      createLead: inboundSource.createLead,
      editor: inboundSource.editor,
      source: inboundSource._id,
    };

    for (const property in draft) {
      if (draft.hasOwnProperty(property) && !filteredDraft[property]) {
        filteredDraft[property] = draft[property];
      }
    }

    const draftAggregate = this.publisher.mergeObjectContext(
      new DraftAggregate(filteredDraft),
    );

    meta._tenantID = inboundSource._tenantID;

    const event = new DraftAddedEvent(
      draft._id,
      filteredDraft as IContactDraft,
      meta,
    );
    draftAggregate.apply(
      event,
    );

    if (
      inboundSource?.doi?.shouldRequest &&
      draft.marketingConsent &&
      inboundSource?.doi?.mail?.imprintURL &&
      draft.email
    ) {
      draftAggregate.apply(
        new DraftDOIMailSentEvent(draft._id, draft.email, inboundSource.doi, meta),
      );
    }

    draftAggregate.commit();

  }
}
