import { AddDraftCommandHandler } from './add-draft.command-handler';
import { DeleteDraftCommandHandler } from './delete-draft.command-handler';
import { VerifyDraftCommandHandler } from './verify-draft.command-handler';

export const ContactDraftCommandHandlers = [
  AddDraftCommandHandler,
  VerifyDraftCommandHandler,
  DeleteDraftCommandHandler,
];
