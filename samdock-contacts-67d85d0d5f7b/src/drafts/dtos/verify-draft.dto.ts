import { ApiProperty } from '@nestjs/swagger';

export class VerifyDraftDTO {

  @ApiProperty({
    description: 'Whether to create a lead once the draft is verified',
    default: true,
  })
  createLead: boolean;
}
