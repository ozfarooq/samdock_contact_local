import { IContactDraft } from '@daypaio/domain-events/contact-drafts';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { v4 } from 'uuid';
import { Gender, IConsent } from '@daypaio/domain-events/persons';
import { plainToClassFromExist } from 'class-transformer';

export class ContactDraftDTO implements IContactDraft {

  @ApiProperty({
    description: 'the id of the contact draft',
    default: v4(),
  })
  _id: string;

  @ApiPropertyOptional({
    description: 'the email of the person in the draft if present',
    default: 'test@daypaio.app',
  })
  email?: string;

  @ApiPropertyOptional({
    description: 'The first name of the person in the draft if present',
    default: 'Anthony',
  })
  firstName?: string;

  @ApiPropertyOptional({
    description: 'The last name of the person in the draft if present',
    default: 'Flannigon',
  })
  lastName?: string;

  @ApiPropertyOptional({
    description: 'the birthday of the person in the draft',
    default: '2011-12-21',
  })
  birthday?: string | Date;

  @ApiPropertyOptional({
    description: 'the address of the contact draft',
    default: 'male',
  })
  gender?: Gender;

  @ApiPropertyOptional({
    description: 'the address of the contact draft',
    default: 'Dr.',
  })
  namePrefix?: string;

  @ApiPropertyOptional({
    description: 'the address of the contact draft',
    default: '?',
  })
  nameSuffix?: string;

  @ApiPropertyOptional({
    description: 'the address of the contact draft',
    default: 'www.de',
  })
  website?: string;

  @ApiPropertyOptional({
    description: 'the address of the contact draft',
    default: 'Walter-Gropius-Strasse',
  })
  street?: string;

  @ApiPropertyOptional({
    description: 'the number of the contact draft',
    default: '17',
  })
  number?: string;

  @ApiPropertyOptional({
    description: 'The post code of the contact draft',
    default: '80808',
  })
  postcode?: string;

  @ApiPropertyOptional({
    description: 'The city of the contact draft',
    default: 'Munich',
  })
  city?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+431111111',
  })
  phoneNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+4322222222',
  })
  mobileNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+4333333333',
  })
  officeNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+434444444',
  })
  homeNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+4355555555',
  })
  privateNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '+43666666666',
  })
  faxNumber?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '#abc',
  })
  instagram?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '#abc',
  })
  linkedin?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '#abc',
  })
  facebook?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '#abc',
  })
  twitter?: string;

  @ApiPropertyOptional({
    description: 'phonenumber of the person in the draft',
    default: '#abc',
  })
  xing?: string;

  @ApiPropertyOptional({
    description: 'the company name of the company present in the draft',
    default: 'Daypaio GmbH',
  })
  companyName?: string;

  @ApiPropertyOptional({
    description: 'the email of the company present in the draft',
    default: 'info@daypaio.com',
  })
  companyEmail?: string;

  @ApiPropertyOptional({
    description: 'the phonenumber of the company',
    default: '+4312345678',
  })
  companyPhoneNumber?: string;

  @ApiPropertyOptional({
    description: 'the FaxNumber of the company',
    default: '+4312345678',
  })
  companyFaxNumber?: string;

  @ApiPropertyOptional({
    description: 'the tax ID of the company',
    default: 'DE23423RFD',
  })
  companyTaxID?: string;

  @ApiPropertyOptional({
    description: 'the Website of the company',
    default: 'www.de',
  })
  companyWebsite?: string;

  @ApiPropertyOptional({
    description: 'the company address of the contact draft',
    default: 'Walter-Gropius-Strasse',
  })
  companyStreet?: string;

  @ApiPropertyOptional({
    description: 'the company number of the contact draft',
    default: '17',
  })
  companyNumber?: string;

  @ApiPropertyOptional({
    description: 'The company post code of the contact draft',
    default: '80808',
  })
  companyPostcode?: string;

  @ApiPropertyOptional({
    description: 'The company city of the contact draft',
    default: 'Munich',
  })
  companyCity?: string;

  @ApiPropertyOptional({
    description: 'the Instagram of the company',
    default: '#cba',
  })
  companyInstagram?: string;

  @ApiPropertyOptional({
    description: 'the Linkedin of the company',
    default: '#cba',
  })
  companyLinkedin?: string;

  @ApiPropertyOptional({
    description: 'the Facebook of the company',
    default: '#cba',
  })
  companyFacebook?: string;

  @ApiPropertyOptional({
    description: 'the Twitter of the company',
    default: '#cba',
  })
  companyTwitter?: string;

  @ApiPropertyOptional({
    description: 'the Xing of the company',
    default: '#cba',
  })
  companyXing?: string;

  @ApiPropertyOptional({
    description: 'free text field for requirements',
    default: 'test',
  })
  leadRequirements?: string;

  createdAt?: number;
  verifiedAt?: number;
  createLead?: boolean;
  editor?: string;
  consent?: IConsent;

  get hasAnyPersonField(): boolean {
    const personFields: (keyof IContactDraft)[] = [
      'firstName',
      'lastName',
      'email',
      'birthday',
      'gender',
      'namePrefix',
      'nameSuffix',
      'website',
      'street',
      'number',
      'postcode',
      'city',
      'phoneNumber',
      'mobileNumber',
      'officeNumber',
      'homeNumber',
      'privateNumber',
      'faxNumber',
      'instagram',
      'linkedin',
      'facebook',
      'twitter',
      'xing',
    ];

    return (Object.keys(this).some(
      (key: keyof IContactDraft) => personFields.includes(key))
    );
  }

  constructor(data?: Partial<ContactDraftDTO>) {
    plainToClassFromExist(this, data);
  }
}
