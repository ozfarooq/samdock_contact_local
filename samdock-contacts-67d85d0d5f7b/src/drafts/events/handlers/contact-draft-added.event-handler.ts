import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DraftAddedEvent } from '@daypaio/domain-events/contact-drafts';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../repositories/drafts.repository';

@EventsHandler(DraftAddedEvent)
export class ContactDraftAddedEventHandler implements IEventHandler<DraftAddedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repo: DraftsRepository) {}
  async handle(event: DraftAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    data.createdAt = meta.timestamp;
    try {
      await this.repo.add(data, meta);
    } catch (error) {
      this.logger.error(`Failed to create draft of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
