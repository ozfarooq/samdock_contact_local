import { DraftVerifiedEvent } from '@daypaio/domain-events/contact-drafts';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../repositories/drafts.repository';

@EventsHandler(DraftVerifiedEvent)
export class ContactDraftVerifiedEventHandler implements IEventHandler<DraftVerifiedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repo: DraftsRepository) {}
  async handle(event: DraftVerifiedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    try {
      await this.repo.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to verify draft of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
