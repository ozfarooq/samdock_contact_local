import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { v4 } from 'uuid';
import {
  CustomFieldEntity,
  CustomFieldStatus,
  CustomFieldType,
  ICustomField,
  ISelectListOption,
} from '@daypaio/domain-events/custom-fields';

export enum CustomFieldStatusEnum {
  Active = 'active',
  Hidden = 'hidden',
}

export enum CustomFieldEntityEnum {
  Person = 'person',
  Organization = 'organization',
}

export enum CustomFieldTypeEnum {
  Text = 'text',
  Select = 'select',
  Multiselect = 'multiselect',
  Currency = 'currency',
}

export class SelectListOptionDTO implements ISelectListOption {
  @ApiProperty({
    description: 'Label of the option',
  })
  label: string;

  @ApiProperty({
    description: 'Value of the option',
  })
  value: string;

  @ApiProperty({
    description: 'Is that value default',
    default: false,
  })
  default: boolean;

  @ApiProperty({
    description: 'Is that value disabled',
    default: false,
  })
  disabled: boolean;
}

export class CustomFieldDTO implements ICustomField {
  @ApiProperty({
    description: 'the id of the custom field',
    required: true,
    default: v4(),
  })
  _id: string;

  @ApiProperty({
    description: 'the id of the tenant',
  })
  _tenantID: string;

  @ApiProperty({
    description: 'the name of the custom field',
    required: true,
  })
  name: string;

  @ApiProperty({
    description: 'the key of the custom field',
    required: true,
  })
  key: string;

  @ApiPropertyOptional({
    description: 'the description of the custom field',
  })
  description: string;

  @ApiProperty({
    description: 'the type of the custom field',
    required: true,
    enum: CustomFieldTypeEnum,
  })
  type: CustomFieldType;

  @ApiPropertyOptional({
    description: 'the status of the custom field',
    required: true,
    enum: CustomFieldStatusEnum,
  })
  status: CustomFieldStatus;

  @ApiProperty({
    description: 'when custom field was created',
  })
  createdAt: number;

  @ApiProperty({
    description: 'id of User who created custom field',
  })
  createdBy: string;

  @ApiPropertyOptional({
    description: 'when custom field was updated',
  })
  updatedAt: number;

  @ApiPropertyOptional({
    description: 'id of User who updated custom field',
  })
  updatedBy: string;

  @ApiPropertyOptional({
    description: 'any options of the custom field',
  })
  options: any;
}

export class AddCustomFieldDTO implements Partial<ICustomField> {
  @ApiProperty({
    description: 'the id of the custom field',
    required: true,
    default: v4(),
  })
  _id: string;

  @ApiProperty({
    description: 'the entity of the custom field',
    required: true,
    enum: CustomFieldEntityEnum,
  })
  entity: CustomFieldEntity;

  @ApiProperty({
    description: 'the name of the custom field',
    required: true,
  })
  name: string;

  @ApiProperty({
    description: 'the key of the custom field',
    required: true,
  })
  key: string;

  @ApiProperty({
    description: 'the type of the custom field',
    required: true,
    enum: CustomFieldTypeEnum,
  })
  type: CustomFieldType;
}

export class BrowseAllDTO {
  @ApiProperty({
    description: 'Array of persons custom fields',
    type: [CustomFieldDTO],
  })
  persons: [CustomFieldDTO];

  @ApiProperty({
    description: 'Array of organizations custom fields',
    type: [CustomFieldDTO],
  })
  organizations: [CustomFieldDTO];
}
