import { CustomFieldDeletedEvent } from '../events/impl';
import { Injectable } from '@nestjs/common';
import { DeleteStreamSaga } from '../../shared/sagas/delete-stream.saga';

@Injectable()
export class CustomFieldStreamSaga extends DeleteStreamSaga {
  constructor() {
    super([CustomFieldDeletedEvent], 'custom_fields');
  }
}
