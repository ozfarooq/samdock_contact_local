import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldEntity, ICustomField } from '@daypaio/domain-events/custom-fields';

export class EditCustomFieldCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly entity: CustomFieldEntity,
    public readonly customField: Partial<ICustomField>,
    public readonly meta: EventMetaData,
  ) { }
}
