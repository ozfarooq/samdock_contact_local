import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldEntity, ValueSetCustomField } from '@daypaio/domain-events/custom-fields';

export class SetValueCustomFieldCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly entity: CustomFieldEntity,
    public readonly data: ValueSetCustomField,
    public readonly meta: EventMetaData,
  ) { }
}
