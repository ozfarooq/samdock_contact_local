import { AddCustomFieldsCommandHandler } from './add-custom-fields.command-handler';
import { DeleteCustomFieldsCommandHandler } from './delete-custom-fields.command-handler';
import { EditCustomFieldsCommandHandler } from './edit-custom-fields.command-handler';
import { SetValueCustomFieldCommandHandler } from './set-value-custom-field.command-handler';

export const CustomFieldsCommandHandlers = [
  AddCustomFieldsCommandHandler,
  DeleteCustomFieldsCommandHandler,
  EditCustomFieldsCommandHandler,
  SetValueCustomFieldCommandHandler,
];
