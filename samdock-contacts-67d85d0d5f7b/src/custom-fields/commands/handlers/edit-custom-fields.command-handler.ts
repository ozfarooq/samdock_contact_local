import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EditCustomFieldCommand } from '../impl/edit-custom-field.command';
import { CustomFieldAggregate } from '../../models/custom-field.aggregate';
import { CustomFieldEditedEvent } from '@daypaio/domain-events/custom-fields';

@CommandHandler(EditCustomFieldCommand)
export class EditCustomFieldsCommandHandler
  implements ICommandHandler<EditCustomFieldCommand> {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: EditCustomFieldCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, entity, customField, meta } = command;

    const customFieldAggregate = this.publisher.mergeObjectContext(
      new CustomFieldAggregate({ _id }),
    );
    customFieldAggregate.apply(
      new CustomFieldEditedEvent(
        _id,
        entity,
        customField,
        meta,
      ),
    );
    customFieldAggregate.commit();
  }
}
