import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldAggregate } from '../../models/custom-field.aggregate';
import { DeleteCustomFieldCommand } from '../impl/delete-custom-field.command';
import { CustomFieldDeletedEvent } from '@daypaio/domain-events/custom-fields';

@CommandHandler(DeleteCustomFieldCommand)
export class DeleteCustomFieldsCommandHandler
  implements ICommandHandler<DeleteCustomFieldCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: DeleteCustomFieldCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, key, entity, meta } = command;
    const customFieldAggregate = this.publisher.mergeObjectContext(
      new CustomFieldAggregate({ _id }),
    );
    customFieldAggregate.apply(
      new CustomFieldDeletedEvent(
        _id,
        key,
        entity,
        meta,
      ),
    );
    customFieldAggregate.commit();
  }
}
