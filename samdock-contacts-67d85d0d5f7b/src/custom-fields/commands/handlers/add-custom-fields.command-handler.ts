import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldAggregate } from '../../models/custom-field.aggregate';
import { AddCustomFieldCommand } from '../impl/add-custom-field.command';
import { CustomFieldAddedEvent } from '../../events/impl';

@CommandHandler(AddCustomFieldCommand)
export class AddCustomFieldsCommandHandler
    implements ICommandHandler<AddCustomFieldCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: AddCustomFieldCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, customField, meta } = command;

    const customFieldAggregate = this.publisher.mergeObjectContext(
      new CustomFieldAggregate({ _id: customField._id }),
    );
    customFieldAggregate.apply(
      new CustomFieldAddedEvent(
        _id,
        customField,
        meta,
      ),
    );
    customFieldAggregate.commit();
  }
}
