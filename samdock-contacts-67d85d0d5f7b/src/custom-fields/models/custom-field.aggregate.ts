import {
  CustomFieldType,
  ICustomField,
  ISelectListOption,
  CustomFieldStatus,
  CustomFieldEntity,
  CustomFieldAddedEvent,
  CustomFieldEditedEvent,
  CustomFieldDeletedEvent,
} from '@daypaio/domain-events/custom-fields';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';

export class CustomFieldAggregate extends AggregateRoot implements ICustomField {
  _id: string;
  _tenantID: string;
  name: string;
  key: string;
  description?: string;
  type: CustomFieldType;
  entity: CustomFieldEntity;
  createdAt: number;
  createdBy: string;
  updatedAt?: number;
  updatedBy?: string;
  status: CustomFieldStatus;
  options?: any;

  constructor(data?: Partial<ICustomField>) {
    super();
    plainToClassFromExist(this, data);
  }

  add(meta: EventMetaData) {
    this.apply(new CustomFieldAddedEvent(this._id, this, meta));
  }

  edit(meta: EventMetaData) {
    this.apply(new CustomFieldEditedEvent(this._id, this.entity, this, meta));
  }

  delete(meta: EventMetaData) {
    this.apply(new CustomFieldDeletedEvent(this._id, this.key, this.entity, meta));
  }
}
