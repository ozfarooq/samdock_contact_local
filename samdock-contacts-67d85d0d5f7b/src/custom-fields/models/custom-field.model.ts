import { index, prop } from '@typegoose/typegoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CustomFieldType, ICustomField, ISelectListOption, CustomFieldStatus } from '@daypaio/domain-events/custom-fields';
import { v4 } from 'uuid';
import { CustomFieldStatusEnum, CustomFieldTypeEnum } from '../dtos/custom-field.dto';

export class CustomFieldSelectOption implements ISelectListOption {
  @prop({ required: true })
  label: string;

  @prop({ required: true })
  value: string;

  @prop({ required: true, default: false })
  default: boolean;

  @prop({ required: true, default: false })
  disabled: boolean;
}

@index({ _tenantID: 1, key: 1 }, { unique: true })
@index({ _tenantID: 1 })
export class CustomField implements ICustomField {
  @ApiProperty({
    description: 'The id of the custom field',
    required: true,
    default: v4(),
  })
  @prop({ required: true, default: v4() })
  _id: string;

  @ApiProperty({
    description: 'The id of the tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The name of the custom field',
    required: true,
  })
  @prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'The api key of the custom field',
    required: true,
  })
  @prop({ required: true })
  key: string;

  @ApiPropertyOptional({
    description: 'The description of the custom field',
  })
  @prop()
  description: string;

  @ApiProperty({
    description: 'The type of the custom field',
    required: true,
  })
  @prop({ required: true, enum: CustomFieldTypeEnum })
  type: CustomFieldType;

  @ApiPropertyOptional({
    description: 'The status of the custom field',
  })
  @prop({ required: true, default: CustomFieldStatusEnum.Active, enum: CustomFieldStatusEnum })
  status: CustomFieldStatus;

  @ApiProperty({
    description: 'The timestamp when custom field was created',
  })
  @prop()
  createdAt: number;

  @ApiProperty({
    description: 'The id of User who has created custom field',
  })
  @prop()
  createdBy: string;

  @ApiPropertyOptional({
    description: 'The timestamp when custom field was updated',
  })
  @prop()
  updatedAt: number;

  @ApiPropertyOptional({
    description: 'The id of User who updated custom field',
  })
  @prop()
  updatedBy: string;

  @ApiPropertyOptional({
    description: 'Any options of the custom field',
  })
  @prop()
  options?: any;
}
