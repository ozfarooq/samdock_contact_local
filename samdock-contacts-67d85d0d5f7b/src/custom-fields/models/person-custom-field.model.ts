import { modelOptions } from '@typegoose/typegoose';
import { CustomField } from './custom-field.model';

@modelOptions({ schemaOptions: { collection: 'person-custom-fields' } })
export class PersonCustomField extends CustomField {}
