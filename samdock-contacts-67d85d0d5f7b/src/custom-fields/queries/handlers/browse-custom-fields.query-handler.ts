import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseCustomFieldsQuery } from '../impl/browse-custom-fields.query';
import { BrowseAllCustomFields, CustomFieldsRepository } from '../../repositories/custom-fields.repository';

@QueryHandler(BrowseCustomFieldsQuery)
export class BrowseCustomFieldsQueryHandler implements IQueryHandler<BrowseCustomFieldsQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: CustomFieldsRepository) { }

  async execute(query: BrowseCustomFieldsQuery): Promise<BrowseAllCustomFields> {
    this.logger.log('Query triggered');
    const { meta } = query;
    try {
      return await this.repo.browse(meta);
    } catch (error) {
      this.logger.error('Failed to browse on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
