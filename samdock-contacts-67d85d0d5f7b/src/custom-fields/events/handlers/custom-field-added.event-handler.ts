import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldsRepository } from '../../repositories/custom-fields.repository';
import { CustomFieldAddedEvent } from '@daypaio/domain-events/custom-fields';

@EventsHandler(CustomFieldAddedEvent)
export class CustomFieldAddedEventHandler
  implements IEventHandler<CustomFieldAddedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: CustomFieldsRepository) { }

  async handle(event: CustomFieldAddedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;

    try {
      await this.repository.add(
        { ...data, createdAt: meta.timestamp },
        meta,
      );
    } catch (error) {
      this.logger.error(`Failed to create custom field of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
