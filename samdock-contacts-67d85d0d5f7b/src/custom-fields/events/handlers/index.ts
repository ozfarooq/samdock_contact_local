import { CustomFieldSetValueEventHandler } from './custom-field-set-value.event-handler';
import { CustomFieldAddedEventHandler } from './custom-field-added.event-handler';
import { CustomFieldDeletedEventHandler } from './custom-field-deleted.event-handler';
import { CustomFieldEditedEventHandler } from './custom-field-edited.event-handler';

export const CustomFieldsEventHandlers = [
  CustomFieldAddedEventHandler,
  CustomFieldEditedEventHandler,
  CustomFieldDeletedEventHandler,
  CustomFieldSetValueEventHandler,
];
