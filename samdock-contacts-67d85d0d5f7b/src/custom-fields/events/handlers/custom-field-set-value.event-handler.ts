import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldsRepository } from '../../repositories/custom-fields.repository';
import { CustomFieldSetValueEvent } from '@daypaio/domain-events/custom-fields';

@EventsHandler(CustomFieldSetValueEvent)
export class CustomFieldSetValueEventHandler
  implements IEventHandler<CustomFieldSetValueEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: CustomFieldsRepository) { }

  async handle(event: CustomFieldSetValueEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, entity, data, meta } = event;

    try {
      await this.repository.setValue(_id, entity, data, meta);
    } catch (error) {
      this.logger.error(`Failed to edit inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
