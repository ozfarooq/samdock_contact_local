import { GrantDOICommandHandler } from './grant-doi.command-handler';

export const DOICommandHandlers = [
  GrantDOICommandHandler,
];
