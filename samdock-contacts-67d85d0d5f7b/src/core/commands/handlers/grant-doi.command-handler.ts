import { CommandHandler, ICommandHandler, EventPublisher, CommandBus } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { GrantDOICommand } from '../impl/grant-doi.command';
import { DraftsService } from '../../../drafts/services/drafts.service';
import { PersonsService } from '../../../persons/services/persons.service';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { DraftAggregate } from '../../../drafts/models/draft.aggregate';
import { PersonAggregate } from '../../../persons/models/person.aggregate';
import { VerifyDraftCommand } from '../../../drafts/commands/impl/verify-draft.command';

@CommandHandler(GrantDOICommand)
export class GrantDOICommandHandler
  implements ICommandHandler<GrantDOICommand> {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
    private readonly commandBus: CommandBus,
    private readonly draftService: DraftsService,
    private readonly personService: PersonsService,
  ) { }

  async execute(command: GrantDOICommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _entityID, redirectURL, verify, ip, email, meta } = command;

    this.logger.log({ ip, redirectURL, _entityID, email, meta });

    this.writeToDB(
      _entityID,
      ip,
      email,
      verify,
      meta,
    );

    return redirectURL;
  }

  async writeToDB(
    _entityID: string,
    ip: string,
    email: string,
    verify: boolean,
    meta: EventMetaData,
  ): Promise<void> {
    try {
      await this.draftService.read(_entityID, meta);
      const draft = this.publisher.mergeObjectContext(
        new DraftAggregate({ _id: _entityID }),
      );
      draft.grantDOI(ip, email, meta);
      draft.commit();

      if (verify) {
        setTimeout(
          () => {
            this.commandBus.execute(new VerifyDraftCommand(draft._id, null, meta));
          },
          1000,
        );
      }
    } catch {
      try {
        await this.personService.read(_entityID, meta);
        const person = this.publisher.mergeObjectContext(
          new PersonAggregate({ _id: _entityID }),
        );
        person.grantDOI(ip, email, meta);
        person.commit();
      } catch {
        // Not found as person OR draft
      }
    }
  }

}
