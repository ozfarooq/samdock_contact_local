import { Module } from '@nestjs/common';
import { DoiController } from './doi/doi.controller';
import { DOICommandHandlers } from './commands/handlers';
import { DraftsService } from '../drafts/services/drafts.service';
import { PersonsService } from '../persons/services/persons.service';
import { InboundSourcesService } from '../inbound-sources/services/inbound-sources.service';

@Module({
  controllers: [DoiController],
  providers: [
    DraftsService,
    PersonsService,
    InboundSourcesService,
    ...DOICommandHandlers,
  ],
})
export class CoreModule {}
