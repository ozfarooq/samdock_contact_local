import { Controller, Get, Param, Redirect, Query } from '@nestjs/common';
import { ApiOperation, ApiOkResponse } from '@nestjs/swagger';
import { CommandBus } from '@nestjs/cqrs';
import { GrantDOICommand } from '../commands/impl/grant-doi.command';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IPAddress } from '../../shared/decorators/ip-address.decorator';

@Controller('doi')
export class DoiController {

  constructor(private commandBus: CommandBus) {}

  @Get(':draftOrPersonID/grant')
  @Redirect('https://samdock.com', 302)
  @ApiOperation({
    summary: 'Grant DOI',
    description: 'Grant DOI',
  })
  @ApiOkResponse({
    description: 'The draft verification event has successfully been delivered',
  })
  async grantDOI(
    @Param('draftOrPersonID') id: string,
    @Query('_tenantID') _tenantID: string,
    @Query('redirectUrl') redirectURL: string,
    @Query('verify') verify: string,
    @Query('email') email: string,
    @IPAddress() ip: string,
  ): Promise<{ url: string }> {
    const meta = new EventMetaData(_tenantID, null);
    const redirectUrl = await this.commandBus.execute(
      new GrantDOICommand(id, redirectURL, verify === 'true', ip, email, meta),
    );

    return { url: redirectUrl };
  }

}
