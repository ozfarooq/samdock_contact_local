import { Test, TestingModule } from '@nestjs/testing';
import { DoiController } from './doi.controller';
import { CommandBus } from '@nestjs/cqrs';

describe('Doi Controller', () => {
  let controller: DoiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DoiController],
      providers: [CommandBus],
    }).compile();

    controller = module.get<DoiController>(DoiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
