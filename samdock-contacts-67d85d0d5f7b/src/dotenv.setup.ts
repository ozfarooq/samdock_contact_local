import * as dotenv from 'dotenv';

export function setupDotenv() {
  dotenv.config();
}

setupDotenv();
