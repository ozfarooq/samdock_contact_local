import {
  EventStoreBusConfig,
  EventStoreSubscriptionType,
} from 'nestjs-eventstore';
import * as TenantEventInstantiators  from '@daypaio/domain-events/tenants/impl';
import * as UserEventInstantiators  from '@daypaio/domain-events/users/impl';
import * as PersonEventInstantiators  from '@daypaio/domain-events/persons/impl';
import * as OrganizationEventInstantiators from '@daypaio/domain-events/organizations/impl';
import * as DealsEventInstantiators from '@daypaio/domain-events/deals/impl';
import * as ContactDraftsEventInstantiators from '@daypaio/domain-events/contact-drafts/impl';
import * as CustomFieldsEventInstantiators from '@daypaio/domain-events/custom-fields/impl';
import * as InboundSourceEventInstantiators from '@daypaio/domain-events/inbound-sources/impl';
import * as ImportEventInstantiators from '@daypaio/domain-events/import/impl';
import * as TaskEventInstantiators from '@daypaio/domain-events/tasks/impl';
import * as ContactViewsEventInstantiators from '@daypaio/domain-events/contact-views/impl';
import * as ActivityEventInstantiators from '@daypaio/domain-events/activities/impl';

export const eventStoreBusConfig: EventStoreBusConfig = {
  subscriptions: [
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-contact_views',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-persons',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-organizations',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-deals',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-contact_drafts',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-inbound_sources',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-custom_fields',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-import',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-TenantDeletedEvent',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-UserAddedEvent',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-UserDeletedEvent',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-tasks',
      persistentSubscriptionName: 'contacts',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-ActivityAddedEvent',
      persistentSubscriptionName: 'contacts',
    },
  ],
  events: {
    ...PersonEventInstantiators,
    ...TenantEventInstantiators,
    ...OrganizationEventInstantiators,
    ...DealsEventInstantiators,
    ...ContactDraftsEventInstantiators,
    ...InboundSourceEventInstantiators,
    ...CustomFieldsEventInstantiators,
    ...ImportEventInstantiators,
    ...TaskEventInstantiators,
    ...ContactViewsEventInstantiators,
    ...UserEventInstantiators,
    ...ActivityEventInstantiators,
  },
};
