import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { TaskAddedEvent, TaskEditedEvent, TaskDeletedEvent } from '@daypaio/domain-events/tasks';
import { TaskLinkedToOrganizationCommand } from '../commands/impl/task-linked-to-organization.command';
import { TaskUnlinkedFromOrganizationCommand } from '../commands/impl/task-unlinked-from-organization.command';
import { OrganizationRelatedTaskCompletedCommand } from '../commands/impl/organization-related-task-completed.command';
import { OrganizationRelatedTaskReopenedCommand } from '../commands/impl/organization-related-task-reopened.command';

export class ORelatedTaskSaga {

  @Saga()
  oRelatedTaskAdded = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(TaskAddedEvent),
      filter(event => event.constructor.name === 'TaskAddedEvent'),
      map(
        (event: TaskAddedEvent) => {
          const { task, meta } = event;
          if (task._organizationID) {
            return new TaskLinkedToOrganizationCommand(task._organizationID, task._id, meta);
          }
        },
      ),
    );
  }

  @Saga()
  oRelatedTaskChanged = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(TaskEditedEvent),
      filter(event => event.constructor.name === 'TaskEditedEvent'),
      map(
        (event: TaskEditedEvent) => {
          const { task, oldTask, meta } = event;
          if (task._organizationID) {
            return new TaskLinkedToOrganizationCommand(task._organizationID, oldTask._id, meta);
          }
          if (task._organizationID === null) {
            return new TaskUnlinkedFromOrganizationCommand(
              oldTask._organizationID, oldTask._id, meta);
          }
          if (oldTask._organizationID && task.completionDate) {
            return new OrganizationRelatedTaskCompletedCommand(
              oldTask._organizationID, oldTask._id, meta);
          }
          if (oldTask._organizationID && task.completionDate === null) {
            return new OrganizationRelatedTaskReopenedCommand(
              oldTask._organizationID, oldTask._id, meta);
          }
        },
      ),
    );
  }

}
