import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RelatedDealWonCommand } from '../commands/impl/related-deal-won.command';
import { DealWonEvent } from '@daypaio/domain-events/deals';

export class RelatedDealWonSaga {

  @Saga()
  relatedDealWon = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealWonEvent),
      map(
        (event: DealWonEvent) => {
          const { _dealID, stage, meta } = event;
          return new RelatedDealWonCommand(_dealID, stage,  meta);
        },
      ),
    );
  }

}
