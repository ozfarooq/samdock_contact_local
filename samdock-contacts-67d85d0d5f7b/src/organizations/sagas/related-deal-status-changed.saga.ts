import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { RelatedDealStageChangedCommand } from '../commands/impl/related-deal-status-changed.command';
import { DealStageChangedEvent } from '@daypaio/domain-events/deals';

export class RelatedDealStageChangedSaga {

  @Saga()
  relatedDealStageChanged = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealStageChangedEvent),
      filter(event => event.constructor.name === 'DealStageChangedEvent'),
      map(
        (event: DealStageChangedEvent) => {
          const { _dealID, stage, meta } = event;
          return new RelatedDealStageChangedCommand(_dealID, stage,  meta);
        },
      ),
    );
  }

}
