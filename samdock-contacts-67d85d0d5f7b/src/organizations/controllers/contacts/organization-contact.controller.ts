import { Controller, Post, Param, Patch, Body, Delete } from '@nestjs/common';
import { OrganizationsService } from '../../services/organizations.service';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import { ApiBody, ApiTags, ApiBearerAuth } from '@nestjs/swagger';

class RelatePersonToOrgDto {
  _id: string;
}

@ApiTags('organizations contacts')
@ApiBearerAuth()
@Controller('organizations/:organizationID/contacts')
export class OrganizationContactController {

  constructor(private organizationsService:  OrganizationsService) {}

  @Post('')
  @ApiBody({
    type: RelatePersonToOrgDto,
  })
  relatePersonToOrganization(
    @Param('organizationID') organizationID: string,
    @Body('_id') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.relatePerson(
      organizationID,
      personID,
      user.generateMeta(),
    );
  }

  @Delete('/:employeeID')
  unrelatePersonToOrganization(
    @Param('organizationID') organizationID: string,
    @Param('employeeID') personID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.unrelatePerson(
      organizationID,
      personID,
      user.generateMeta(),
    );
  }

  @Patch(':personID/assignAsContactPerson')
  assignContactPerson(
    @Param('personID') personID: string,
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.assignContactPerson(
      organizationID,
      personID,
      user.generateMeta(),
    );
  }

  @Patch(':employeeID/role')
  changeRoleOfEmployee(
    @Param('employeeID') personID: string,
    @Param('organizationID') organizationID: string,
    @Body('role') role: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.changeRoleOfEmployee(
      organizationID,
      personID,
      role,
      user.generateMeta(),
    );
  }
}
