import {
  Controller,
  Get,
  Logger,
  Patch,
  Param,
  Body,
  Post,
  Delete,
  Put,
  UseInterceptors,
  ForbiddenException,
  UploadedFile,
  ValidationPipe,
} from '@nestjs/common';
import { OrganizationsService } from '../services/organizations.service';
import { BreadController } from '../../shared/controllers/bread.controller';
import {
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiBody,
} from '@nestjs/swagger';
import {
  GetUser,
  LoggedInUser,
} from '../../shared/decorators/get-user.decorator';
import { IOrganization } from '../models/organization.interface';
import { Organization } from '../models/organization.model';
import { OrganizationDTO } from '../dto/organization.dto';
import { GoogleCloudStorageService } from '../../shared/services/google-cloud-storage/google-cloud-storage.service';
import { memoryStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';

const multiplier = 5;
const bit = 1024;
const fiveMB = multiplier * bit * bit;

@ApiTags('organizations')
@ApiBearerAuth()
@Controller('organizations')
export class OrganizationsController implements BreadController<IOrganization> {
  protected logger = new Logger(this.constructor.name);
  constructor(
    private service: OrganizationsService,
    private storageService: GoogleCloudStorageService,
  ) {}

  @Get()
  @ApiOperation({
    summary: 'Browse Organizations',
    description: 'Get all organizations',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: Organization,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<IOrganization[]> {
    this.logger.log(`BROWSE user:${user._id}`);
    return await this.service.browse(user.generateMeta());
  }

  @Get('/:id')
  @ApiOperation({
    summary: 'Read Organization',
    description: 'Get an organization by id',
  })
  @ApiOkResponse({
    description: 'The organization has successfully been found',
    type: Organization,
  })
  @ApiNotFoundResponse({ description: 'The organization was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<IOrganization> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return await this.service.read(id, user.generateMeta());
  }

  @Patch('/:id')
  @ApiBody({
    type: OrganizationDTO,
  })
  @ApiOperation({
    summary: 'Edit Organization',
    description: 'Edit an organization',
  })
  @ApiOkResponse({
    description:
      'The organization editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body() organization: Partial<OrganizationDTO>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.edit(id, organization, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Organization',
    description: 'Create an organization',
  })
  @ApiOkResponse({
    description:
      'The organization adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() organization: OrganizationDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${organization._id} user:${user._id}`);
    await this.service.add(organization, user.generateMeta());
  }

  @Delete()
  @ApiOperation({
    summary: 'Bulk delete organizations',
    description: 'Delete a list of organizations',
  })
  @ApiOkResponse({
    description: 'The organizations have been deleted',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async bulkDelete(
    @Body('ids') ids: string[],
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`BULK DELETE id:${ids} user:${user._id}`);
    return await this.service.bulkDelete(ids, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete Organization',
    description: 'Delete an organization',
  })
  @ApiOkResponse({
    description:
      'The organization deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    await this.service.delete(id, user.generateMeta());
  }

  @Put('/:id/logo')
  @ApiOperation({
    summary: 'Change Tenant logo',
    description: 'Change the logo of a tenant',
  })
  @ApiOkResponse({
    description: 'The tenant logo has been changed',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @UseInterceptors(FileInterceptor('logo', {
    storage: memoryStorage(),
    limits: { fileSize: fiveMB } },
  ))
  async changeLogo(
    @Param('id') id: string,
    @UploadedFile() file: any,
    @Body('version') version: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    if (!file.mimetype.startsWith('image/')) {
      throw new ForbiddenException();
    }

    const logoUrl = `https://storage.googleapis.com/${user._tenantID}/organizations/${id}?v=${version}`;

    await this.storageService.uploadFile(
      file,
      `organizations/${id}`,
      user._tenantID,
      true,
    );

    await this.service.edit(id, { logo: logoUrl }, user.generateMeta());

  }

  @Delete('/:id/logo')
  @ApiOperation({
    summary: 'Delete Tenant logo',
    description: 'Delete the logo of a tenant',
  })
  @ApiOkResponse({
    description: 'The tenant logo has been deleted',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async deleteLogo(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {

    await this.storageService.deleteFile(
      `organizations/${id}`,
      user._tenantID,
    );

    await this.service.edit(id, { logo: null }, user.generateMeta());

  }

}
