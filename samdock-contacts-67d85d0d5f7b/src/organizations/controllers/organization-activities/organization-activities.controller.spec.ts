import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationActivitiesController } from './organization-activities.controller';
import { OrganizationActivitiesService } from '../../services/organization-activities.service';

const mockOrganizationActivitiesService = () => ({});

describe('OrganizationActivities Controller', () => {
  let controller: OrganizationActivitiesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrganizationActivitiesController],
      providers: [{
        provide: OrganizationActivitiesService,
        useValue: mockOrganizationActivitiesService,
      }],
    }).compile();

    controller = module.get<OrganizationActivitiesController>(OrganizationActivitiesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
