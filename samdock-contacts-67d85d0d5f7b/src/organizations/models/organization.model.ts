import { prop, index } from '@typegoose/typegoose';
import {
  IOrganization,
  IOrganizationAddress,
} from './organization.interface';
import { plainToClassFromExist } from 'class-transformer';
import { IOrganizationBranch, EmployeeRelation } from '../events/impl';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { v4 } from 'uuid';
import { IIntegrations } from '@daypaio/domain-events/shared/interfaces/integrations.interface';

export class OrganizationAddress implements IOrganizationAddress {
  @ApiPropertyOptional({
    description: 'The street name of the address (string:alpha)',
  })
  @prop()
  street: string;

  @ApiPropertyOptional({
    description: 'The house number (two digits and char)',
  })
  @prop()
  number: string;

  @ApiPropertyOptional({
    description: 'The post code consisting of 5 digits',
  })
  @prop()
  postcode: string;

  @ApiPropertyOptional({
    description: 'The city name (string:alpha)',
  })
  @prop()
  city: string;

  @ApiPropertyOptional({
    description: 'The name of the country (string:alpha)',
  })
  @prop()
  country: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

  serialize(): string {
    return JSON.stringify(this);
  }
}

export class OrganizationBranch implements IOrganizationBranch {

  constructor(data: Partial<OrganizationBranch>) {
    plainToClassFromExist(this, data);
  }

  @ApiProperty({
    description: 'The ID of the branch',
    required: true,
    default: v4(),
  })
  @prop({ required: true, sparse: true })
  _id: string;

  @ApiProperty({
    description: 'The name of the branch',
    required: true,
  })
  @prop({ required: true })
  name: string;

  @ApiPropertyOptional({
    description: 'The address of the branch',
  })
  @prop()
  address: OrganizationAddress;
}

class EmployeeRelationModel implements EmployeeRelation {
  @prop()
  _personID: string;

  @prop()
  role?: string;
}

@index({ _tenantID: 1 })
@index({ _tenantID: 1, _id: 1 })
export class Organization implements IOrganization {
  @ApiProperty({
    description: 'the related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the organization',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiPropertyOptional({
    description: 'The email of the organization',
  })
  @prop()
  email: string;

  @ApiProperty({
    description: 'The name of the organization',
    required: true,
  })
  @prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'The phone number of the organization',
    required: false,
  })
  @prop({ required: false })
  phoneNumber: string;

  @ApiPropertyOptional({
    description: 'The fax number of the person',
  })
  @prop()
  faxNumber: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  linkedin: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  xing: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  facebook: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  twitter: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  instagram: string;

  @ApiPropertyOptional({
    description: 'The Umsatzsteuer ID / VAT Reg No of the organization',
  })
  @prop({ required: false })
  vatRegNo: string;

  @ApiPropertyOptional({
    description: 'The website of the organization',
  })
  @prop({ required: false })
  website: string;

  @ApiPropertyOptional({
    description: 'The logo of the organization',
  })
  @prop({ required: false })
  logo: string;

  @ApiPropertyOptional({
    description: 'The created at timestamp of the organization',
  })
  @prop()
  createdAt: Date;

  @prop()
  lastActivity: number;

  @ApiPropertyOptional({
    description: 'Import id',
  })
  @prop()
  importID: string;

  @ApiPropertyOptional({
    description: 'The address of the organization',
  })
  @prop()
  address: OrganizationAddress;

  @ApiPropertyOptional({
    description: 'The branches of the organization',
  })
  @prop({ type: OrganizationBranch })
  branches: OrganizationBranch[];

  @ApiPropertyOptional({
    description: 'The employees of the organization',
  })
  @prop({ type: EmployeeRelationModel, _id: false })
  employees: EmployeeRelationModel[];

  @ApiProperty({
    description: 'The deals related to the organization',
    required: false,
  })
  @prop({ type: String })
  deals: string[];

  @ApiPropertyOptional({
    description: 'The main contact person of the organization',
  })
  @prop()
  contactPerson: string;

  @ApiPropertyOptional({
    description: 'Custom fields for each organization',
  })
  @prop({ _id: false })
  customFields?: EntityCustomFields;

  @ApiPropertyOptional({
    description: 'Customer status of the organization',
  })
  @prop()
  isCustomer: boolean;

  @ApiPropertyOptional({
    description: 'Integrations info of the organzaion',
  })
  @prop({ _id: false })
  integrations?: IIntegrations;

  constructor(data?: Partial<IOrganization>) {
    plainToClassFromExist(this, data);
  }
}
