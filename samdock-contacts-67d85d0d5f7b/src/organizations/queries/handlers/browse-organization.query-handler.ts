import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseOrganizationQuery } from '../impl/browse-organization.query';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { Organization } from '../../models/organization.model';

@QueryHandler(BrowseOrganizationQuery)
export class BrowseOrganizationHandler
  implements IQueryHandler<BrowseOrganizationQuery> {
  private logger: Logger;
  constructor(private repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: BrowseOrganizationQuery): Promise<Organization[]> {
    this.logger.log('QUERY TRIGGERED: BrowseOrganizationHandler...');
    try {
      return await this.repository.browse(query.meta);
    } catch (error) {
      this.logger.error('Failed to browse organizations');
      this.logger.log(error.stack);
      throw error;
    }
  }
}
