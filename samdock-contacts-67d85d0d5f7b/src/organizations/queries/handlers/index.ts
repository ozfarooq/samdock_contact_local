import { ReadOrganizationHandler } from './read-organization.query-handler';
import { BrowseOrganizationHandler } from './browse-organization.query-handler';
import { FindOrganizationHandler } from './find-organization.query-handler';

export const OrganizationQueryHandlers = [
  ReadOrganizationHandler,
  BrowseOrganizationHandler,
  FindOrganizationHandler,
];
