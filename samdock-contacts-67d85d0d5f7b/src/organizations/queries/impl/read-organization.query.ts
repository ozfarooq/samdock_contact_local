import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ReadOrganizationQuery implements IQuery {
  constructor(public readonly id: string, public meta: EventMetaData) {}
}
