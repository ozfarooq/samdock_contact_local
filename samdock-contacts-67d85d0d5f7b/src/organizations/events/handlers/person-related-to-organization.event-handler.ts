import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { PersonRelatedToOrganizationEvent } from '../impl';

@EventsHandler(PersonRelatedToOrganizationEvent)
export class PersonRelatedToOrganizationEventHandler
  implements IEventHandler<PersonRelatedToOrganizationEvent> {
  private logger: Logger;
  constructor(private readonly repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async handle(event: PersonRelatedToOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _personID, meta } = event;
    try {
      await this.repository.relatePerson(_organizationID, _personID, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
