import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { BranchOfOrganizationEditedEvent } from '../impl';

@EventsHandler(BranchOfOrganizationEditedEvent)
export class BranchOfOrganizationEditedEventHandler
  implements IEventHandler<BranchOfOrganizationEditedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: BranchOfOrganizationEditedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _branchID, data, meta } = event;
    try {
      await this.repository.editBranch(_organizationID, _branchID, data, meta);
    } catch (error) {
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
