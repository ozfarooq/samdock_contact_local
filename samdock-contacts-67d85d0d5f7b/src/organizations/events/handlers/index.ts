import { AddOrganizationEventHandler } from './add-organization.event-handler';
import { DeleteOrganizationEventHandler } from './delete-organization.event-handler';
import {
  DeleteByQueryOrganizationEventHandler,
} from './delete-by-query-organization.event-handler';
import { EditOrganizationEventHandler } from './edit-organization.event-handler';
import {
  BranchAddedToOrganizationEventHandler,
} from './branch-added-to-organization.event-handler';
import { BranchOfOrganizationEditedEventHandler } from './branch-of-organization-edited.event-handler';
import { BranchOfOrganizationDeletedEventHandler } from './branch-of-organization-deleted.event-handler';
import { PersonRelatedToOrganizationEventHandler } from './person-related-to-organization.event-handler';
import { ContactPersonAssignedEventHandler } from './contact-person-assigned.event-handler';
import { PersonUnrelatedFromOrganizationEventHandler } from './person-unrelated-from-organization.event-handler';
import {
  DealRelatedToOrganizationEventHandler,
} from './deal-related-to-organization.event-handler';
import { DealUnrelatedFromOrganizationEventHandler } from './deal-unrelated-from-organization.event-handler';
import { OrganizationCreatedFromDraftEventHandler } from './organization-created-from-draft.event-handler';
import { EmployeeRelationRoleChangedEventHandler } from './employee-relation-role-changed.event-handler';
import {
  LeadRelatedToOrganizationEventHandler,
} from './lead-related-to-organization.event-handler';
import {
  LeadUnrelatedFromOrganizationEventHandler,
} from './lead-unrelated-from-organization.event-handler';
import { ODeletePersonEventHandler } from './delete-person.event-handler';

export const OrganizationEventHandlers = [
  AddOrganizationEventHandler,
  DeleteOrganizationEventHandler,
  EditOrganizationEventHandler,
  BranchAddedToOrganizationEventHandler,
  BranchOfOrganizationEditedEventHandler,
  BranchOfOrganizationDeletedEventHandler,
  PersonRelatedToOrganizationEventHandler,
  PersonUnrelatedFromOrganizationEventHandler,
  ContactPersonAssignedEventHandler,
  DealRelatedToOrganizationEventHandler,
  DealUnrelatedFromOrganizationEventHandler,
  OrganizationCreatedFromDraftEventHandler,
  EmployeeRelationRoleChangedEventHandler,
  LeadRelatedToOrganizationEventHandler,
  LeadUnrelatedFromOrganizationEventHandler,
  DeleteByQueryOrganizationEventHandler,
  ODeletePersonEventHandler,
];
