import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { BranchOfOrganizationDeletedEvent } from '../impl';

@EventsHandler(BranchOfOrganizationDeletedEvent)
export class BranchOfOrganizationDeletedEventHandler
  implements IEventHandler<BranchOfOrganizationDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: BranchOfOrganizationDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _branchID, meta } = event;
    try {
      await this.repository.deleteBranch(_organizationID, _branchID, meta);
    } catch (error) {
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
