import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationEditedEvent } from '../impl';

@EventsHandler(OrganizationEditedEvent)
export class EditOrganizationEventHandler
  implements IEventHandler<OrganizationEditedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: OrganizationEditedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _id, data, meta } = event;
    try {
      await this.repository.edit(_id, data, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
