import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationDeletedByQueryEvent } from '../impl';

@EventsHandler(OrganizationDeletedByQueryEvent)
export class DeleteByQueryOrganizationEventHandler
  implements IEventHandler<OrganizationDeletedByQueryEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: OrganizationDeletedByQueryEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { query, meta } = event;
    try {
      await this.repository.deleteByQuery(query, meta);
    } catch (error) {
      this.logger.error(`Failed to delete organization of query ${query}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
