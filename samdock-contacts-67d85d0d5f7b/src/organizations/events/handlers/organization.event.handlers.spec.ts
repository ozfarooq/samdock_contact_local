import { TestingModule, Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { OrganizationEventHandlers } from '.';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { Organization } from '../../models/organization.model';
import { AddOrganizationEventHandler } from './add-organization.event-handler';
import { EditOrganizationEventHandler } from './edit-organization.event-handler';
import { DeleteOrganizationEventHandler } from './delete-organization.event-handler';
import {
  OrganizationAddedEvent,
  OrganizationEditedEvent,
  OrganizationDeletedEvent,
} from '../impl';
import { EventMetaData } from '@daypaio/domain-events/shared';

const mockOrganizationRepository = () => ({
  add: jest.fn(),
  edit: jest.fn(),
  delete: jest.fn(),
});

const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockOrganizations(): Organization[] {
  const firstOrganization = new Organization({
    _id: 'org1',
    name: 'Alam',
    email: 't@example.com',
    phoneNumber: '12345',
  });

  const secondOrganization = new Organization({
    _id: 'org2',
    name: 'Grosch',
    email: 'j@example.com',
    phoneNumber: '12345',
  });

  const arrayOfPersons: Organization[] = [];
  arrayOfPersons.push(firstOrganization);
  arrayOfPersons.push(secondOrganization);

  return arrayOfPersons;
}

describe('OrganizationEventHandlers', () => {
  const mockOrganizations: Organization[] = createMockOrganizations();
  let eventBus: EventBus;
  let repo;

  let addOrganizationEventHandler: AddOrganizationEventHandler;
  let editOrganizationEventHandler: EditOrganizationEventHandler;
  let deleteOrganizationEventHandler: DeleteOrganizationEventHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...OrganizationEventHandlers,
        {
          provide: OrganizationRepository,
          useFactory: mockOrganizationRepository,
        },
      ],
    }).compile();
    eventBus = module.get<EventBus>(EventBus);
    repo = module.get<OrganizationRepository>(OrganizationRepository);

    addOrganizationEventHandler = module.get<AddOrganizationEventHandler>(
      AddOrganizationEventHandler,
    );
    editOrganizationEventHandler = module.get<EditOrganizationEventHandler>(
      EditOrganizationEventHandler,
    );
    deleteOrganizationEventHandler = module.get<DeleteOrganizationEventHandler>(
      DeleteOrganizationEventHandler,
    );
  });

  describe(' for AddOrganizationEventHandler', () => {
    it('should successfully return an array of organizations', async () => {
      repo.add.mockResolvedValue(null);
      eventBus.register([AddOrganizationEventHandler]);
      expect(repo.add).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationAddedEvent(
          mockOrganizations[0]._id,
          mockOrganizations[0],
          mockMeta,
        ),
      );
      expect(repo.add).toHaveBeenCalledWith(mockOrganizations[0], mockMeta);
    });

    it('should log the error if repo.add() fails', async () => {
      const logger = (addOrganizationEventHandler as any).logger;
      repo.add.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([AddOrganizationEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.add).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationAddedEvent(
          mockOrganizations[0]._id,
          mockOrganizations[0],
          mockMeta,
        ),
      );
      expect(repo.add).toHaveBeenCalledWith(mockOrganizations[0], mockMeta);
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe(' for EditOrganizationEventHandler', () => {
    it('should successfully repo.updatedById() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([EditOrganizationEventHandler]);
      expect(repo.edit).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationEditedEvent(
          mockOrganizations[0]._id,
          mockOrganizations[0],
          mockMeta,
        ),
      );
      expect(repo.edit).toHaveBeenCalledWith(
        mockOrganizations[0]._id,
        mockOrganizations[0],
        mockMeta,
      );
    });

    it('should log the error if repo.edit() fails', async () => {
      const logger = (editOrganizationEventHandler as any).logger;
      repo.edit.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([EditOrganizationEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.edit).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationEditedEvent(
          mockOrganizations[0]._id,
          mockOrganizations[0],
          mockMeta,
        ),
      );
      expect(repo.edit).toHaveBeenCalledWith(
        mockOrganizations[0]._id,
        mockOrganizations[0],
        mockMeta,
      );
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe(' for DeleteOrganizationEventHandler', () => {
    it('should successfully repo.delete() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([DeleteOrganizationEventHandler]);
      expect(repo.delete).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationDeletedEvent(mockOrganizations[0]._id, mockMeta),
      );
      expect(repo.delete).toHaveBeenCalledWith(
        mockOrganizations[0]._id,
        mockMeta,
      );
    });

    it('should log the error if repo.delete() fails', async () => {
      const logger = (deleteOrganizationEventHandler as any).logger;
      repo.delete.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([DeleteOrganizationEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.delete).not.toHaveBeenCalled();
      await eventBus.publish(
        new OrganizationDeletedEvent(mockOrganizations[0]._id, mockMeta),
      );
      expect(repo.delete).toHaveBeenCalledWith(mockOrganizations[0]._id, mockMeta);
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });
});
