import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { PersonUnrelatedFromOrganizationEvent } from '../impl';

@EventsHandler(PersonUnrelatedFromOrganizationEvent)
export class PersonUnrelatedFromOrganizationEventHandler
  implements IEventHandler<PersonUnrelatedFromOrganizationEvent> {
  private logger: Logger;
  constructor(private readonly repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async handle(event: PersonUnrelatedFromOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _organizationID, _personID, meta } = event;
    try {
      await this.repository.unrelatePerson(_organizationID, _personID, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
