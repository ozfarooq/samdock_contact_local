import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from './../../repositories/organization.repository';
import { LeadRelatedToOrganizationEvent } from '../impl';

@EventsHandler(LeadRelatedToOrganizationEvent)
export class LeadRelatedToOrganizationEventHandler
  implements IEventHandler<LeadRelatedToOrganizationEvent> {
  private logger: Logger;
  constructor(private readonly repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async handle(event: LeadRelatedToOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _leadID, meta } = event;
    try {
      await this.repository.relateDeal(_organizationID, _leadID, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
