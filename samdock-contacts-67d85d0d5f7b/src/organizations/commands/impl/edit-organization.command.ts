import { ICommand } from '@nestjs/cqrs';
import { IOrganization } from '../../models/organization.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditOrganizationCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly organization: Partial<IOrganization>,
    public readonly meta: EventMetaData,
  ) {}
}
