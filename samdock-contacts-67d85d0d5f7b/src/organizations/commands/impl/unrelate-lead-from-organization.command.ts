import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class UnrelateLeadFromOrganizationCommand implements ICommand {
  constructor(
    public organizationID: string,
    public leadID: string,
    public meta: EventMetaData,
  ) {}
}
