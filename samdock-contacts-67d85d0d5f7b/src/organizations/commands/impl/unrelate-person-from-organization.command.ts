import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class UnrelatePersonFromOrganizationCommand implements ICommand {
  constructor(
    public _organizationID: string,
    public _personID: string,
    public meta: EventMetaData,
  ) {}
}
