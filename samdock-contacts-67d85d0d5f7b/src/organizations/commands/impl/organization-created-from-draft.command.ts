import { ICommand } from '@nestjs/cqrs';
import { IContactDraft } from '@daypaio/domain-events/contact-drafts';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class OrganizationCreatedFromDraftCommand implements ICommand {
  constructor(
    public _id: string,
    public data: IContactDraft,
    public meta: EventMetaData,
  ) {}
}
