import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateLeadToOrganizationCommand implements ICommand {
  constructor(
    public organizationID: string,
    public leadID: string,
    public meta: EventMetaData,
  ) { }
}
