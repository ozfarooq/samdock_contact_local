import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateDealToOrganizationCommand implements ICommand {
  constructor(
    public organizationID: string,
    public dealID: string,
    public meta: EventMetaData,
  ) {}
}
