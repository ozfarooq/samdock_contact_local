import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { DeleteBranchOfOrganizationCommand } from '../impl/delete-branch-of-organization.command';

@CommandHandler(DeleteBranchOfOrganizationCommand)
export class DeleteBranchOfOrganizationCommandHandler
  implements ICommandHandler<DeleteBranchOfOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteBranchOfOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteBranchOfOrganizationCommandHandler...');
    const { organizationID, branchID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.deleteBranch(branchID, meta);
    organizationAggregate.commit();
  }
}
