import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ORelatedLeadQualifiedCommand } from '../impl/related-lead-qualified.command';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { OrganizationRelatedLeadQualifiedEvent } from '@daypaio/domain-events/organizations';

@CommandHandler(ORelatedLeadQualifiedCommand)
export class ORelatedLeadQualifiedCommandHandler
  implements ICommandHandler<ORelatedLeadQualifiedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly organizationRepository: OrganizationRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ORelatedLeadQualifiedCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _leadID, meta } = command;
    const organizations = await this.organizationRepository.browseByDeals(_leadID, meta);
    for (const organization of organizations) {
      const aggregate = this.publisher.mergeObjectContext(
        new OrganizationAggregate(organization),
      );
      aggregate.apply(new OrganizationRelatedLeadQualifiedEvent(organization._id, _leadID, meta));
      aggregate.commit();
    }
  }
}
