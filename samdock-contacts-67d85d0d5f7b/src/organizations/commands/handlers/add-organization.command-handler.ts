import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { AddOrganizationCommand } from '../impl/add-organization.command';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(AddOrganizationCommand)
export class AddOrganizationCommandHandler
  implements ICommandHandler<AddOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: AddOrganizationCommandHandler...');
    const { organization, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate(organization),
    );
    organizationAggregate.add(meta);
    organizationAggregate.commit();
  }
}
