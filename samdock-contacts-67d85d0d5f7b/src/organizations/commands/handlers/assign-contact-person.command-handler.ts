import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { AssignContactPersonCommand } from '../impl/assign-contact-person.command';

@CommandHandler(AssignContactPersonCommand)
export class AssignContactPersonCommandHandler
  implements ICommandHandler<AssignContactPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AssignContactPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: AssignContactPersonCommand...');
    const { _organizationID, _personID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.assignContactPerson(_personID, meta);
    organizationAggregate.commit();
  }
}
