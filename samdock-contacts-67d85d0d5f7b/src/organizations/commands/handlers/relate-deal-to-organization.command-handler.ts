import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { RelateDealToOrganizationCommand } from '../impl/relate-deal-to-organization.command';

@CommandHandler(RelateDealToOrganizationCommand)
export class RelateDealToOrganizationCommandHandler
  implements ICommandHandler<RelateDealToOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateDealToOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: DealRelatedToOrganizationCommand...');
    const { organizationID, dealID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.relateDeal(dealID, meta);
    organizationAggregate.commit();
  }
}
