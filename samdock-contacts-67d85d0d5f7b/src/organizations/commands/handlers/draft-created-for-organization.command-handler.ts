import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftCreatedForOrganizationCommand } from '../impl/draft-created-for-organization.command';
import { DraftCreatedForOrganizationEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(DraftCreatedForOrganizationCommand)
export class DraftCreatedForOrganizationCommandHandler
  implements ICommandHandler<DraftCreatedForOrganizationCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: DraftCreatedForOrganizationCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: DraftCreatedForOrganizationCommandHandler...',
    );
    const { _id, data, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id }),
    );
    organizationAggregate.apply(new DraftCreatedForOrganizationEvent(_id, data, meta));
    organizationAggregate.commit();
  }
}
