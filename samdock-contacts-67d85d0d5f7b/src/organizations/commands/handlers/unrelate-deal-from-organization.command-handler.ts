import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { UnrelateDealFromOrganizationCommand } from '../impl/unrelate-deal-from-organization.command';

@CommandHandler(UnrelateDealFromOrganizationCommand)
export class UnrelateDealFromOrganizationCommandHandler
  implements ICommandHandler<UnrelateDealFromOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateDealFromOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelateDealFromOrganizationCommand...');
    const { organizationID, dealID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.unrelateDeal(dealID, meta);
    organizationAggregate.commit();
  }
}
