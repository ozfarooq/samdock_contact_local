import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationRelatedDealWonEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { RelatedDealWonCommand } from '../impl/related-deal-won.command';

@CommandHandler(RelatedDealWonCommand)
export class RelatedDealWonCommandHandler
  implements ICommandHandler<RelatedDealWonCommand> {
  private logger: Logger;
  constructor(
    private readonly organizationRepository: OrganizationRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelatedDealWonCommand) {
    this.logger.log('COMMAND TRIGGERED orgs');
    const { _dealID, stage, meta } = command;
    const organizations = await this.organizationRepository.browseByDeals(_dealID, meta);
    for (const organization of organizations) {
      const aggregate = this.publisher.mergeObjectContext(
        new OrganizationAggregate(organization),
      );
      aggregate.apply(new OrganizationRelatedDealWonEvent(
        organization._id,
        _dealID,
        stage,
        meta,
      ));
      aggregate.commit();
    }
  }
}
