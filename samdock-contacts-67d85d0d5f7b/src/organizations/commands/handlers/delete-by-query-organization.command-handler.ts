import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DeleteByQueryOrganizationCommand } from '../impl/delete-by-query-organization.command';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(DeleteByQueryOrganizationCommand)
export class DeleteByQueryOrganizationCommandHandler
  implements ICommandHandler<DeleteByQueryOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteByQueryOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteByQueryOrganizationCommandHandler...');
    const { query, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate(),
    );
    organizationAggregate.deleteByQuery(query, meta);
    organizationAggregate.commit();
  }
}
