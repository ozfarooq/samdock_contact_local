import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRelatedTaskCompletedCommand }
  from '../impl/organization-related-task-completed.command';
import { OrganizationRelatedTaskCompletedEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(OrganizationRelatedTaskCompletedCommand)
export class OrganizationRelatedTaskCompletedCommandHandler
  implements ICommandHandler<OrganizationRelatedTaskCompletedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: OrganizationRelatedTaskCompletedCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: OrganizationRelatedTaskCompletedCommandHandler...',
    );
    const { _organizationID, _taskID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.apply(
      new OrganizationRelatedTaskCompletedEvent(_organizationID, _taskID, meta),
    );
    organizationAggregate.commit();
  }
}
