import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { ORelatedDealDemotedToLeadCommand } from '../impl/related-deal-demoted-to-lead.command';
import { OrganizationRelatedDealDemotedToLeadEvent } from '@daypaio/domain-events/organizations';

@CommandHandler(ORelatedDealDemotedToLeadCommand)
export class ORelatedDealDemotedToLeadCommandCommandHandler
  implements ICommandHandler<ORelatedDealDemotedToLeadCommand> {
  private logger: Logger;
  constructor(
    private readonly publisher: EventPublisher,
    private readonly organizationRepository: OrganizationRepository,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: ORelatedDealDemotedToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, meta } = command;
    const organizations = await this.organizationRepository.browseByDeals(_dealID, meta);
    for (const organization of organizations) {
      const aggregate = this.publisher.mergeObjectContext(
        new OrganizationAggregate(organization),
      );
      aggregate.apply(new OrganizationRelatedDealDemotedToLeadEvent(
        organization._id,
        _dealID,
        meta,
      ));
      aggregate.commit();
    }

  }
}
