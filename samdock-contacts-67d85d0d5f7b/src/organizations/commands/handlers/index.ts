import { AddOrganizationCommandHandler } from './add-organization.command-handler';
import { DeleteOrganizationCommandHandler } from './delete-organization.command-handler';
import { DeleteByQueryOrganizationCommandHandler } from './delete-by-query-organization.command-handler';
import { EditOrganizationCommandHandler } from './edit-organization.command-handler';
import {
  AddBranchToOrganizationCommandHandler,
} from './add-branch-to-organization.command-handler';
import {
  EditBranchOfOrganizationCommandHandler,
} from './edit-branch-of-organization.command-handler';
import { DeleteBranchOfOrganizationCommandHandler } from './delete-branch-of-organization.command-handler';
import { RelatePersonToOrganizationCommandHandler } from './relate-person-to-organization.command-handler';
import { AssignContactPersonCommandHandler } from './assign-contact-person.command-handler';
import { UnrelatePersonFromOrganizationCommandHandler } from './unrelate-person-from-organization.command-handler';
import { RelateDealToOrganizationCommandHandler } from './relate-deal-to-organization.command-handler';
import { UnrelateDealFromOrganizationCommandHandler } from './unrelate-deal-from-organization.command-handler';
import { RelatedDealWonCommandHandler } from './related-deal-won.command-handler';
import { RelatedDealStageChangedCommandHandler } from './related-deal-status-changed.command-handler';
import { RelatedDealLostCommandHandler } from './related-deal-lost.command-handler';
import { OrganizationCreatedFromDraftCommandHandler } from './organization-created-from-draft.command-handler';
import { DraftCreatedForOrganizationCommandHandler } from './draft-created-for-organization.command-handler';
import { ChangeEmployeeRelationRoleCommandHandler } from './change-employee-relation-role.command-handler';
import { RelateLeadToOrganizationCommandHandler } from './relate-lead-to-organization.command-handler';
import { UnrelateLeadFromOrganizationCommandHandler } from './unrelate-lead-from-organization.command-handler';
import { ORelatedDealDemotedToLeadCommandCommandHandler } from './related-deal-demoted-to-lead.command-handler';
import { ORelatedLeadPromotedToDealCommandCommandHandler } from './related-lead-promoted-to-deal.command-handler';
import { ORelatedLeadQualifiedCommandHandler } from './related-lead-qualified.command-handler';
import { OrganizationRelatedTaskCompletedCommandHandler } from './organization-related-task-completed.command-handler';
import { OrganizationRelatedTaskReopenedCommandHandler } from './organization-related-task-reopened.command-handler';
import { TaskLinkedToOrganizationCommandHandler } from './task-linked-to-organization.command-handler';
import { TaskUnlinkedFromOrganizationCommandHandler } from './task-unlinked-from-organization-event.command-handler';
import { ORelatedLeadUnqualifiedCommandHandler } from './related-lead-unqualified.command-handler';

export const OrganizationCommandHandlers = [
  AddOrganizationCommandHandler,
  DeleteOrganizationCommandHandler,
  DeleteByQueryOrganizationCommandHandler,
  EditOrganizationCommandHandler,
  AddBranchToOrganizationCommandHandler,
  EditBranchOfOrganizationCommandHandler,
  DeleteBranchOfOrganizationCommandHandler,
  RelatePersonToOrganizationCommandHandler,
  UnrelatePersonFromOrganizationCommandHandler,
  AssignContactPersonCommandHandler,
  RelateDealToOrganizationCommandHandler,
  UnrelateDealFromOrganizationCommandHandler,
  RelatedDealWonCommandHandler,
  RelatedDealStageChangedCommandHandler,
  RelatedDealLostCommandHandler,
  OrganizationCreatedFromDraftCommandHandler,
  DraftCreatedForOrganizationCommandHandler,
  ChangeEmployeeRelationRoleCommandHandler,
  RelateLeadToOrganizationCommandHandler,
  UnrelateLeadFromOrganizationCommandHandler,
  ORelatedLeadPromotedToDealCommandCommandHandler,
  ORelatedDealDemotedToLeadCommandCommandHandler,
  ORelatedLeadQualifiedCommandHandler,
  OrganizationRelatedTaskCompletedCommandHandler,
  OrganizationRelatedTaskReopenedCommandHandler,
  TaskLinkedToOrganizationCommandHandler,
  TaskUnlinkedFromOrganizationCommandHandler,
  ORelatedLeadUnqualifiedCommandHandler,
];
