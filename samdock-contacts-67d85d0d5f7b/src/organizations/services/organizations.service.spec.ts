import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationsService } from './organizations.service';
import { CqrsModule, CommandBus, QueryBus, EventBus } from '@nestjs/cqrs';
import { OrganizationCommandHandlers } from '../commands/handlers';
import { OrganizationQueryHandlers } from '../queries/handlers';
import { OrganizationRepository } from '../repositories/organization.repository';
import { AddOrganizationCommandHandler } from '../commands/handlers/add-organization.command-handler';
import { EditOrganizationCommandHandler } from '../commands/handlers/edit-organization.command-handler';
import { DeleteOrganizationCommandHandler } from '../commands/handlers/delete-organization.command-handler';
import { ReadOrganizationHandler } from '../queries/handlers/read-organization.query-handler';
import { BrowseOrganizationHandler } from '../queries/handlers/browse-organization.query-handler';
import { EventBusProvider } from 'nestjs-eventstore';
import { OrganizationDTO } from '../dto/organization.dto';
import { EventMetaData } from '@daypaio/domain-events/shared';

const mockOrganizationRepository = () => ({
  create: jest.fn(),
});

describe('OrganizationsService', () => {
  let service: OrganizationsService;
  let commandBus: CommandBus;
  let queryBus: QueryBus;
  const mockMeta = new EventMetaData('mocktenant', 'xyz');
  let addOrganizationCommandHandler: AddOrganizationCommandHandler;
  let editOrganizationCommandHandler: EditOrganizationCommandHandler;
  let deleteOrganizationCommandHandler: DeleteOrganizationCommandHandler;
  let browseOrganizationQueryHandler: BrowseOrganizationHandler;
  let readOrganizationQueryHandler: ReadOrganizationHandler;

  const mockOrganization = {
    _id: 'a',
    name: 'taimoor',
    email: 'this@example.com',
    phoneNumber: 1232341432,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        OrganizationsService,
        ...OrganizationCommandHandlers,
        ...OrganizationQueryHandlers,
        {
          provide: EventBusProvider,
          useExisting: EventBus,
        },
        {
          provide: OrganizationRepository,
          useFactory: mockOrganizationRepository,
        },
      ],
    }).compile();

    service = module.get<OrganizationsService>(OrganizationsService);
    commandBus = module.get<CommandBus>(CommandBus);
    queryBus = module.get<QueryBus>(QueryBus);
    addOrganizationCommandHandler = module.get<AddOrganizationCommandHandler>(
      AddOrganizationCommandHandler,
    );
    editOrganizationCommandHandler = module.get<EditOrganizationCommandHandler>(
      EditOrganizationCommandHandler,
    );
    deleteOrganizationCommandHandler = module.get<
      DeleteOrganizationCommandHandler>(DeleteOrganizationCommandHandler);
    browseOrganizationQueryHandler = module.get<BrowseOrganizationHandler>(
      BrowseOrganizationHandler,
    );
    readOrganizationQueryHandler = module.get<ReadOrganizationHandler>(
      ReadOrganizationHandler,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('commands', () => {
    describe('for executing commands on the command bus', () => {
      it('should be executed on the commandBus for adding organization', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.add(
          new OrganizationDTO(mockOrganization),
          mockMeta,
        );
        expect(commandBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the commandBus for editing organization', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.edit(
          mockOrganization._id,
          new OrganizationDTO(mockOrganization),
          mockMeta,
        );
        expect(commandBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the commandBus for deleting organization', async () => {
        jest.spyOn(commandBus, 'execute').mockResolvedValue(null);
        expect(commandBus.execute).not.toHaveBeenCalled();
        await service.delete(mockOrganization._id, mockMeta);
        expect(commandBus.execute).toHaveBeenCalled();
      });
    });

    describe('for triggering command handlers', () => {
      it('should trigger AddOrganizationCommand when adding an organization', async () => {
        jest
          .spyOn(addOrganizationCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([AddOrganizationCommandHandler]);
        expect(addOrganizationCommandHandler.execute).not.toHaveBeenCalled();
        await service.add(
          new OrganizationDTO(mockOrganization),
          mockMeta,
        );
        expect(addOrganizationCommandHandler.execute).toHaveBeenCalled();
      });

      it('should trigger EditOrganizationCommand when editing an organization', async () => {
        jest
          .spyOn(editOrganizationCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([EditOrganizationCommandHandler]);
        expect(editOrganizationCommandHandler.execute).not.toHaveBeenCalled();
        await service.edit(
          mockOrganization._id,
          new OrganizationDTO(mockOrganization),
          mockMeta,
        );
        expect(editOrganizationCommandHandler.execute).toHaveBeenCalled();
      });

      it('should trigger DeleteOrganizationCommand when deleting an organization', async () => {
        jest
          .spyOn(deleteOrganizationCommandHandler, 'execute')
          .mockReturnValue(null);
        commandBus.register([DeleteOrganizationCommandHandler]);
        expect(deleteOrganizationCommandHandler.execute).not.toHaveBeenCalled();
        await service.delete(mockOrganization._id, mockMeta);
        expect(deleteOrganizationCommandHandler.execute).toHaveBeenCalled();
      });
    });
  });

  describe('queries', () => {
    describe('for executing queries on the QueryBus', () => {
      it('should be executed on the queryBus for browsing organizations', async () => {
        jest.spyOn(queryBus, 'execute').mockResolvedValue(null);
        expect(queryBus.execute).not.toHaveBeenCalled();
        await service.browse(mockMeta);
        expect(queryBus.execute).toHaveBeenCalled();
      });

      it('should be executed on the queryBus for finding an organization', async () => {
        jest.spyOn(queryBus, 'execute').mockResolvedValue(null);
        expect(queryBus.execute).not.toHaveBeenCalled();
        await service.read(mockOrganization._id, mockMeta);
        expect(queryBus.execute).toHaveBeenCalled();
      });
    });

    describe('for triggering query handlers', () => {
      it('should trigger BrowseOrganizationQuery when browsing organizations', async () => {
        jest
          .spyOn(browseOrganizationQueryHandler, 'execute')
          .mockReturnValue(null);
        queryBus.register([BrowseOrganizationHandler]);
        expect(browseOrganizationQueryHandler.execute).not.toHaveBeenCalled();
        await service.browse(mockMeta);
        expect(browseOrganizationQueryHandler.execute).toHaveBeenCalled();
      });

      it('should trigger ReadOrganizationQuery when reading  an organizations', async () => {
        jest
          .spyOn(readOrganizationQueryHandler, 'execute')
          .mockReturnValue(null);
        queryBus.register([ReadOrganizationHandler]);
        expect(readOrganizationQueryHandler.execute).not.toHaveBeenCalled();
        await service.read(mockOrganization._id, mockMeta);
        expect(readOrganizationQueryHandler.execute).toHaveBeenCalled();
      });
    });
  });
});
