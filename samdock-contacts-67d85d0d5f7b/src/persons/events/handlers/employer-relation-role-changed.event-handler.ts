import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { EmployerRelationRoleChangedEvent } from '../impl';

@EventsHandler(EmployerRelationRoleChangedEvent)
export class EmployerRelationRoleChangedEventHandler
  implements IEventHandler<EmployerRelationRoleChangedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) { }
  async handle(event: EmployerRelationRoleChangedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _personID, _employerID, role, meta } = event;
    try {
      const result = await this.repository.changeEmployerRelationRole(
        _personID,
        _employerID,
        role,
        meta,
      );
      return result;
    } catch (error) {
      this.logger.error(`Failed to update employer ${_employerID} of person ${_personID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
