import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonDeletedByQueryEvent } from '../impl';

@EventsHandler(PersonDeletedByQueryEvent)
export class DeleteByQueryPersonEventHandler
  implements IEventHandler<PersonDeletedByQueryEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) {}
  async handle(event: PersonDeletedByQueryEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { query, meta } = event;
    try {
      const result = await this.repository.deleteByQuery(query, meta);
      return result;
    } catch (error) {
      this.logger.error(`Cannot delete person of query ${query}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event possibly
    }
  }
}
