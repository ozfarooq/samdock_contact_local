import { AddPersonEventHandler } from './add-person.event-handler';
import { DeletePersonEventHandler } from './delete-person.event-handler';
import { EditPersonEventHandler } from './edit-person.event-handler';
import { OrganizationUnrelatedFromPersonEventHandler } from './organization-unrelated-from-person.event-handler';
import { OrganizationRelatedToPersonEventHandler } from './organization-related-to-person.event-handler';
import { DealRelatedToPersonEventHandler } from './deal-related-to-person.event-handler';
import { AssignedToDealAsContactPersonEventHandler } from './assigned-to-deal-as-contact-person.event-handler';
import { RemovedFromDealAsContactPersonEventHandler } from './removed-from-deal-as-contact-person.event-handler';
import { DealUnrelatedFromPersonEventHandler } from './deal-unrelated-from-person.event-handler';
import { PersonCreatedFromDraftEventHandler } from './person-created-from-draft.event-handler';
import { EmployerRelationRoleChangedEventHandler } from './employer-relation-role-changed.event-handler';
import { LeadRelatedToPersonEventHandler } from './lead-related-to-person.event-handler';
import { LeadUnrelatedFromPersonEventHandler } from './lead-unrelated-from-person.event-handler';
import { DeleteByQueryPersonEventHandler } from './delete-by-query-person.event-handler';
import { PersonDOIGrantedEventHandler } from './person-doi-granted.event-handler';
import { TenantDeletedEventHandler } from './tenant-deleted.event-handler';
import { PDeleteOrganizationEventHandler } from './delete-organization.event-handler';
import { ActivityAddedEventHandler } from './activity-add-event-handler';

export const PersonEventHandlers = [
  AddPersonEventHandler,
  DeletePersonEventHandler,
  EditPersonEventHandler,
  DeleteByQueryPersonEventHandler,
  OrganizationRelatedToPersonEventHandler,
  OrganizationUnrelatedFromPersonEventHandler,
  DealRelatedToPersonEventHandler,
  DealUnrelatedFromPersonEventHandler,
  AssignedToDealAsContactPersonEventHandler,
  RemovedFromDealAsContactPersonEventHandler,
  PersonCreatedFromDraftEventHandler,
  EmployerRelationRoleChangedEventHandler,
  LeadRelatedToPersonEventHandler,
  LeadUnrelatedFromPersonEventHandler,
  PersonDOIGrantedEventHandler,
  TenantDeletedEventHandler,
  PDeleteOrganizationEventHandler,
  ActivityAddedEventHandler,
];
