import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { DealUnrelatedFromPersonEvent } from '../impl';

@EventsHandler(DealUnrelatedFromPersonEvent)
export class DealUnrelatedFromPersonEventHandler
  implements IEventHandler<DealUnrelatedFromPersonEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) {}
  async handle(event: DealUnrelatedFromPersonEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _personID, _dealID, meta } = event;
    try {
      const result = await this.repository.unrelateFromDeal(_personID, _dealID, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to update person of id: ${_personID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
