import { OrganizationUnrelatedFromPersonEvent } from '../impl';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';

@EventsHandler(OrganizationUnrelatedFromPersonEvent)
export class OrganizationUnrelatedFromPersonEventHandler
  implements IEventHandler<OrganizationUnrelatedFromPersonEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) {}
  async handle(event: OrganizationUnrelatedFromPersonEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _organizationID, _personID, meta } = event;
    try {
      const result = await this.repository.unrelateFromOrganization(
          _personID,
          _organizationID,
          meta,
        );
      return result;
    } catch (error) {
      this.logger.error(`Failed to update person of id: ${_personID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
