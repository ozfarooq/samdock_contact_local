import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { OrganizationDeletedEvent } from '@daypaio/domain-events/organizations';

@EventsHandler(OrganizationDeletedEvent)
export class PDeleteOrganizationEventHandler
  implements IEventHandler<OrganizationDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) {}
  async handle(event: OrganizationDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.repository.unrelateAllFromOrganization(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete organization of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
