import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class FindPersonQuery implements IQuery {
  constructor(public query, public meta: EventMetaData) {}
}
