import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { FindPersonQuery } from '../impl/find-person.query';
import { Person } from '../../models/person.model';
import { PersonRepository } from '../../repositories/person.repository';

@QueryHandler(FindPersonQuery)
export class FindPersonHandler implements IQueryHandler<FindPersonQuery> {
  private logger: Logger;
  constructor(private repository: PersonRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(findPersonQuery: FindPersonQuery): Promise<Person[]> {
    this.logger.log('Async BrowseHandler...');
    try {
      const { query, meta } = findPersonQuery;
      const result = await this.repository.find(query, meta);
      return result;
    } catch (error) {
      this.logger.error('Failed to browse on Person');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
