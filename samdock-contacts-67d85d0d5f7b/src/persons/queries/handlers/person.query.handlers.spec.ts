import { Person } from '../../models/person.model';
import { QueryBus, CqrsModule } from '@nestjs/cqrs';
import { TestingModule, Test } from '@nestjs/testing';
import { PersonQueryHandlers } from '.';
import { PersonRepository } from '../../repositories/person.repository';
import { BrowsePersonHandler } from './browse-person.query-handler';
import { BrowsePersonQuery } from '../impl/browse-person.query';
import { NotFoundException } from '@nestjs/common';
import { ReadPersonHandler } from './read-person.query-handler';
import { ReadPersonQuery } from '../impl/read-person.query';
import { EventMetaData } from '@daypaio/domain-events/shared';

const mockPersonRespository = () => ({
  browse: jest.fn(),
  read: jest.fn(),
});

const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockPersons(): Person[] {
  const firstPerson = new Person({
    _id: 'org1',
    firstName: 'T',
    lastName: 'Alam',
    email: 't@example.com',
    phoneNumber: '12345',
  });

  const secondPerson = new Person({
    _id: 'org2',
    firstName: 'J',
    lastName: 'Grosch',
    email: 'j@example.com',
    phoneNumber: '12345',
  });

  const arrayOfPersons: Person[] = [];
  arrayOfPersons.push(firstPerson);
  arrayOfPersons.push(secondPerson);

  return arrayOfPersons;
}

describe('PersonQueryHandlers', () => {
  const mockPersons: Person[] = createMockPersons();
  let queryBus: QueryBus;
  let repo;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...PersonQueryHandlers,
        {
          provide: PersonRepository,
          useFactory: mockPersonRespository,
        },
      ],
    }).compile();
    queryBus = module.get<QueryBus>(QueryBus);
    repo = module.get<PersonRepository>(PersonRepository);
  });

  describe(' for BrowseQueryHandler', () => {
    it('should successfully return an array of persons', async () => {
      repo.browse.mockResolvedValue(mockPersons);
      queryBus.register([BrowsePersonHandler]);
      expect(repo.browse).not.toHaveBeenCalled();
      const result = await queryBus.execute(new BrowsePersonQuery(mockMeta));
      expect(repo.browse).toHaveBeenCalledWith(mockMeta);
      expect(result).toEqual(mockPersons);
    });

    it('should throw InternatServerException on the failure of repository.browse()', async () => {
      repo.browse.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([BrowsePersonHandler]);
      expect(repo.browse).not.toHaveBeenCalled();
      expect(
        queryBus.execute(new BrowsePersonQuery(mockMeta)),
      ).rejects.toThrow(Error);
      expect(repo.browse).toHaveBeenCalledWith(mockMeta);
    });
  });

  describe(' for ReadQueryHandler', () => {
    it('should successfully return a person on repo.read()', async () => {
      repo.read.mockResolvedValue(mockPersons[0]);
      queryBus.register([ReadPersonHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      const result = await queryBus.execute(
        new ReadPersonQuery(mockPersons[0]._id, mockMeta),
      );
      expect(repo.read).toHaveBeenCalledWith(mockPersons[0]._id, mockMeta);
      expect(result).toEqual(mockPersons[0]);
    });

    it('should throw NotFoundException on repo.read()', async () => {
      repo.read.mockReturnValue(
        Promise.reject(
          new NotFoundException('This is an auto generated error'),
        ),
      );
      queryBus.register([ReadPersonHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(new ReadPersonQuery(mockPersons[0]._id, mockMeta)),
      ).rejects.toThrow(NotFoundException);
      expect(repo.read).toHaveBeenCalledWith(mockPersons[0]._id, mockMeta);
    });

    it('should throw InternalServerException on repo.read() failing', async () => {
      repo.read.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([ReadPersonHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(new ReadPersonQuery(mockPersons[0]._id, mockMeta)),
      ).rejects.toThrow(Error);
      expect(repo.read).toHaveBeenCalledWith(mockPersons[0]._id, mockMeta);
    });
  });
});
