import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonRelatedToLeadEvent, PersonUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';
import { RelateLeadToPersonCommand } from '../commands/impl/relate-lead-to-person.command';
import { UnrelateLeadFromPersonCommand } from '../commands/impl/unrelate-lead-from-person.command';

export class NormalizePersonToLeadRelationSaga {
  @Saga()
  relationCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonRelatedToLeadEvent),
      map(
        (event: PersonRelatedToLeadEvent) => {
          const { _leadID, _personID, meta } = event;
          return new RelateLeadToPersonCommand(_personID, _leadID,  meta);
        },
      ),
    );
  }

  @Saga()
  relationDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonUnrelatedFromLeadEvent),
      map(
        (event: PersonUnrelatedFromLeadEvent) => {
          const { _leadID, _personID, meta } = event;
          return new UnrelateLeadFromPersonCommand(_personID, _leadID,  meta);
        },
      ),
    );
  }

}
