import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable, Subject } from 'rxjs';
import { DraftVerifiedEvent } from '@daypaio/domain-events/contact-drafts';
import { DraftCreatedForPersonCommand } from '../commands/impl/draft-created-for-person.command';
import { PersonCreatedFromDraftCommand } from '../commands/impl/person-created-from-draft.command';
import { ContactDraftDTO } from '../../drafts/dtos/contact-draft.dto';

export class PersonCreatedFromDraftSaga {

  @Saga()
  personCreatedFromDraft = (events$: Observable<any>): Observable<ICommand> => {
    const subject: Subject<ICommand> = new Subject();
    events$.pipe(
      ofType(DraftVerifiedEvent),
    ).subscribe((event: DraftVerifiedEvent) => {
      const { _id, data, meta } = event;
      const contactDraft = new ContactDraftDTO(data);
      // check any of persons fields exists
      if (!contactDraft?.hasAnyPersonField) {
        return;
      }
      data.verifiedAt = meta.timestamp;
      subject.next(new DraftCreatedForPersonCommand(_id, data, {
        _tenantID: meta._tenantID,
        _userID: undefined,
        timestamp: data.createdAt,
      }));
      subject.next(new PersonCreatedFromDraftCommand(_id, data, {
        _tenantID: meta._tenantID,
        _userID: meta._userID,
        timestamp: data.verifiedAt,
      }));
    });

    return subject.asObservable();
  }
}
