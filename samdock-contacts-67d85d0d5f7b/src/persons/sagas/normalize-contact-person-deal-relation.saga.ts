import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AssignToDealAsContactPersonCommand } from '../commands/impl/assign-to-deal-as-contact-person.command';
import { RemoveFromDealAsContactPersonCommand } from '../commands/impl/remove-from-deal-as-contact-person.command';
import { ContactPersonAssignedToDealEvent, ContactPersonRemovedFromDealEvent } from '@daypaio/domain-events/deals';

export class NormalizeContactPersonDealRelationSaga {
  @Saga()
  contactPersonAssigned = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(ContactPersonAssignedToDealEvent),
      map((event: ContactPersonAssignedToDealEvent) => {
        const { _dealID, _personID, meta } = event;
        return new AssignToDealAsContactPersonCommand(_personID, _dealID, meta);
      }),
    );
  }

  @Saga()
  contactPersonRemoved = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(ContactPersonRemovedFromDealEvent),
      map(
        (event: ContactPersonRemovedFromDealEvent) => {
          const { _dealID, _personID, meta } = event;
          return new RemoveFromDealAsContactPersonCommand(_personID, _dealID,  meta);
        },
      ),
    );
  }
}
