import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { PRelatedDealStageChangedCommand } from '../commands/impl/related-deal-status-changed.command';
import { DealStageChangedEvent } from '@daypaio/domain-events/deals';

export class PRelatedDealStageChangedSaga {

  @Saga()
  pRelatedDealStageChanged = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealStageChangedEvent),
      filter(event => event.constructor.name === 'DealStageChangedEvent'),
      map(
        (event: DealStageChangedEvent) => {
          const { _dealID, stage, meta } = event;
          return new PRelatedDealStageChangedCommand(_dealID, stage,  meta);
        },
      ),
    );
  }

}
