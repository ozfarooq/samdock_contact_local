import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContactPersonAssignedToLeadEvent, ContactPersonRemovedFromLeadEvent } from '@daypaio/domain-events/deals';
import { AssignToLeadAsContactPersonCommand } from '../commands/impl/assign-to-lead-as-contact-person.command';
import { RemoveFromLeadAsContactPersonCommand } from '../commands/impl/remove-from-lead-as-contact-person.command';

export class NormalizeContactPersonLeadRelationSaga {
  @Saga()
  contactPersonAssigned = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(ContactPersonAssignedToLeadEvent),
      map((event: ContactPersonAssignedToLeadEvent) => {
        const { _dealID, _personID, meta } = event;
        return new AssignToLeadAsContactPersonCommand(_personID, _dealID, meta);
      }),
    );
  }

  @Saga()
  contactPersonRemoved = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(ContactPersonRemovedFromLeadEvent),
      map(
        (event: ContactPersonRemovedFromLeadEvent) => {
          const { _dealID, _personID, meta } = event;
          return new RemoveFromLeadAsContactPersonCommand(_personID, _dealID,  meta);
        },
      ),
    );
  }
}
