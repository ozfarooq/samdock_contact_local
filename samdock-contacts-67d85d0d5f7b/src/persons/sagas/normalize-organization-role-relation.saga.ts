import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmployeeRelationRoleChangedEvent } from '@daypaio/domain-events/organizations';
import { ChangeEmployerRelationRoleCommand } from '../commands/impl/change-employer-relation-role.command';

export class NormalizeOrganizationRoleChangedSaga {
  @Saga()
  employerRoleChangedRelation = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(EmployeeRelationRoleChangedEvent),
      map((event: EmployeeRelationRoleChangedEvent) => {
        const { _organizationID, _employeeID, role, meta } = event;
        return new ChangeEmployerRelationRoleCommand(_employeeID, _organizationID, role, meta);
      }),
    );
  }

}
