import { Controller, Get, Param, Patch } from '@nestjs/common';
import { ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { PersonLastSeenService } from '../../services/person-last-seen/person-last-seen.service';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import { ILastSeen } from '../../models/last-seen.model';

@Controller('persons/last-seen')
export class PersonLastSeenController {
  constructor(private readonly service: PersonLastSeenService) {}

  @Get()
  @ApiOperation({
    summary: 'Read lastSeen',
    description: 'Read lastSeen persons of current user',
  })
  @ApiOkResponse({
    description: 'LastSeen persons of current user provided',
  })
  read(
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.read(user._id, user.generateMeta());
  }

  @Patch('/:id')
  @ApiOperation({
    summary: 'Add lastSeen',
    description: 'Add single lastSeen',
  })
  @ApiOkResponse({
    description: 'The lastSeen adding or editing existing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Param('id') personId: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    const lastSeen = { _id: user._id, _personIDs: [personId] } as ILastSeen;
    await this.service.add(lastSeen, user.generateMeta());
  }
}
