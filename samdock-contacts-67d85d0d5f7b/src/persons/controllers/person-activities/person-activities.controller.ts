import { Controller, Get, Param } from '@nestjs/common';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import {
  PersonActivitiesService,
} from '../../services/person-activities/person-activities.service';

@Controller('persons/:id/activities')
export class PersonActivitiesController {
  constructor(private readonly service: PersonActivitiesService) {}

  @Get()
  browseActivities(
    @GetUser() user: LoggedInUser,
    @Param('id') personID: string,
  ) {
    return this.service.browseActivities(personID, user.generateMeta());
  }

}
