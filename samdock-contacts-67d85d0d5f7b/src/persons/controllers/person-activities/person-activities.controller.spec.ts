import { Test, TestingModule } from '@nestjs/testing';
import { PersonActivitiesController } from './person-activities.controller';
import {
   PersonActivitiesService,
 } from '../../services/person-activities/person-activities.service';

const mockPersonActivitiesService = () => ({});

describe('PersonActivities Controller', () => {
  let controller: PersonActivitiesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PersonActivitiesController],
      providers: [{ provide: PersonActivitiesService, useValue: mockPersonActivitiesService }],
    }).compile();

    controller = module.get<PersonActivitiesController>(PersonActivitiesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
