import { Module } from '@nestjs/common';
import { PersonsController } from './controllers/persons.controller';
import { PersonsService } from './services/persons.service';
import { Person, PersonAddress } from './models/person.model';
import { TypegooseModule } from 'nestjs-typegoose';
import { PersonRepository } from './repositories/person.repository';
import { PersonCommandHandlers } from './commands/handlers';
import { PersonQueryHandlers } from './queries/handlers';
import { PersonEventHandlers } from './events/handlers';
import { PersonSagas } from './sagas';
import { PersonActivitiesController } from './controllers/person-activities/person-activities.controller';
import { PersonActivitiesService } from './services/person-activities/person-activities.service';
import { OrganizationsModule } from '../organizations/organizations.module';
import { DraftsModule } from '../drafts/drafts.module';
import { InboundSourcesModule } from '../inbound-sources/inbound-sources.module';
import { PersonLastSeenService } from './services/person-last-seen/person-last-seen.service';
import { PersonLastSeenController } from './controllers/person-last-seen/person-last-seen.controller';
import { LastSeen } from './models/last-seen.model';
import { CustomFieldsModule } from '../custom-fields/custom-fields.module';
import { ContactViewsModule } from '../contact-views/contact-views.module';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Person,
        schemaOptions: {
          collection: 'persons',
        },
      },
      PersonAddress,
      {
        typegooseClass: LastSeen,
        schemaOptions: {
          collection: 'last-seen',
        },
      },
    ]),
    OrganizationsModule,
    DraftsModule,
    InboundSourcesModule,
    CustomFieldsModule,
    ContactViewsModule,
  ],
  controllers: [
    PersonLastSeenController,
    PersonsController,
    PersonActivitiesController,
  ],
  providers: [
    PersonsService,
    PersonRepository,
    PersonActivitiesService,
    PersonLastSeenService,
    ...PersonQueryHandlers,
    ...PersonCommandHandlers,
    ...PersonEventHandlers,
    ...PersonSagas,
  ],
  exports: [
    PersonRepository,
    PersonsService,
    PersonLastSeenService,
  ],
})
export class PersonsModule {}
