import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ChangeEmployerRelationRoleCommand implements ICommand {
  constructor(
    public _personID: string,
    public _organizationID: string,
    public role: string,
    public meta: EventMetaData,
  ) { }
}
