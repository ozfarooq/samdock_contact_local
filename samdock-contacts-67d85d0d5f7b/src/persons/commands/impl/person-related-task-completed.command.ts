import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PersonRelatedTaskCompletedCommand implements ICommand {
  constructor(
    public _personID: string,
    public _taskID: string,
    public meta: EventMetaData,
  ) { }
}
