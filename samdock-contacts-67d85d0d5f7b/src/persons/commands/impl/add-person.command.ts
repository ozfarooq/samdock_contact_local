import { ICommand } from '@nestjs/cqrs';
import { IPerson } from '../../models/person.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AddPersonCommand implements ICommand {
  constructor(
    public readonly person: IPerson,
    public readonly meta: EventMetaData,
  ) {}
}
