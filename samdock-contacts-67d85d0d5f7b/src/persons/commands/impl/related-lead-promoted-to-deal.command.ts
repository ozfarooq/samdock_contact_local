import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PRelatedLeadPromotedToDealCommand implements ICommand {
  constructor(
    public readonly _dealID: string,
    public readonly meta: EventMetaData,
  ) { }
}
