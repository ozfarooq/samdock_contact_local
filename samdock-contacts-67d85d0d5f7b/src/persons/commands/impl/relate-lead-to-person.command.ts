import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateLeadToPersonCommand implements ICommand {
  constructor(
    public readonly _personID: string,
    public readonly _leadID: string,
    public readonly meta: EventMetaData,
  ) { }
}
