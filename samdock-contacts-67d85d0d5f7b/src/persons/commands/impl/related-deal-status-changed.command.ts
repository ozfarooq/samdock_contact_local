import { ICommand } from '@nestjs/cqrs';
import { IDealStage } from '@daypaio/domain-events/deals';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PRelatedDealStageChangedCommand implements ICommand {
  constructor(
    public readonly _dealID: string,
    public readonly stage: IDealStage,
    public readonly meta: EventMetaData,
  ) {}
}
