import { PersonCreatedFromDraftCommand } from '../impl/person-created-from-draft.command';
import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { PersonCreatedFromDraftEvent } from '@daypaio/domain-events/persons';

@CommandHandler(PersonCreatedFromDraftCommand)
export class PersonCreatedFromDraftCommandHandler
  implements ICommandHandler<PersonCreatedFromDraftCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: PersonCreatedFromDraftCommand) {
    this.logger.log('COMMAND TRIGGERED: PersonCreatedFromDraftCommandHandler...');
    const { _id, data, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id }),
    );
    personAggregate.apply(new PersonCreatedFromDraftEvent(_id, data, meta));
    personAggregate.commit();
  }
}
