import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonAggregate } from '../../models/person.aggregate';
import { PersonRelatedLeadUnqualifiedEvent } from '@daypaio/domain-events/persons';
import { PRelatedLeadUnqualifiedCommand } from '../impl/related-lead-unqualified.command';

@CommandHandler(PRelatedLeadUnqualifiedCommand)
export class PRelatedLeadUnqualifiedCommandHandler
  implements ICommandHandler<PRelatedLeadUnqualifiedCommand> {
  private logger: Logger;
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: PRelatedLeadUnqualifiedCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _leadID, meta } = command;
    const persons = await this.personRepository.browseByDeals(_leadID, meta);
    for (const person of persons) {
      const aggregate = this.publisher.mergeObjectContext(
        new PersonAggregate(person),
      );
      aggregate.apply(new PersonRelatedLeadUnqualifiedEvent(person._id, _leadID, meta));
      aggregate.commit();
    }
  }
}
