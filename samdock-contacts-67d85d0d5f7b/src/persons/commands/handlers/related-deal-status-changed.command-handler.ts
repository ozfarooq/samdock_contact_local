import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PRelatedDealStageChangedCommand } from '../impl/related-deal-status-changed.command';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonAggregate } from '../../models/person.aggregate';
import { PersonRelatedDealStageChangedEvent } from '@daypaio/domain-events/persons';

@CommandHandler(PRelatedDealStageChangedCommand)
export class PRelatedDealStageChangedCommandHandler
  implements ICommandHandler<PRelatedDealStageChangedCommand> {
  private logger: Logger;
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: PRelatedDealStageChangedCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _dealID, stage, meta } = command;
    const persons = await this.personRepository.browseByDeals(_dealID, meta);
    for (const person of persons) {
      const aggregate = this.publisher.mergeObjectContext(
        new PersonAggregate(person),
      );
      aggregate.apply(new PersonRelatedDealStageChangedEvent(
        person._id,
        _dealID,
        stage,
        meta,
      ));
      aggregate.commit();
    }
  }
}
