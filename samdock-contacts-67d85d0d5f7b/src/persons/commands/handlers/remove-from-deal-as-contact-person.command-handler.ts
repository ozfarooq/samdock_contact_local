import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { RemoveFromDealAsContactPersonCommand } from '../impl/remove-from-deal-as-contact-person.command';

@CommandHandler(RemoveFromDealAsContactPersonCommand)
export class RemoveFromDealAsContactPersonCommandHandler
  implements ICommandHandler<RemoveFromDealAsContactPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RemoveFromDealAsContactPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: RemoveFromDealAsContactPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.removeFromDealAsContactPerson(_dealID, meta);
    personAggregate.commit();
  }
}
