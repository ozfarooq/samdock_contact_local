import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { UnrelateLeadFromPersonCommand } from '../impl/unrelate-lead-from-person.command';
import { LeadUnrelatedFromPersonEvent } from '@daypaio/domain-events/persons';

@CommandHandler(UnrelateLeadFromPersonCommand)
export class UnrelateLeadFromPersonCommandHandler
  implements ICommandHandler<UnrelateLeadFromPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateLeadFromPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelateDealFromPersonCommand...');
    const { _personID, _leadID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(new LeadUnrelatedFromPersonEvent(_personID, _leadID, meta));
    personAggregate.commit();
  }
}
