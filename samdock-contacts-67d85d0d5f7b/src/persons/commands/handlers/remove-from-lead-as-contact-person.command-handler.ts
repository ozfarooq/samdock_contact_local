import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { RemoveFromLeadAsContactPersonCommand } from '../impl/remove-from-lead-as-contact-person.command';

@CommandHandler(RemoveFromLeadAsContactPersonCommand)
export class RemoveFromLeadAsContactPersonCommandHandler
  implements ICommandHandler<RemoveFromLeadAsContactPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RemoveFromLeadAsContactPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: RemoveFromDealAsContactPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.removeFromLeadAsContactPerson(_dealID, meta);
    personAggregate.commit();
  }
}
