import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { PersonAggregate } from '../../models/person.aggregate';
import { RelateOrganizationToPersonCommand } from '../impl/relate-organization-to-person.command';

@CommandHandler(RelateOrganizationToPersonCommand)
export class RelateOrganizationToPersonCommandHandler
  implements ICommandHandler<RelateOrganizationToPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateOrganizationToPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: RelateOrganizationToPersonCommand...');
    const { _personID, _organizationID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.relateToOrganization(_organizationID, meta);
    personAggregate.commit();
  }
}
