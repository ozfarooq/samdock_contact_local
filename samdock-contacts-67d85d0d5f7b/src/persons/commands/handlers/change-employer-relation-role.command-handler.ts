import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { ChangeEmployerRelationRoleCommand } from '../impl/change-employer-relation-role.command';
import { EmployerRelationRoleChangedEvent } from '@daypaio/domain-events/persons';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(ChangeEmployerRelationRoleCommand)
export class ChangeEmployerRelationRoleCommandHandler
  implements ICommandHandler<ChangeEmployerRelationRoleCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: ChangeEmployerRelationRoleCommand) {
    this.logger.log('COMMAND TRIGGERED: ChangeEmployerRelationRoleCommand...');
    const { _personID, _organizationID, role, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _organizationID }),
    );
    personAggregate.apply(
      new EmployerRelationRoleChangedEvent(
        _personID,
        _organizationID,
        role,
        meta,
      ),
    );
    personAggregate.commit();
  }
}
