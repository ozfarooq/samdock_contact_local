import { AddPersonCommandHandler } from './add-person.command-handler';
import { DeletePersonCommandHandler } from './delete-person.command-handler';
import { DeletePersonsByQueryCommandHandler } from './delete-persons-by-query.command-handler';
import { EditPersonCommandHandler } from './edit-person.command-handler';
import { RelateOrganizationToPersonCommandHandler } from './relate-organization-to-person.command-handler';
import { UnrelateOrganizationFromPersonCommandHandler } from './unrelate-organization-from-person.command-handler';
import { RelateDealToPersonCommandHandler } from './relate-deal-to-person.command-handler';
import { UnrelateDealFromPersonCommandHandler } from './unrelate-deal-from-person.command-handler';
import { AssignToDealAsContactPersonCommandHandler } from './assign-to-deal-as-contact-person.command-handler';
import { RemoveFromDealAsContactPersonCommandHandler } from './remove-from-deal-as-contact-person.command-handler';
import { PRelatedDealLostCommandHandler } from './related-deal-lost.command-handler';
import { PRelatedDealWonCommandHandler } from './related-deal-won.command-handler';
import { PRelatedDealStageChangedCommandHandler } from './related-deal-status-changed.command-handler';
import { DraftCreatedForPersonCommandHandler } from './draft-created-for-person.command-handler';
import { PersonCreatedFromDraftCommandHandler } from './person-created-from-draft.command-handler';
import { ChangeEmployerRelationRoleCommandHandler } from './change-employer-relation-role.command-handler';
import { RelateLeadToPersonCommandHandler } from './relate-lead-to-person.command-handler';
import { UnrelateLeadFromPersonCommandHandler } from './unrelate-lead-from-person.command-handler';
import { PRelatedLeadPromotedToDealCommandHandler } from './related-lead-promoted-to-deal.command-handler';
import { PRelatedDealDemotedToLeadCommandHandler } from './related-deal-demoted-to-lead.command-handler';
import { PRelatedLeadQualifiedCommandHandler } from './related-lead-qualified.command-handler';
import { TaskUnlinkedFromPersonCommandHandler } from './task-unlinked-from-person-event.command-handler';
import { TaskLinkedToPersonCommandHandler } from './task-linked-to-person.command-handler';
import { PersonRelatedTaskReopenedCommandHandler } from './person-related-task-reopened.command-handler';
import { PersonRelatedTaskCompletedCommandHandler } from './person-related-task-completed.command-handler';
import { AssignToLeadAsContactPersonCommandHandler } from './assign-to-lead-as-contact-person.command-handler';
import { PRelatedLeadUnqualifiedCommandHandler } from './related-lead-unqualified.command-handler';
import { RemoveFromLeadAsContactPersonCommandHandler } from './remove-from-lead-as-contact-person.command-handler';

export const PersonCommandHandlers = [
  AddPersonCommandHandler,
  DeletePersonCommandHandler,
  DeletePersonsByQueryCommandHandler,
  EditPersonCommandHandler,
  RelateOrganizationToPersonCommandHandler,
  UnrelateOrganizationFromPersonCommandHandler,
  RelateDealToPersonCommandHandler,
  UnrelateDealFromPersonCommandHandler,
  AssignToDealAsContactPersonCommandHandler,
  RemoveFromDealAsContactPersonCommandHandler,
  PRelatedDealWonCommandHandler,
  PRelatedDealStageChangedCommandHandler,
  PRelatedDealLostCommandHandler,
  DraftCreatedForPersonCommandHandler,
  PersonCreatedFromDraftCommandHandler,
  ChangeEmployerRelationRoleCommandHandler,
  RelateLeadToPersonCommandHandler,
  UnrelateLeadFromPersonCommandHandler,
  PRelatedLeadPromotedToDealCommandHandler,
  PRelatedDealDemotedToLeadCommandHandler,
  PRelatedLeadQualifiedCommandHandler,
  PersonRelatedTaskCompletedCommandHandler,
  PersonRelatedTaskReopenedCommandHandler,
  TaskLinkedToPersonCommandHandler,
  TaskUnlinkedFromPersonCommandHandler,
  AssignToLeadAsContactPersonCommandHandler,
  PRelatedLeadUnqualifiedCommandHandler,
  RemoveFromLeadAsContactPersonCommandHandler,
];
