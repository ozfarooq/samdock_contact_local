import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { UnrelateDealFromPersonCommand } from '../impl/unrelate-deal-from-person.command';

@CommandHandler(UnrelateDealFromPersonCommand)
export class UnrelateDealFromPersonCommandHandler
  implements ICommandHandler<UnrelateDealFromPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateDealFromPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelateDealFromPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.unrelateDeal(_dealID, meta);
    personAggregate.commit();
  }
}
