import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TaskLinkedToPersonCommand }
  from '../impl/task-linked-to-person.command';
import { TaskLinkedToPersonEvent } from '@daypaio/domain-events/persons';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(TaskLinkedToPersonCommand)
export class TaskLinkedToPersonCommandHandler
  implements ICommandHandler<TaskLinkedToPersonCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: TaskLinkedToPersonCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: TaskLinkedToPersonCommandHandler...',
    );
    const { _personID, _taskID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(
      new TaskLinkedToPersonEvent(_personID, _taskID, meta),
    );
    personAggregate.commit();
  }
}
