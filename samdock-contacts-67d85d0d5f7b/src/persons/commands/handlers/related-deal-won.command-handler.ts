import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PRelatedDealWonCommand } from '../impl/related-deal-won.command';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonAggregate } from '../../models/person.aggregate';
import { PersonRelatedDealWonEvent } from '@daypaio/domain-events/persons';

@CommandHandler(PRelatedDealWonCommand)
export class PRelatedDealWonCommandHandler
  implements ICommandHandler<PRelatedDealWonCommand> {
  private logger: Logger;
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: PRelatedDealWonCommand) {
    this.logger.log('COMMAND TRIGGERED persons');
    const { _dealID, stage, meta } = command;
    const persons = await this.personRepository.browseByDeals(_dealID, meta);
    for (const person of persons) {
      const aggregate = this.publisher.mergeObjectContext(
        new PersonAggregate(person),
      );
      aggregate.apply(new PersonRelatedDealWonEvent(person._id, _dealID, stage, meta));
      aggregate.commit();
    }
  }
}
