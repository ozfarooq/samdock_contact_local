import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PRelatedDealLostCommand } from '../impl/related-deal-lost.command';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonAggregate } from '../../models/person.aggregate';
import { PersonRelatedDealLostEvent } from '@daypaio/domain-events/persons';

@CommandHandler(PRelatedDealLostCommand)
export class PRelatedDealLostCommandHandler
  implements ICommandHandler<PRelatedDealLostCommand> {
  private logger: Logger;
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: PRelatedDealLostCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _dealID, stage, meta } = command;
    const persons = await this.personRepository.browseByDeals(_dealID, meta);
    for (const person of persons) {
      const aggregate = this.publisher.mergeObjectContext(
        new PersonAggregate(person),
      );
      aggregate.apply(new PersonRelatedDealLostEvent(person._id, _dealID, stage, meta));
      aggregate.commit();
    }
  }
}
