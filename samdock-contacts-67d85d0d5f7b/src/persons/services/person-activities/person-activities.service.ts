import { Injectable, UnauthorizedException } from '@nestjs/common';
import { EventStore } from 'nestjs-eventstore';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { streamPosition } from 'node-eventstore-client';

@Injectable()
export class PersonActivitiesService {
  constructor(private eventStore: EventStore) {}

  async browseActivities(personID: string, meta: EventMetaData) {
    const events = await this.eventStore.connection.readStreamEventsBackward(
      `persons-${personID}`,
      streamPosition.end,
      1000,
    );

    const firstEvent = JSON.parse(events.events[0].event.data.toString());

    if (!firstEvent.meta || firstEvent.meta._tenantID !== meta._tenantID) {
      throw new UnauthorizedException();
    }

    return events.events.map((event) => {
      const data = JSON.parse(event.event.data.toString());
      const meta = data.meta;
      delete data.meta;

      if (meta && !meta.timestamp) {
        meta.timestamp = event.event.created.getTime();
      }

      return {
        data,
        meta,
        type: event.event.eventType,
      };
    });
  }
}
