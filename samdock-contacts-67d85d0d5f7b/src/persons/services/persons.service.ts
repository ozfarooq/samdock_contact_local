import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Person } from '../models/person.model';
import { BreadService } from '../../shared/services/bread.service';
import { BrowsePersonQuery } from '../queries/impl/browse-person.query';
import { FindPersonQuery } from '../queries/impl/find-person.query';
import { ReadPersonQuery } from '../queries/impl/read-person.query';
import { EditPersonCommand } from '../commands/impl/edit-person.command';
import { AddPersonCommand } from '../commands/impl/add-person.command';
import { DeletePersonCommand } from '../commands/impl/delete-person.command';
import { DeletePersonsByQueryCommand } from '../commands/impl/delete-persons-by-query.command';
import { IPerson } from '../models/person.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

@Injectable()
export class PersonsService extends BreadService<IPerson> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<Person[]> {
    return await this.executeQuery(new BrowsePersonQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<Person> {
    return await this.executeQuery(new ReadPersonQuery(id, meta));
  }

  async edit(
    id: string,
    person: Partial<IPerson>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditPersonCommand(id, person, meta),
    );
  }

  async add(person: IPerson, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddPersonCommand(person, meta));
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeletePersonCommand(id, meta));
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeletePersonCommand(id, meta));
    }));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async find(query, meta: EventMetaData): Promise<Person[]> {
    return await this.executeQuery(new FindPersonQuery(query, meta));
  }

  async deleteByQuery(query, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeletePersonsByQueryCommand(query, meta));
  }
}
