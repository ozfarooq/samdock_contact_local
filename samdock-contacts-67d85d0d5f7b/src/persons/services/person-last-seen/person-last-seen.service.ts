import { Injectable, NotFoundException, NotImplementedException } from '@nestjs/common';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { LastSeen } from '../../models/last-seen.model';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { BreadRepository } from '../../../shared/bread.respository';

@Injectable()
export class PersonLastSeenService extends BreadRepository<LastSeen> {

  constructor(
    @InjectModel(LastSeen) lastSeenModel: ReturnModelType<typeof LastSeen>,
  ) {
    super(lastSeenModel);
  }

  async read(id: string, meta: EventMetaData): Promise<LastSeen> {
    const result = await this.model.findOne(
      { _id: meta._userID },
    ).lean();
    if (!result) {
      return { _id: meta._userID, _personIDs: [] };
    }
    return result;
  }

  async add(data: LastSeen, meta: EventMetaData): Promise<void> {
    const lastSeen = await this.model.findOne(
      { _id: meta._userID },
    ).lean() || data;

    try {
      this.logger.verbose('EDIT');
      if (lastSeen._personIDs?.includes(data._personIDs[0])) {
        lastSeen._personIDs?.splice(lastSeen._personIDs?.indexOf(data._personIDs[0]), 1);
      }
      lastSeen._personIDs?.unshift(data._personIDs[0]);

      const slicedArray = lastSeen._personIDs?.slice(0, 10);
      lastSeen._personIDs = slicedArray;
      const result = await this.model.updateOne(
        { _id: meta._userID },
        { $set: { _personIDs: lastSeen._personIDs } },
        { upsert : true },
      );

      return result;
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browse(): Promise<LastSeen[]> {
    throw new NotImplementedException();
  }

  async edit(): Promise<void> {
    throw new NotImplementedException();
  }

  async delete(): Promise<void> {
    throw new NotImplementedException();
  }
}
