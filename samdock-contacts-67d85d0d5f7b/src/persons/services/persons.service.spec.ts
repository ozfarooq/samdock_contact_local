import { Test, TestingModule } from '@nestjs/testing';
import { PersonsService } from './persons.service';
import { getModelToken } from 'nestjs-typegoose';
import { CqrsModule } from '@nestjs/cqrs';

const mockPersonModel = () => ({});

describe('PersonsService', () => {
  let service: PersonsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        PersonsService,
        {
          provide: getModelToken('Person'),
          useFactory: mockPersonModel,
        },
      ],
    }).compile();

    service = module.get<PersonsService>(PersonsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
