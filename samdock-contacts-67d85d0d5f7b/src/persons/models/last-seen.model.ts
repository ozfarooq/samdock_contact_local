import { ApiProperty } from '@nestjs/swagger';
import { prop } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';

export interface ILastSeen {
  _id: string;
  _personIDs: string[];
}

export class LastSeen implements ILastSeen {
  @ApiProperty({
    description: 'The ID of the user',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiProperty({
    description: 'Last seen persons',
    required: false,
  })
  @prop({ type: String })
  _personIDs: string[];

  constructor(data?: Partial<LastSeen>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
