import { AggregateRoot } from '@nestjs/cqrs';
import { IPerson, IPersonAddress } from './person.interface';
import {
  PersonAddedEvent,
  PersonEditedEvent,
  PersonDeletedEvent,
  PersonDeletedByQueryEvent,
} from '../events/impl';
import { plainToClassFromExist } from 'class-transformer';
import {
  OrganizationRelatedToPersonEvent,
  OrganizationUnrelatedFromPersonEvent,
  DealRelatedToPersonEvent,
  DealUnrelatedFromPersonEvent,
  AssignedToDealAsContactPersonEvent,
  RemovedFromDealAsContactPersonEvent,
  PersonDOIGrantedEvent,
  IConsent,
  Gender,
  AssignedToLeadAsContactPersonEvent,
  RemovedFromLeadAsContactPersonEvent,
} from '@daypaio/domain-events/persons';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { IIntegrations } from '@daypaio/domain-events/shared/interfaces/integrations.interface';

class PersonAddress implements IPersonAddress {

  street: string;

  number: string;

  postcode: string;

  city: string;

  country: string;

}

export class PersonAggregate extends AggregateRoot implements IPerson {
  _id: string;

  email: string;

  firstName: string;

  lastName: string;

  gender: Gender;

  namePrefix: string;

  nameSuffix: string;

  phoneNumber: string;

  mobileNumber: string;

  privateNumber: string;

  officeNumber: string;

  homeNumber: string;

  faxNumber: string;

  website: string;

  linkedin: string;
  xing: string;
  facebook: string;
  twitter: string;
  instagram: string;

  birthday: string;

  createdAt: Date = new Date();

  address: PersonAddress;

  importID: string;

  consents: IConsent[];

  customFields?: EntityCustomFields;

  integrations?: IIntegrations;

  constructor(data?: Partial<IPerson>) {
    super();

    plainToClassFromExist(this, data);
  }

  add(meta: EventMetaData) {
    this.apply(new PersonAddedEvent(this._id, this, meta));
  }

  edit(meta: EventMetaData) {
    this.apply(new PersonEditedEvent(this._id, this, meta));
  }

  delete(meta: EventMetaData) {
    this.apply(new PersonDeletedEvent(this._id, meta));
  }

  deleteByQuery(query, meta: EventMetaData) {
    this.apply(new PersonDeletedByQueryEvent(query, meta));
  }

  relateToOrganization(_organizationID: string, meta: EventMetaData) {
    this.apply(new OrganizationRelatedToPersonEvent(this._id, _organizationID, meta));
  }

  unrelateFromOrganization(_organizationID: string, meta: EventMetaData) {
    this.apply(new OrganizationUnrelatedFromPersonEvent(this._id, _organizationID, meta));
  }

  relateDeal(_organizationID: string, meta: EventMetaData) {
    this.apply(new DealRelatedToPersonEvent(this._id, _organizationID, meta));
  }

  unrelateDeal(_organizationID: string, meta: EventMetaData) {
    this.apply(new DealUnrelatedFromPersonEvent(this._id, _organizationID, meta));
  }

  assignToDealAsContactPerson(dealID: string, meta: EventMetaData) {
    this.apply(new AssignedToDealAsContactPersonEvent(this._id, dealID, meta));
  }

  removeFromDealAsContactPerson(dealID: string, meta: EventMetaData) {
    this.apply(new RemovedFromDealAsContactPersonEvent(this._id, dealID, meta));
  }

  assignToLeadAsContactPerson(dealID: string, meta: EventMetaData) {
    this.apply(new AssignedToLeadAsContactPersonEvent(this._id, dealID, meta));
  }

  removeFromLeadAsContactPerson(dealID: string, meta: EventMetaData) {
    this.apply(new RemovedFromLeadAsContactPersonEvent(this._id, dealID, meta));
  }

  grantDOI(ip: string, email: string, meta: EventMetaData) {
    this.apply(new PersonDOIGrantedEvent(this._id, ip, email, meta));
  }

}
