import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ImportRepository } from '../../repositories/import.repository';
import { ImportAddedEvent } from '../impl';

@EventsHandler(ImportAddedEvent)
export class AddImportEventHandler implements IEventHandler<ImportAddedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: ImportRepository) {}
  async handle(event: ImportAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    try {
      await this.repository.add(data, meta);
    } catch (error) {
      this.logger.error(`Failed to create import of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
