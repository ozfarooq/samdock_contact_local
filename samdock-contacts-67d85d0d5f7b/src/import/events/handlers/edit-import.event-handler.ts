import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ImportRepository } from '../../repositories/import.repository';
import { ImportEditedEvent } from '../impl';

@EventsHandler(ImportEditedEvent)
export class EditImportEventHandler
  implements IEventHandler<ImportEditedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: ImportRepository) {}
  async handle(event: ImportEditedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _id, data, meta } = event;
    try {
      const result = await this.repository.edit(_id, data, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to update import of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
