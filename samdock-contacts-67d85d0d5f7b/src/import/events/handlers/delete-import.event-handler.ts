import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ImportRepository } from '../../repositories/import.repository';
import { ImportDeletedEvent } from '../impl';

@EventsHandler(ImportDeletedEvent)
export class DeleteImportEventHandler
  implements IEventHandler<ImportDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: ImportRepository) {}
  async handle(event: ImportDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      const result = await this.repository.delete(_id, meta);
      return result;
    } catch (error) {
      this.logger.error(`Cannot delete import of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event possibly
    }
  }
}
