import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadImportQuery } from '../impl/read-import.query';
import { Import } from '../../models/import.model';
import { ImportRepository } from '../../repositories/import.repository';

@QueryHandler(ReadImportQuery)
export class ReadImportQueryHandler implements IQueryHandler<ReadImportQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repository: ImportRepository) {}

  async execute(query: ReadImportQuery): Promise<Import> {
    this.logger.log(`Reading Import: ${query.id}`);
    try {
      const { id, meta } = query;
      const result = await this.repository.read(id, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to read Import with id: ${query.id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
