import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseImportQuery } from '../impl/browse-import.query';
import { Import } from '../../models/import.model';
import { ImportRepository } from '../../repositories/import.repository';

@QueryHandler(BrowseImportQuery)
export class BrowseImportHandler implements IQueryHandler<BrowseImportQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repository: ImportRepository) {}

  async execute(query: BrowseImportQuery): Promise<Import[]> {
    this.logger.log('Browsing Imports');
    try {
      const result = await this.repository.browse(query.meta);
      return result;
    } catch (error) {
      this.logger.error('Failed to browse on Import');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
