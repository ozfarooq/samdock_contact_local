import { AddImportCommandHandler } from './add-import.command-handler';
import { DeleteImportCommandHandler } from './delete-import.command-handler';
import { EditImportCommandHandler } from './edit-import.command-handler';

export const ImportCommandHandlers = [
  AddImportCommandHandler,
  EditImportCommandHandler,
  DeleteImportCommandHandler,
];
