import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { DeleteImportCommand } from '../impl/delete-import.command';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { ImportAggregate } from '../../models/import.aggregate';

@CommandHandler(DeleteImportCommand)
export class DeleteImportCommandHandler
  implements ICommandHandler<DeleteImportCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteImportCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteCommandHandler...');
    const { id, meta } = command;
    const importAggregate = this.publisher.mergeObjectContext(
      new ImportAggregate({ _id: id }),
    );
    importAggregate.delete(meta);
    importAggregate.commit();
  }
}
