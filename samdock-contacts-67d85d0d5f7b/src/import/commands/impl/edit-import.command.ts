import { ICommand } from '@nestjs/cqrs';
import { IImport } from '../../models/import.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditImportCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly data: Partial<IImport>,
    public readonly meta: EventMetaData,
  ) {}
}
