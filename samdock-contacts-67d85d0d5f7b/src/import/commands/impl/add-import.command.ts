import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IImport } from '../../models/import.interface';

export class AddImportCommand implements ICommand {
  constructor(
    public readonly data: Partial<IImport>,
    public readonly meta: EventMetaData,
  ) {}
}
