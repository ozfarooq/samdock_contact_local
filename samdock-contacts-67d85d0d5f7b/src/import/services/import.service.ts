import { Injectable, NotFoundException, Logger } from '@nestjs/common';
import { Import } from '../models/import.model';
import { BreadService } from '../../shared/services/bread.service';
import { BrowseImportQuery } from '../queries/impl/browse-import.query';
import { ReadImportQuery } from '../queries/impl/read-import.query';
import { EditImportCommand } from '../commands/impl/edit-import.command';
import { AddImportCommand } from '../commands/impl/add-import.command';
import { DeleteImportCommand } from '../commands/impl/delete-import.command';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ImportDTO } from '../dtos/import.dto';
import { CommandBus, EventBus, QueryBus, ICommand, IQuery } from '@nestjs/cqrs';
import { filter } from 'rxjs/operators';

@Injectable()
export class ImportService {
  protected logger: Logger;
  constructor(
    private commandBus: CommandBus,
    private queryBus: QueryBus,
    private eventBus: EventBus,
    ) {
    this.logger = new Logger(this.constructor.name);
  }

  waitForEvent(id: string, importID: string) {
    return new Promise((resolve) => {
      this.eventBus
        .pipe(
          filter(event =>
            (<any>event).data?._id === id && (<any>event).data?.importID === importID),
        )
        .subscribe((event) => {
          resolve(event);
        });
    });
  }

  protected async executeCommand(command: ICommand): Promise<void> {
    if (this.commandBus) {
      this.logger.log(`${command.constructor.name} executed`);
      return await this.commandBus.execute(command);
    }
    this.logger.error(
      `Could not execute command of type ${command.constructor.name} on the command bus of wrong type
      `,
    );
  }

  protected async executeQuery(query: IQuery): Promise<any> {
    if (this.queryBus) {
      this.logger.log(`${query.constructor.name} executed`);
      return await this.queryBus.execute(query);
    }
    this.logger.error(
      `Could not execute query of type ${query.constructor.name} on the query bus of wrong type
      `,
    );
  }

  async browse(meta: EventMetaData): Promise<ImportDTO[]> {
    return await this.executeQuery(new BrowseImportQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<ImportDTO> {
    return await this.executeQuery(new ReadImportQuery(id, meta));
  }

  async edit(
    id: string,
    data: Partial<Import>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditImportCommand(id, data, meta),
    );
  }

  async add(data: Partial<Import>, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddImportCommand(data, meta));
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteImportCommand(id, meta));
  }
}
