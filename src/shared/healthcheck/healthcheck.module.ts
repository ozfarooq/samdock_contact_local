import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './terminus-options.service';
import { EventStoreHealthIndicator } from './event-store.health';
import { EventStoreCatchupHealthIndicator } from './event-store-catchup.health';
import { TyepgooseHealthIndicator } from './typegoose.health';

@Module({
  imports: [
    TerminusModule.forRootAsync({
      imports: [HealthcheckModule],
      useClass: TerminusOptionsService,
    }),
  ],
  providers: [
    EventStoreHealthIndicator,
    EventStoreCatchupHealthIndicator,
    TyepgooseHealthIndicator,
  ],
  exports: [
    EventStoreHealthIndicator,
    EventStoreCatchupHealthIndicator,
    TyepgooseHealthIndicator,
  ],
})
export class HealthcheckModule {}
