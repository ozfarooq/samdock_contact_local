import { Injectable } from '@nestjs/common';
import { HealthCheckError } from '@godaddy/terminus';
import { HealthIndicatorResult, HealthIndicator } from '@nestjs/terminus';
import { ModuleRef } from '@nestjs/core';
import { EventBusProvider } from 'nestjs-eventstore';

@Injectable()
export class EventStoreCatchupHealthIndicator extends HealthIndicator {
  constructor(private moduleRef: ModuleRef) {
    super();
  }

  async isHealthy(key: string): Promise<HealthIndicatorResult> {
    const eventBus: EventBusProvider = this.moduleRef.get(EventBusProvider, {
      strict: false,
    });

    const isHealthy =
      eventBus && eventBus.publisher && eventBus.publisher.isLive;

    const result = this.getStatus(key, isHealthy);

    if (isHealthy) {
      return result;
    }

    throw new HealthCheckError('EventStoreCatchup Healthcheck failed', result);
  }
}
