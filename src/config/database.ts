export default {
  mongoConnectionString:
    process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost:27017/test',
};
