import { Module } from '@nestjs/common';
import { ImportController } from './controllers/import.controller';
import { ImportGateway } from './gateway/import.gateway';
import { XLSXService } from './../shared/services/xlsx.service';
import { ImportService } from './services/import.service';
import { PersonsService } from './../persons/services/persons.service';
import { PersonRepository } from './../persons/repositories/person.repository';
import { OrganizationsService } from './../organizations/services/organizations.service';
import { OrganizationRepository } from './../organizations/repositories/organization.repository';
import { Import } from './models/import.model';
import { TypegooseModule } from 'nestjs-typegoose';
import { ImportRepository } from './repositories/import.repository';
import { ImportCommandHandlers } from './commands/handlers';
import { ImportQueryHandlers } from './queries/handlers';
import { ImportEventHandlers } from './events/handlers';
import { PersonsModule } from './../persons/persons.module';
import { OrganizationsModule } from './../organizations/organizations.module';
import { CustomFieldsModule } from './../custom-fields/custom-fields.module';
import { CustomFieldsService } from './../custom-fields/services/custom-fields.service';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: Import,
        schemaOptions: {
          collection: 'import',
        },
      },
    ]),
    PersonsModule,
    OrganizationsModule,
    CustomFieldsModule,
  ],
  controllers: [ImportController],
  providers: [
    ImportGateway,
    XLSXService,
    PersonsService,
    OrganizationsService,
    CustomFieldsService,
    ImportService,
    ImportRepository,
    ...ImportQueryHandlers,
    ...ImportCommandHandlers,
    ...ImportEventHandlers,
  ],
})
export class ImportModule {}
