import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { AddImportCommand } from '../impl/add-import.command';
import { EventPublisher } from 'nestjs-eventstore';
import { ImportAggregate } from '../../models/import.aggregate';

@CommandHandler(AddImportCommand)
export class AddImportCommandHandler
  implements ICommandHandler<AddImportCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddImportCommand) {
    this.logger.log('COMMAND TRIGGERED: AddCommandHandler...');
    const { data, meta } = command;
    const importAggregate = this.publisher.mergeObjectContext(
      new ImportAggregate(data),
    );
    importAggregate.add(meta);
    importAggregate.commit();
  }
}
