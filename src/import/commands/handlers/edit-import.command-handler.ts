import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EditImportCommand } from '../impl/edit-import.command';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { ImportAggregate } from '../../models/import.aggregate';

@CommandHandler(EditImportCommand)
export class EditImportCommandHandler
  implements ICommandHandler<EditImportCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditImportCommand) {
    this.logger.log('COMMAND TRIGGERED: EditCommandHandler...');
    const { id, data, meta } = command;
    data._id = id;
    const importAggregate = this.publisher.mergeObjectContext(
      new ImportAggregate(data),
    );
    importAggregate.edit(meta);
    importAggregate.commit();
  }
}
