import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteImportCommand implements ICommand {
  constructor(
    public id: string,
    public meta: EventMetaData,
  ) {}
}
