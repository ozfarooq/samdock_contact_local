import { BreadRepository } from '../../shared/bread.respository';
import { Import } from '../models/import.model';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';

export class ImportRepository extends BreadRepository<Import> {
  constructor(@InjectModel(Import) importModel: ReturnModelType<typeof Import>) {
    super(importModel);
  }
}
