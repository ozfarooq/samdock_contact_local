import { SubscribeMessage, WebSocketGateway, MessageBody, ConnectedSocket,
  OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { PersonsService } from '../../persons/services/persons.service';
import { OrganizationsService } from '../../organizations/services/organizations.service';
import { RelatePersonToOrganizationCommand } from '../../organizations/commands/impl/relate-person-to-organization.command';
import { CommandBus } from '@nestjs/cqrs';
import { ImportService } from '../services/import.service';
import { GetWsUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { Socket } from 'socket.io';
import { OrganizationRepository } from '../../organizations/repositories/organization.repository';
import { PersonRepository } from '../../persons/repositories/person.repository';
import { filter } from 'rxjs/operators';

const IMPORT_PART_SIZE = 100;

@WebSocketGateway({ path: '/api/contacts/import/socket.io' })
export class ImportGateway implements OnGatewayConnection, OnGatewayDisconnect {
  protected logger = new Logger(this.constructor.name);
  protected connectedUsers: any[] = [];
  protected importSubscribtions = [];

  constructor(
    private personsService: PersonsService,
    private organizationsService: OrganizationsService,
    private importService: ImportService,
    private commandBus: CommandBus,
    private personRepository: PersonRepository,
    private organizationRepository: OrganizationRepository,
  ) {
    this.logger.log('Web socket is listening');
  }

  handleConnection(@ConnectedSocket() client: Socket) {
    this.logger.log(`Connected: ${client.id}`);
  }

  async handleDisconnect(@ConnectedSocket() client: Socket) {
    this.logger.log(`Disconnected: ${client.id}`);
    const userIndex = this.connectedUsers.findIndex(user => user.clientID === client.id);
    if (userIndex === -1) return;
    const user = this.connectedUsers.splice(userIndex, 1)[0];
    this.logger.log(this.connectedUsers);
    if (user && user.importID && user.meta) {
      const { importID, meta } = user;
      // If user disconnects in the middle of process
      const importSubscriptionEntity = this.importSubscribtions
        .find(item => item.importID === importID);
      if (importSubscriptionEntity) {
        importSubscriptionEntity.subscriptions.forEach(subscription => subscription.unsubscribe());
      }
      try {
        this.importService.delete(importID, meta);
      } catch (err) {
        this.logger.log('Import document is not exists. Cancel connection.');
      }
    }
  }

  @SubscribeMessage('import')
  async import(
    @ConnectedSocket() client: Socket,
    @MessageBody() message: any,
    @GetWsUser() user: LoggedInUser,
  ) {
    this.logger.log('Client import');
    const importID = message.importID;
    const meta = user.generateMeta();

    try {
      const importDocument = await this.importService.read(importID, meta);

      const result = { entities: [] };

      this.subscribeToRepositories(client, importDocument.data, importID);

      for (let importIndex = 0; importIndex < importDocument.data.length; importIndex += 1) {
        const importEntity: any = importDocument.data[importIndex];
        const items = importEntity.entities;
        if (!items.length) continue;

        const service = this.getService(importEntity.model);

        // Import slice less then IMPORT_PART_SIZE
        if (items.length <= IMPORT_PART_SIZE) {
          await this.importReadyValues(items, service, meta);
          if (importEntity.model === 'persons') {
            this.relatePersonsToOrganizations(items, meta);
          }
          await this.importService.waitForEvent(items[items.length - 1]._id, importID);
          continue;
        } // else

        // Import bigger slices
        const itemsParts = [];
        for (let i = 0, len = Math.ceil(items.length / IMPORT_PART_SIZE); i < len; i += 1) {
          itemsParts[i] = items.slice(
            IMPORT_PART_SIZE * i,
            IMPORT_PART_SIZE * i + IMPORT_PART_SIZE,
          );
        }

        for (let i = 0; i < itemsParts.length; i += 1) {
          const items = itemsParts[i];
          await this.importReadyValues(items, service, meta);
          await this.importService.waitForEvent(items[items.length - 1]._id, importID);
        }

        this.logger.log('Large import is finished');
        if (importEntity.model === 'persons') {
          this.relatePersonsToOrganizations(items, meta);
        }
      }
      this.importService.delete(importID, meta);
    } catch (error) {
      this.onImportError(client, error, importID, meta);
    }
  }

  @SubscribeMessage('connectImportID')
  async connect(
    @ConnectedSocket() client: Socket,
    @MessageBody() message: any,
    @GetWsUser() user: LoggedInUser,
  ) {
    this.logger.log(`Connected importID: ${message.importID}`);
    const foundIndex = this.connectedUsers.findIndex(user => user.clientID === client.id);
    if (foundIndex > -1) {
      this.connectedUsers[foundIndex].importID = message.importID;
      this.connectedUsers[foundIndex].meta = user.generateMeta();
    } else {
      this.connectedUsers.push({
        clientID: client.id,
        importID: message.importID,
        meta: user.generateMeta(),
      });
    }

    this.logger.log(this.connectedUsers);
  }

  private async importReadyValues(data, service, meta) {
    await Promise.all(data.map(row => service.add(row, meta)));
  }

  private relatePersonsToOrganizations(items, meta) {
    items.forEach((item) => {
      if (item.employers && item.employers.length && item.employers[0]._organizationID) {
        const employers = Object.assign({}, item.employers);
        delete item.employers;
        this.commandBus.execute(
          new RelatePersonToOrganizationCommand(
            employers[0]._organizationID,
            item._id,
            meta,
          ),
        );
      }
    });
  }

  private onImportFinished(client, result, importID) {
    client.emit('importFinished', result);
    const importSubscriptionEntity = this.importSubscribtions
    .find(item => item.importID === importID);
    if (importSubscriptionEntity) {
      importSubscriptionEntity.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
    this.logger.log('Import finished');
  }

  private onImportError(client, error, importID, meta) {
    client.emit('importError', error);
    this.personsService.deleteByQuery({ importID }, meta);
    this.organizationsService.deleteByQuery({ importID }, meta);
  }

  private async subscribeToRepositories(client: Socket, importData, importID) {
    const result = { entities: [] };
    const subscribtionEntity = { importID, subscriptions: [] };
    for (let importIndex = 0; importIndex < importData.length; importIndex += 1) {
      const importEntity: any = importData[importIndex];
      const items = importEntity.entities;
      if (!items.length) continue;
      const repository = this.getRepository(importEntity.model);

      let partSuccess = [];
      let partErrors = [];
      let leftItems = items.length;
      const entityResult = { model: importEntity.model, success: [], errors: [] };

      const successSubscription = repository.importResult.success$
      .pipe(filter(data => (<any>data).importID === importID))
      .subscribe((document) => {
        partSuccess.push(document);
        if (partSuccess.length + partErrors.length === leftItems) {
          client.emit('imported', {
            model: importEntity.model,
            success: partSuccess,
            errors: partErrors,
          });
          entityResult.success = [...entityResult.success, ...partSuccess];
          entityResult.errors = [...entityResult.errors, ...partErrors];
          result.entities.push(entityResult);
          partSuccess = [];
          partErrors = [];

          if (importIndex === importData.length - 1) {
            this.onImportFinished(client, result, importID);
          }

        } else if (partSuccess.length + partErrors.length === IMPORT_PART_SIZE) {
          client.emit('imported', {
            model: importEntity.model,
            success: partSuccess,
            errors: partErrors,
          });
          entityResult.success = [...entityResult.success, ...partSuccess];
          entityResult.errors = [...entityResult.errors, ...partErrors];
          leftItems -= partSuccess.length + partErrors.length;
          partSuccess = [];
          partErrors = [];
        }
      });

      const errorsSubscription = repository.importResult.errors$
      .pipe(filter(data => (<any>data).importID === importID))
      .subscribe((data: any) => {
        partErrors.push(data.error);
        if (partSuccess.length + partErrors.length === leftItems) {
          client.emit('imported', {
            model: importEntity.model,
            success: partSuccess,
            errors: partErrors,
          });
          entityResult.success = [...entityResult.success, ...partSuccess];
          entityResult.errors = [...entityResult.errors, ...partErrors];
          result.entities.push(entityResult);
          partSuccess = [];
          partErrors = [];

          if (importIndex === importData.length) {
            this.onImportFinished(client, result, importID);
          }
        }
      });

      subscribtionEntity.subscriptions.push(successSubscription);
      subscribtionEntity.subscriptions.push(errorsSubscription);
    }
    this.importSubscribtions.push(subscribtionEntity);
  }

  private getService(model) {
    if (model === 'persons') return this.personsService;
    if (model === 'organizations') return this.organizationsService;
    throw new Error(`Service for model ${model} not found`);
  }

  private getRepository(model) {
    if (model === 'persons') return this.personRepository;
    if (model === 'organizations') return this.organizationRepository;
    throw new Error(`Repository for model ${model} not found`);
  }
}
