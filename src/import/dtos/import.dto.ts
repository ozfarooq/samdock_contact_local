import { plainToClassFromExist } from 'class-transformer';
import {
  IsString,
  IsArray,
  IsOptional,
} from 'class-validator';
import { IImport } from '../events/impl';

export class ImportDTO implements IImport {

  constructor(data?: any) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsArray()
  @IsOptional()
  data: [];
}
