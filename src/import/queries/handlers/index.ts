import { BrowseImportHandler } from './browse-import.query-handler';
import { ReadImportQueryHandler } from './read-import.query-handler';

export const ImportQueryHandlers = [BrowseImportHandler, ReadImportQueryHandler];
