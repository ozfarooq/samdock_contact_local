import { prop } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IImport, IImportEntity } from './import.interface';

class ImportEntity implements IImportEntity {
  @prop()
  model: string;

  @prop()
  entities: any;
}

export class Import implements IImport {
  @ApiProperty({
    description: 'The ID of the import',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiProperty({
    description: 'the related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiPropertyOptional({
    description: 'Data prepared for import',
  })
  @prop({ type: ImportEntity, _id: false })
  data: ImportEntity[];

  constructor(data?: Partial<Import>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
