import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { IImport, IImportEntity } from './import.interface';
import {
  ImportAddedEvent,
  ImportEditedEvent,
  ImportDeletedEvent,
} from '../events/impl';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ImportAggregate extends AggregateRoot implements IImport {
  _id: string;

  data: IImportEntity[];

  constructor(data?: Partial<IImport>) {
    super();

    plainToClassFromExist(this, data);
  }

  add(meta: EventMetaData) {
    this.apply(new ImportAddedEvent(this._id, this, meta));
  }

  edit(meta: EventMetaData) {
    this.apply(new ImportEditedEvent(this._id, this, meta));
  }

  delete(meta: EventMetaData) {
    this.apply(new ImportDeletedEvent(this._id, meta));
  }
}
