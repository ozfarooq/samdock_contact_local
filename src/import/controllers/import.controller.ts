import { Controller, Param, Post, Get, Res, Body, Logger,
  UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { XLSXService } from '../../shared/services/xlsx.service';
import { PersonsService } from '../../persons/services/persons.service';
import { OrganizationsService } from '../../organizations/services/organizations.service';
import { ImportService } from '../services/import.service';
import { Import } from '../models/import.model';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import {
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { v4 as uuidv4 } from 'uuid';
import * as moment from 'moment';
import { isEmpty, validate } from 'class-validator';
import { PersonDTO } from '../../persons/dtos/person.dto';
import { OrganizationDTO } from '../../organizations/dto/organization.dto';
import * as fs from 'fs';
import * as path from 'path';
import { CustomFieldsService } from '../../custom-fields/services/custom-fields.service';
import { CustomFieldDTO } from '../../custom-fields/dtos/custom-field.dto';
import { Gender } from '../../persons/events/impl';

const MAX_DOCUMENTS_RETURN = 100;

const PERSON_IMPORT_IGNORED_COLUMNS = {
  en: 'Explanation: Ignore or delete this column',
  de: 'Info: Diese Spalte löschen oder ignorieren',
};

const MALE_GENDERS = ['male', 'mr.', 'mr', 'male', 'sir', 'herr', 'hr.', 'hr', 'männlich'];

const FEMALE_GENDERS = ['female', 'mrs', 'mrs.', 'ms', 'ms.', 'female', 'madam', 'weiblich', 'frau', 'fr.', 'fr', 'fräulein'];

@ApiTags('import')
@ApiBearerAuth()
@Controller('import')
export class ImportController {
  protected logger = new Logger(this.constructor.name);

  constructor(
    private xlsxService: XLSXService,
    private personsService: PersonsService,
    private organizationsService: OrganizationsService,
    private importService: ImportService,
    private customFieldsService: CustomFieldsService,
  ) {}

  @ApiOperation({
    summary: 'Get xlsx file with all persons with that import ID',
    description: 'Get all persons with that import ID',
  })
  @ApiOkResponse({
    description: 'Persons file have successfully been queried',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('organizationsTemplate/:importID/:lang')
  async getOrganizationsTemplate(
    @Res() res,
    @GetUser() user: LoggedInUser,
    @Param('importID') importID: string,
    @Param('lang') lang: string = 'en',
  ) {
    this.logger.log(`Get persons template user:${user._id}`);
    this.logger.log(user.generateMeta());
    const meta = user.generateMeta();

    let customFields = [];

    try {
      const organizationCustomFields =
        await this.customFieldsService.browseByEntity('organization', meta);
      if (organizationCustomFields && organizationCustomFields.length) {
        customFields = organizationCustomFields.map(item => item.key);
      }
    } catch (err) {
      this.logger.log(`No custom fields for tenant ${user._tenantID}`);
    }

    const fileExample = fs.readFileSync(path.resolve(__dirname, `../../xlsx-templates/organizations_example_${lang}.xlsx`));
    const buffer = this.xlsxService.getOrganizationsFileBuffer(customFields, fileExample);

    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    let fileName: string;
    switch (lang) {
      case 'en':
        fileName = 'Organizations_Template.xlsx';
        break;
      case 'de':
        fileName = 'Unternehmen_Vorlage.xlsx';
        break;
      default:
        throw new Error(`Language ${lang} is not recognized`);
    }
    res.set('Content-Disposition', `attachment; filename=${fileName}`);
    return res.status(200).send(buffer);
  }

  @ApiOperation({
    summary: 'Get xlsx file with all organizations + ready for import organizations',
    description: 'Get all organizations',
  })
  @ApiOkResponse({
    description: 'Organizations file have successfully been queried',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('personsTemplate/:importID/:lang')
  async getPersonsTemplate(
    @Res() res,
    @GetUser() user: LoggedInUser,
    @Param('lang') lang: string = 'en',
    @Param('importID') importID: string,
  ) {
    this.logger.log(`Get persons template user:${user._id}`);
    this.logger.log(user.generateMeta());
    const meta = user.generateMeta();
    const documents = await this.organizationsService.browse(meta);

    let importDocuments = [];

    try {
      const importDocument: any = await this.importService.read(importID, meta);
      if (importDocument) {
        importDocuments = importDocument.data[0].entities;
      }
    } catch (err) {
      this.logger.log('Getting organizations file with the import id which was not created yet');
    }

    const organizationCustomFields =
      await this.customFieldsService.browseByEntity('organization', meta);
    const personCustomFields = await this.customFieldsService.browseByEntity('person', meta);

    const parsedDocuments = [...documents, ...importDocuments]
      .filter(document => document)
      .map(document => ({
        name: document.name,
        _id: document._id,
        email: document.email,
        phoneNumber: document.phoneNumber,
        vatRegNo: document.vatRegNo,
        street: document.address?.street,
        streetNumber: document.address?.number,
        postcode: document.address?.postcode,
        city: document.address?.city,
        country: document.address?.country,
        ...this.getDocumentCustomFields(document, organizationCustomFields),
      }));

    const fileExample = fs.readFileSync(path.resolve(__dirname, `../../xlsx-templates/persons_example_${lang}.xlsx`));

    const customFields = personCustomFields.map(item => item.key);
    const buffer =
      this.xlsxService.getPersonsFileBuffer(parsedDocuments, fileExample, customFields, lang);

    res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    let fileName: string;
    switch (lang) {
      case 'en':
        fileName = 'Persons_Template.xlsx';
        break;
      case 'de':
        fileName = 'Personen_Vorlage.xlsx';
        break;
      default:
        throw new Error(`Language ${lang} is not recognized`);
    }
    res.set('Content-Disposition', `attachment; filename=${fileName}`);
    return res.status(200).send(buffer);
  }

  @ApiOperation({
    summary: 'Upload organizations file for current import',
    description: 'Upload organizations file for current import',
  })
  @ApiOkResponse({
    description: 'Import with organizations has been created successfully',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Post('organizationsFile/:importID')
  @UseInterceptors(FileInterceptor('file'))
  async postOrganizationsFile(
    @Res() res,
    @UploadedFile() file,
    @Param('importID') importID: string,
    @GetUser() user: LoggedInUser,
  ) {
    this.logger.log(`Post organizationsFile importID:${importID} user:${user._id}`);
    const meta = user.generateMeta();
    const organizationCustomFields =
      await this.customFieldsService.browseByEntity('organization', meta);

    const fileData = file.buffer;
    const documents = this.xlsxService.parseFileData(fileData)
      .map((document) => {
        Object.values(PERSON_IMPORT_IGNORED_COLUMNS).forEach((value) => {
          delete document[value];
        });
        return document;
      })
      .filter(document => JSON.stringify(document) !== JSON.stringify({}));
    const result = [];
    const errors = [];
    this.logger.log(`Documents length ${documents.length}`);

    for (let i = 0; i < documents.length; i += 1) {
      const document = documents[i];
      // prepare fields
      this.parseAddress(document);
      this.parseCustomFields(document, organizationCustomFields);
      document._id = uuidv4();
      document.importID = importID;
      if (document.phoneNumber) {
        document.phoneNumber = document.phoneNumber.toString();
      }
      if (document.faxNumber) {
        document.faxNumber = document.faxNumber.toString();
      }
    }

    await Promise.all(
      documents.map((document, i) => validate(new OrganizationDTO(document))
      .then((documentErrors) => {
        if (documentErrors.length) {
          errors.push({ line: i + 2, errors: documentErrors });
        } else {
          result.push(document);
        }
      })),
    );

    let status = 200;

    // edit if exists or create new
    try {
      const importDocument: any = await this.importService.read(importID, meta);
      if (importDocument) {
        importDocument.data[0] = { model: 'organizations', entities: result };
        await this.importService.edit(importID, importDocument, meta);
      }
    } catch (err) {
      await this.importService.add(
        new Import({ _id: importID, data: [{ model: 'organizations', entities: result }] }),
        meta,
      );
      status = 201;
    }
    return res.status(status).json({
      errors,
      data: result.slice(0, MAX_DOCUMENTS_RETURN),
      length: result.length,
    });
  }

  @ApiOperation({
    summary: 'Upload persons file for current import',
    description: 'Upload persons file for current import',
  })
  @ApiOkResponse({
    description: 'Import with persons has been created successfully',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Post('personsFile/:importID')
  @UseInterceptors(FileInterceptor('file'))
  async postPersonsFile(
    @Res() res,
    @UploadedFile() file,
    @Param('importID') importID: string,
    @GetUser() user: LoggedInUser,
  ) {
    this.logger.log(`Post personsFile importID:${importID} user:${user._id}`);
    const meta = user.generateMeta();
    const personCustomFields = await this.customFieldsService.browseByEntity('person', meta);

    const fileData = file.buffer;
    const documents = this.xlsxService.parseFileData(fileData)
      .map((document) => {
        Object.values(PERSON_IMPORT_IGNORED_COLUMNS).forEach((value) => {
          delete document[value];
        });
        return document;
      })
      .filter(document => JSON.stringify(document) !== JSON.stringify({}));
    const result = [];
    const errors = [];

    for (let i = 0; i < documents.length; i += 1) {
      const document = documents[i];
      // prepare fields
      this.parseOrganizationID(document);
      this.parseAddress(document);
      this.parseCustomFields(document, personCustomFields);
      document._id = uuidv4();
      document.importID = importID;
      if (document.phoneNumber) {
        document.phoneNumber = document.phoneNumber.toString();
      }
      if (document.mobileNumber) {
        document.mobileNumber = document.mobileNumber.toString();
      }
      if (document.birthday) {
        document.birthday = moment(document.birthday, 'DD.MM.YYYY').toISOString();
      }
      if (document.gender) {
        document.gender = this.mapGender(document.gender);
      }
    }

    await Promise.all(
      documents.map((document, i) => validate(new PersonDTO(document))
      .then((documentErrors) => {
        if (documentErrors.length) {
          errors.push({ line: i + 2, errors: documentErrors });
        } else {
          result.push(document);
        }
      })),
    );
    let status = 200;

    // edit if exists or create new
    try {
      const importDocument: any = await this.importService.read(importID, meta);
      if (importDocument) {
        importDocument.data[1] = { model: 'persons', entities: result };
        await this.importService.edit(importID, importDocument, meta);
      }
    } catch (err) {
      await this.importService.add(
        new Import({ _id: importID, data: [{ model: 'organizations', entities: [] }, { model: 'persons', entities: result }] }),
        meta,
      );
      status = 201;
    }

    return res.status(status).json({
      errors,
      data: result.slice(0, MAX_DOCUMENTS_RETURN),
      length: result.length,
    });
  }

  @ApiOperation({
    summary: 'Cancel import',
    description: 'Cancel import',
  })
  @ApiOkResponse({
    description: 'Import was canceled successfully',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @Get('cancel/:importID')
  async cancelImport(
    @Res() res,
    @Body() body,
    @Param('importID') importID: string,
    @GetUser() user: LoggedInUser,
  ) {
    this.logger.log(`Cancel import importID:${importID} user:${user._id}`);
    // Remove all organizations with that importID
    // Remove all persons with that importID
    // Remove cached import
    const meta = user.generateMeta();
    this.personsService.deleteByQuery({ importID }, meta);
    this.organizationsService.deleteByQuery({ importID }, meta);
    this.importService.delete(importID, meta);

    return res.status(200).json({ ok: true });
  }

  private parseAddress(document) {
    if (
      document.street ||
      document.streetNumber ||
      document.postcode ||
      document.city ||
      document.country
    ) {
      document.address = {
        street: document.street,
        number: document.streetNumber,
        postcode: document.postcode,
        city: document.city,
        country: document.country,
      };
    }
    delete document.street;
    delete document.streetNumber;
    delete document.postcode;
    delete document.city;
    delete document.country;
  }

  private parseOrganizationID(document) {
    if (document.organizationID || document.role) {
      document.employers = [{
        isContactPerson: false,
        role: document.role || null,
        _organizationID: document?.organizationID,
      }];
    }
    delete document.organizationID;
    delete document.role;
  }

  private mapGender(gender) {
    const parsedGender = gender.toLowerCase().trim();
    if (MALE_GENDERS.includes(parsedGender)) {
      return Gender.Male;
    }
    if (FEMALE_GENDERS.includes(parsedGender)) {
      return Gender.Female;
    }
    return Gender.Other;
  }

  private parseCustomFields(document, customFields: CustomFieldDTO[]) {
    document.customFields = {};
    document.customFieldsErrors = {};
    customFields.forEach((field) => {
      const value = document[`customfield.${field.key}`];
      if (!isEmpty(value)) {
        if (field.type === 'select' || field.type === 'multiselect') {
          const labels = value?.toString()?.split(',').map(value => value.toLowerCase().trim());
          const ids = labels?.map(
            label => field.options?.find(option => option.label.toLowerCase() === label)?.value,
          );

          if (ids.some(id => isEmpty(id))) {
            const notResolvedIndexes = [];
            ids.forEach((id, index) => isEmpty(id) && notResolvedIndexes.push(index));
            const notResolvedLabels = notResolvedIndexes.map(index => labels[index]).join(', ');
            document.customFieldsErrors[field.key] = `Has unresolved options: ${notResolvedLabels}`;
          } else {
            document.customFields[field.key] = ids;
          }
        } else if (field.type === 'currency') {
          const parsedCurrency = parseFloat(value);
          if (isNaN(parsedCurrency)) {
            document.customFieldsErrors[field.key] = 'Should be a number';
          } else {
            document.customFields[field.key] = parsedCurrency;
          }
        } else if (field.type === 'text') {
          document.customFields[field.key] = value.toString();
        }
        delete document[`customfield.${field.key}`];
      }
    });
    if (JSON.stringify(document.customFieldsErrors) === JSON.stringify({})) {
      delete document.customFieldsErrors;
    }
  }

  private getDocumentCustomFields(
    document: PersonDTO | OrganizationDTO,
    customFields: CustomFieldDTO[],
  ): any {
    const result = {};
    if (!customFields || !customFields.length) return result;
    customFields.forEach((field) => {
      if (field.type === 'select' || field.type === 'multiselect') {
        const ids = document?.customFields?.[field.key];
        if (!Array.isArray(field.options)) {
          throw Error(`Wrong custom field ${field._id}:${field.name}`);
        }
        if (ids) {
          if (!Array.isArray(ids)) {
            throw Error(`Wrong custom field value: "${ids}". Field: ${field._id}:${field.name} Document: ${document._id}`);
          }
          const labels = ids?.map(
            id => field.options?.find(option => option.value === id)?.label,
          );
          result[`customfield.${field.key}`] = labels?.join(', ');
        } else {
          result[`customfield.${field.key}`] = '';
        }
      } else {
        result[`customfield.${field.key}`] = document?.customFields?.[field.key];
      }
    });
    return result;
  }
}
