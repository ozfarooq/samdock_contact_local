import { AddImportEventHandler } from './add-import.event-handler';
import { DeleteImportEventHandler } from './delete-import.event-handler';
import { EditImportEventHandler } from './edit-import.event-handler';

export const ImportEventHandlers = [
  AddImportEventHandler,
  DeleteImportEventHandler,
  EditImportEventHandler,
];
