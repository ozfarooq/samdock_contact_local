import { CustomFieldDeletedEvent } from '../impl';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldsRepository } from '../../repositories/custom-fields.repository';

@EventsHandler(CustomFieldDeletedEvent)
export class CustomFieldDeletedEventHandler
  implements IEventHandler<CustomFieldDeletedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: CustomFieldsRepository) { }

  async handle(event: CustomFieldDeletedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, key, entity, meta } = event;
    try {
      await this.repository.delete(_id, key, entity, meta);
    } catch (error) {
      this.logger.error(`Failed to delete custom field of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
