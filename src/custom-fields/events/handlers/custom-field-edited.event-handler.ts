import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { CustomFieldsRepository } from '../../repositories/custom-fields.repository';
import { CustomFieldEditedEvent } from '@daypaio/domain-events/custom-fields';

@EventsHandler(CustomFieldEditedEvent)
export class CustomFieldEditedEventHandler
  implements IEventHandler<CustomFieldEditedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: CustomFieldsRepository) { }

  async handle(event: CustomFieldEditedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, entity, data, meta } = event;

    try {
      await this.repository.edit(_id, entity, data, meta);
    } catch (error) {
      this.logger.error(`Failed to edit inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
