import { CustomFieldType, ValueSetCustomField } from '@daypaio/domain-events/custom-fields';
import { ApiProperty } from '@nestjs/swagger';

export class ValueSetCustomFieldDTO implements ValueSetCustomField {
  @ApiProperty({
    description: 'From value',
  })
  from: string;

  @ApiProperty({
    description: 'To value',
  })
  to: string;

  @ApiProperty({
    description: 'Custom field key',
  })
  key: string;

  @ApiProperty({
    description: 'Custom field type',
    enum: ['select', 'multiselect'],
    default: 'select',
  })
  type: CustomFieldType;
}
