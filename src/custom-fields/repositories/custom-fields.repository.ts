import { CustomField } from '../models/custom-field.model';
import { PersonCustomField } from '../models/person-custom-field.model';
import { OrganizationCustomField } from '../models/organization-custom-field.model';
import { NotFoundException, InternalServerErrorException, Logger } from '@nestjs/common';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldEntity, ValueSetCustomField } from '@daypaio/domain-events/custom-fields';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Person } from '../../persons/models/person.model';
import { Organization } from '../../organizations/models/organization.model';

const MAX_CUSTOM_FIELDS_PER_ENTITY = 50;

export type BrowseAllCustomFields = {
  persons: PersonCustomField[],
  organizations: OrganizationCustomField[],
};

export class CustomFieldsRepository {
  private logger: Logger;

  constructor(
    @InjectModel(PersonCustomField)
    private personCustomFieldModel: ReturnModelType<typeof PersonCustomField>,
    @InjectModel(OrganizationCustomField)
    private organizationCustomFieldModel: ReturnModelType<typeof OrganizationCustomField>,
    @InjectModel(Person)
    private personModel: ReturnModelType<typeof Person>,
    @InjectModel(Organization)
    private organizationModel: ReturnModelType<typeof Organization>,
  ) {
    this.logger = new Logger(`${this.constructor.name}`);
  }

  protected generateErrorMessage(
    error: any,
    operation: string,
    id?: string,
    data?: any,
  ) {
    const errorMessage = error.message;
    const operationMessage = `${
      this.constructor.name
    } could not be ${operation.toLowerCase()}ed}`;
    const idMessage = id ? `ID: ${id}` : '';
    const dataMessage = data ? JSON.stringify(data) : '';
    return {
      error: operationMessage + errorMessage,
      data: idMessage + dataMessage,
      verbose: `${error.constructor.name} \n
        ${operationMessage} \n
        ${errorMessage} \n
        ${idMessage} \n
        ${dataMessage}`,
    };
  }

  private generateKey(name: string) {
    return name.toLowerCase().replace(/ /g, '_');
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE');
    const _tenantID = meta._tenantID;
    data._tenantID = _tenantID;
    data.createdBy = meta._userID;
    console.table(data);
    try {
      const model = this.getCustomFieldModel(data.entity);

      const allDocuments = await model.find({ _tenantID });

      this.logger.log(`Current documents length ${allDocuments.length}`);
      if (allDocuments.length >= MAX_CUSTOM_FIELDS_PER_ENTITY) {
        throw new Error(`Tenant ${_tenantID} already has maximum amount of custom fields for ${data.entity}`);
      }

      const key = data.key;
      let duplicate = allDocuments.some(document => document.key === key);

      if (!duplicate) {
        await model.create(data);
      } else {
        // found duplicate - regenerating key
        const generatedKey = this.generateKey(data.name);
        let lastIteration = parseInt(key?.split(generatedKey)[1], 10) + 1;
        if (!lastIteration) {
          lastIteration = 1;
        }
        let currentKey: string;
        while (duplicate) {
          currentKey = `${generatedKey}${lastIteration}`;
          duplicate = allDocuments.some(document => document.key === currentKey);
          lastIteration += 1;
        }
        data.key = currentKey;
        await model.create(data);
      }
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async edit(id: string, entity: CustomFieldEntity, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    console.table({ data, _id: id });
    data.updatedBy = meta._userID;
    try {
      const model = this.getCustomFieldModel(entity);
      const result = await model?.updateOne(
        { _tenantID: meta._tenantID, _id: id },
        { $set: data },
        { $push: { options: data.options } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, data);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async setValue(
    id: string,
    entity: CustomFieldEntity,
    data: ValueSetCustomField,
    meta: EventMetaData,
  ): Promise<void> {
    const _tenantID = meta._tenantID;
    this.logger.verbose('SET VALUE');
    console.table({ id, entity, data });
    try {
      const model: any = this.getEntityModel(entity);
      const customFieldKey = `customFields.${data.key}`;
      if (data.type === 'select') {
        const result = await model.updateMany(
          { _tenantID, [customFieldKey]: data.from }, { [customFieldKey]: data.to },
        );
        this.logger.log(result);
        return;
      }
      if (data.type === 'multiselect') {
        if (data.to !== null && data.to !== undefined) {
          const foundIteminCustomFieldKey = `${customFieldKey}.$`;
          const result = await model.updateMany(
            {
              _tenantID,
              $and: [
                { [customFieldKey]: data.from },
                { [customFieldKey]: { $ne: data.to } },
              ],
            },
            { $set: { [foundIteminCustomFieldKey]: data.to } },
          );
          this.logger.log(result);
        }

        await model.updateMany(
          { _tenantID, [customFieldKey]: data.from }, { $pull: { [customFieldKey]: data.from } },
        );
        return;
      }
    } catch (error) {
      const message = this.generateErrorMessage(error, 'delete', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async delete(
    id: string,
    key: string,
    entity: CustomFieldEntity,
    meta: EventMetaData,
  ): Promise<void> {
    this.logger.verbose('DELETE');
    const _tenantID = meta._tenantID;
    console.table({ id });
    try {
      const model = this.getCustomFieldModel(entity);
      const result = await model.deleteOne({
        key,
        _tenantID,
        _id: id,
      });
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        const entityModel: any = this.getEntityModel(entity);
        const property = `customFields.${key}`;
        const updateResult = await entityModel?.updateMany(
          { _tenantID }, { $unset: { [property]: 1 } },
        );
        this.logger.log(updateResult);
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${model.modelName} was found with id: ${id} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'delete', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseByEntity(entity: CustomFieldEntity, meta: EventMetaData): Promise<CustomField[]> {
    this.logger.verbose('FIND');
    try {
      const model = this.getCustomFieldModel(entity);
      const result = await model.find({ _tenantID: meta._tenantID });
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browse(
    meta: EventMetaData,
  ): Promise<BrowseAllCustomFields> {
    this.logger.verbose('FIND');
    const result = { persons: [], organizations: [] };
    try {
      const models = [this.getCustomFieldModel('person'), this.getCustomFieldModel('organization')];
      const resultArray = await Promise.all(
        models.map(model => model.find({ _tenantID: meta._tenantID })),
      );
      result.persons = resultArray[0];
      result.organizations = resultArray[1];
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  private getCustomFieldModel(entity: CustomFieldEntity) {
    if (entity === 'person') return this.personCustomFieldModel;
    if (entity === 'organization') return this.organizationCustomFieldModel;
    throw Error(`Custom field entity "${entity}" not exists`);
  }

  private getEntityModel(entity: CustomFieldEntity) {
    if (entity === 'person') return this.personModel;
    if (entity === 'organization') return this.organizationModel;
    throw Error(`Custom field entity "${entity}" not exists`);
  }
}
