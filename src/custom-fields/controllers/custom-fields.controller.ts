import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBody,
  ApiForbiddenResponse,
  ApiParam,
} from '@nestjs/swagger';
import { Controller, Logger, Get, Param, Patch, Body, Post, Delete } from '@nestjs/common';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { AddCustomFieldDTO, BrowseAllDTO, CustomFieldDTO, CustomFieldEntityEnum } from '../dtos/custom-field.dto';
import { CustomFieldsService } from '../services/custom-fields.service';
import { CustomFieldEntity, ICustomField, ValueSetCustomField } from '@daypaio/domain-events/custom-fields';
import { BrowseAllCustomFields } from '../repositories/custom-fields.repository';
import { ValueSetCustomFieldDTO } from '../dtos/value-set-custom-field.dto';

@ApiTags('custom fields')
@ApiBearerAuth()
@Controller('custom-fields')
export class CustomFieldsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: CustomFieldsService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse all Custom fields',
    description: 'Get all custom fields',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: BrowseAllDTO,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<BrowseAllCustomFields> {
    this.logger.log(`BROWSE user:${user._id}`);
    return this.service.browse(user.generateMeta());
  }

  @Get('/:entity')
  @ApiParam({ name: 'entity', enum: CustomFieldEntityEnum })
  @ApiOperation({
    summary: 'Browse Custom fields by entity',
    description: 'Get custom fields by entity',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: [CustomFieldDTO],
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browseByEntity(
    @Param('entity') entity: CustomFieldEntity,
    @GetUser() user: LoggedInUser,
  ): Promise<CustomFieldDTO[]> {
    this.logger.log(`BROWSE BY ENTITY, entity:${entity} user:${user._id}`);
    return this.service.browseByEntity(entity, user.generateMeta());
  }

  @Patch('values/:entity/:id')
  @ApiParam({ name: 'entity', enum: CustomFieldEntityEnum })
  @ApiBody({ type: ValueSetCustomFieldDTO })
  @ApiOperation({
    summary: 'Set value of Custom Field',
    description: 'Set value of Custom Field',
  })
  @ApiOkResponse({
    description:
      'The custom field set value event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async setValue(
    @Param('id') id: string,
    @Param('entity') entity: CustomFieldEntity,
    @Body() data: ValueSetCustomField,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`SET value of custom field from ${data.from} to ${data.to} user:${user._id}`);
    return this.service.setValue(id, entity, data, user.generateMeta());
  }

  @Patch('/:entity/:id')
  @ApiParam({ name: 'entity', enum: CustomFieldEntityEnum })
  @ApiBody({ type: CustomFieldDTO })
  @ApiOperation({
    summary: 'Edit Custom Field',
    description: 'Edit an Custom Field, id is not required in the body',
  })
  @ApiOkResponse({
    description:
      'The custom field editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Param('entity') entity: CustomFieldEntity,
    @Body() customField: Partial<ICustomField>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    return this.service.edit(id, entity, customField, user.generateMeta());
  }

  @Post()
  @ApiBody({ type: AddCustomFieldDTO })
  @ApiOperation({
    summary: 'Add a Custom Field',
    description: 'Create a custom field',
  })
  @ApiOkResponse({
    description:
      'The Custom field adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() customField: CustomFieldDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${customField._id} user:${user._id}`);
    return this.service.add(customField, user.generateMeta());
  }

  @Delete(':entity/:key/:id')
  @ApiParam({ name: 'entity', enum: CustomFieldEntityEnum })
  @ApiOperation({
    summary: 'Delete a custom field',
    description: 'Delete a custom field',
  })
  @ApiOkResponse({
    description:
      'The custom field deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @Param('key') key: string,
    @Param('entity') entity: CustomFieldEntity,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    return this.service.delete(id, key, entity, user.generateMeta());
  }

}
