import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { CustomFieldsController } from './controllers/custom-fields.controller';
import { CustomFieldsEventHandlers } from './events/handlers';
import { CustomFieldsQueryHandlers } from './queries/handlers';
import { CustomFieldsRepository } from './repositories/custom-fields.repository';
import { OrganizationCustomField } from './models/organization-custom-field.model';
import { PersonCustomField } from './models/person-custom-field.model';
import { CustomFieldsService } from './services/custom-fields.service';
import { CustomFieldStreamSaga } from './sagas/delete-custom-field-stream.saga';
import { CustomFieldsCommandHandlers } from './commands/handlers';
import { Person } from '../persons/models/person.model';
import { Organization } from '../organizations/models/organization.model';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: PersonCustomField,
        schemaOptions: {
          collection: 'person-custom-fields',
          timestamps: true,
        },
      },
      {
        typegooseClass: OrganizationCustomField,
        schemaOptions: {
          collection: 'organization-custom-fields',
          timestamps: true,
        },
      },
      Person,
      Organization,
    ]),
  ],
  controllers: [
    CustomFieldsController,
  ],
  providers: [
    CustomFieldsService,
    CustomFieldsRepository,
    ...CustomFieldsCommandHandlers,
    ...CustomFieldsQueryHandlers,
    ...CustomFieldsEventHandlers,
    CustomFieldStreamSaga,
  ],
  exports: [
    CustomFieldsRepository,
    CustomFieldsService,
  ],
})
export class CustomFieldsModule { }
