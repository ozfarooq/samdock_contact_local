import { modelOptions } from '@typegoose/typegoose';
import { CustomField } from './custom-field.model';

@modelOptions({ schemaOptions: { collection: 'organization-custom-fields' } })
export class OrganizationCustomField extends CustomField {}
