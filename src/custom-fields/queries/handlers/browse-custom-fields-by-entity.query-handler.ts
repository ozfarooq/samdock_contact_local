import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { BrowseCustomFieldsByEntityQuery } from '../impl/browse-custom-fields-by-entity.query';
import { CustomFieldsRepository } from '../../repositories/custom-fields.repository';
import { CustomField } from '../../models/custom-field.model';

@QueryHandler(BrowseCustomFieldsByEntityQuery)
export class BrowseCustomFieldsByEntityQueryHandler
  implements IQueryHandler<BrowseCustomFieldsByEntityQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: CustomFieldsRepository) { }

  async execute(query: BrowseCustomFieldsByEntityQuery): Promise<CustomField[]> {
    this.logger.log('Query triggered');
    const { entity, meta } = query;
    try {
      return await this.repo.browseByEntity(entity, meta);
    } catch (error) {
      this.logger.error('Failed to browse on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
