import { BrowseCustomFieldsByEntityQueryHandler } from './browse-custom-fields-by-entity.query-handler';
import { BrowseCustomFieldsQueryHandler } from './browse-custom-fields.query-handler';

export const CustomFieldsQueryHandlers = [
  BrowseCustomFieldsQueryHandler,
  BrowseCustomFieldsByEntityQueryHandler,
];
