import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldEntity } from '@daypaio/domain-events/custom-fields';

export class BrowseCustomFieldsByEntityQuery implements IQuery {
  constructor(public entity: CustomFieldEntity, public meta: EventMetaData) {}
}
