import { Injectable, Logger, NotImplementedException } from '@nestjs/common';
import { CommandBus, ICommand, IQuery, QueryBus } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldDTO } from '../dtos/custom-field.dto';
import { BrowseCustomFieldsQuery } from '../queries/impl/browse-custom-fields.query';
import { CustomFieldEntity, ValueSetCustomField } from '@daypaio/domain-events/custom-fields';
import { EditCustomFieldCommand } from '../commands/impl/edit-custom-field.command';
import { AddCustomFieldCommand } from '../commands/impl/add-custom-field.command';
import { DeleteCustomFieldCommand } from '../commands/impl/delete-custom-field.command';
import { BrowseAllCustomFields } from '../repositories/custom-fields.repository';
import { BrowseCustomFieldsByEntityQuery } from '../queries/impl/browse-custom-fields-by-entity.query';
import { SetValueCustomFieldCommand } from '../commands/impl/set-value-custom-field.command';

@Injectable()
export class CustomFieldsService {
  protected logger: Logger;

  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {
    this.logger = new Logger(this.constructor.name);
  }

  protected async executeCommand(command: ICommand): Promise<void> {
    if (this.commandBus) {
      this.logger.log(`${command.constructor.name} executed`);
      return await this.commandBus.execute(command);
    }
    this.logger.error(
      `Could not execute command of type ${command.constructor.name} on the command bus of wrong type
      `,
    );
  }

  protected async executeQuery(query: IQuery): Promise<any> {
    if (this.queryBus) {
      this.logger.log(`${query.constructor.name} executed`);
      return await this.queryBus.execute(query);
    }
    this.logger.error(
      `Could not execute query of type ${query.constructor.name} on the query bus of wrong type
      `,
    );
  }

  async browse(meta: EventMetaData): Promise<BrowseAllCustomFields> {
    return await this.executeQuery(new BrowseCustomFieldsQuery(meta));
  }

  async browseByEntity(entity: CustomFieldEntity, meta: EventMetaData): Promise<CustomFieldDTO[]> {
    return await this.executeQuery(new BrowseCustomFieldsByEntityQuery(entity, meta));
  }

  async edit(
    id: string,
    entity: CustomFieldEntity,
    data: Partial<CustomFieldDTO>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditCustomFieldCommand(id, entity, data, meta),
    );
  }

  async setValue(
    id: string,
    entity: CustomFieldEntity,
    data: ValueSetCustomField,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new SetValueCustomFieldCommand(id, entity, data, meta),
    );
  }

  async add(data: CustomFieldDTO, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new AddCustomFieldCommand(data._id, data, meta));
  }

  async delete(
    id: string,
    key: string,
    entity: CustomFieldEntity,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(new DeleteCustomFieldCommand(id, key, entity, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const personCustomFields = items.persons.map(item =>
      ({ id: item._id, key: item.key, entity: 'person' as CustomFieldEntity }),
    );
    const organizationCustomFields = items.organizations.map(item =>
      ({ id: item._id, key: item.key, entity: 'organization' as CustomFieldEntity }),
    );
    return this.bulkDelete([...personCustomFields, ...organizationCustomFields], meta);
  }

  async bulkDelete(
    items: { id: string, key: string, entity: CustomFieldEntity }[],
    meta: EventMetaData,
  ): Promise<void> {
    await Promise.all(items.map(async ({ id, key, entity }) => {
      return await this.executeCommand(new DeleteCustomFieldCommand(id, key, entity, meta));
    }));
  }
}
