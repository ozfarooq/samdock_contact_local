import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { SetValueCustomFieldCommand } from '../impl/set-value-custom-field.command';
import { CustomFieldAggregate } from '../../models/custom-field.aggregate';
import { CustomFieldSetValueEvent } from '@daypaio/domain-events/custom-fields';

@CommandHandler(SetValueCustomFieldCommand)
export class SetValueCustomFieldCommandHandler
  implements ICommandHandler<SetValueCustomFieldCommand> {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: SetValueCustomFieldCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, entity, data, meta } = command;

    const customFieldAggregate = this.publisher.mergeObjectContext(
      new CustomFieldAggregate({ _id }),
    );
    customFieldAggregate.apply(
      new CustomFieldSetValueEvent(
        _id,
        entity,
        data,
        meta,
      ),
    );
    customFieldAggregate.commit();
  }
}
