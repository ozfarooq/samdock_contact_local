import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { CustomFieldEntity } from '@daypaio/domain-events/custom-fields';

export class DeleteCustomFieldCommand implements ICommand {
  constructor(
      public readonly _id: string,
      public readonly key: string,
      public readonly entity: CustomFieldEntity,
      public readonly meta: EventMetaData,
    ) { }
}
