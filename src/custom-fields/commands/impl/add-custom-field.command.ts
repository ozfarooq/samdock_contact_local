import { ICustomField } from '@daypaio/domain-events/custom-fields';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ICommand } from '@nestjs/cqrs';

export class AddCustomFieldCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly customField: ICustomField,
    public readonly meta: EventMetaData,
  ) { }
}
