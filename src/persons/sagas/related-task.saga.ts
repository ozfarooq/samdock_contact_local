import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { TaskAddedEvent, TaskEditedEvent, TaskDeletedEvent } from '@daypaio/domain-events/tasks';
import { TaskLinkedToPersonCommand } from '../commands/impl/task-linked-to-person.command';
import { TaskUnlinkedFromPersonCommand } from '../commands/impl/task-unlinked-from-person.command';
import { PersonRelatedTaskCompletedCommand } from '../commands/impl/person-related-task-completed.command';
import { PersonRelatedTaskReopenedCommand } from '../commands/impl/person-related-task-reopened.command';

export class PRelatedTaskSaga {

  constructor() {}

  @Saga()
  pRelatedTaskAdded = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(TaskAddedEvent),
      filter(event => event.constructor.name === 'TaskAddedEvent'),
      map(
        (event: TaskAddedEvent) => {
          const { task, meta } = event;
          if (task._personID) {
            return new TaskLinkedToPersonCommand(task._personID, task._id, meta);
          }
        },
      ),
    );
  }

  @Saga()
  pRelatedTaskChanged = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(TaskEditedEvent),
      filter(event => event.constructor.name === 'TaskEditedEvent'),
      map(
        (event: TaskEditedEvent) => {
          const { task, oldTask, meta } = event;
          if (task._personID) {
            return new TaskLinkedToPersonCommand(task._personID, oldTask._id, meta);
          }
          if (task._personID === null) {
            return new TaskUnlinkedFromPersonCommand(oldTask._personID, oldTask._id, meta);
          }
          if (oldTask._personID && task.completionDate) {
            return new PersonRelatedTaskCompletedCommand(oldTask._personID, oldTask._id, meta);
          }
          if (oldTask._personID && task.completionDate === null) {
            return new PersonRelatedTaskReopenedCommand(oldTask._personID, oldTask._id, meta);
          }
        },
      ),
    );
  }

}
