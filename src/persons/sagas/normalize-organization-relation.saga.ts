import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonRelatedToOrganizationEvent, PersonUnrelatedFromOrganizationEvent } from '@daypaio/domain-events/organizations';
import { RelateOrganizationToPersonCommand } from '../commands/impl/relate-organization-to-person.command';
import { UnrelateOrganizationFromPersonCommand } from '../commands/impl/unrelate-organization-from-person.command';

export class NormalizeOrganizationRelationSaga {
  @Saga()
  relationCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonRelatedToOrganizationEvent),
      map(
        (event: PersonRelatedToOrganizationEvent) => {
          const { _organizationID, _personID, meta } = event;
          return new RelateOrganizationToPersonCommand(_personID, _organizationID, meta);
        },
      ),
    );
  }

  @Saga()
  relationDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonUnrelatedFromOrganizationEvent),
      map(
        (event: PersonUnrelatedFromOrganizationEvent) => {
          const { _organizationID, _personID, meta } = event;
          return new UnrelateOrganizationFromPersonCommand(_personID, _organizationID, meta);
        },
      ),
    );
  }
}
