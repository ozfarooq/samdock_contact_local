import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeadPromotedToDealEvent, DealDemotedToLeadEvent } from '@daypaio/domain-events/deals';
import { PRelatedLeadPromotedToDealCommand } from '../commands/impl/related-lead-promoted-to-deal.command';
import { PRelatedDealDemotedToLeadCommand } from '../commands/impl/related-deal-demoted-to-lead.command';

export class PRelatedDealPromotedOrDemotedSaga {

  @Saga()
  pLeadPromotedToDeal = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadPromotedToDealEvent),
      map(
        (event: LeadPromotedToDealEvent) => {
          const { _dealID, meta } = event;
          return new PRelatedLeadPromotedToDealCommand(_dealID, meta);
        },
      ),
    );
  }

  @Saga()
  pDealDemotedToLead = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealDemotedToLeadEvent),
      map(
        (event: DealDemotedToLeadEvent) => {
          const { _dealID, meta } = event;
          return new PRelatedDealDemotedToLeadCommand(_dealID, meta);
        },
      ),
    );
  }

}
