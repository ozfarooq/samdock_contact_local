import { Injectable } from '@nestjs/common';
import { DeleteStreamSaga } from '../../shared/sagas/delete-stream.saga';
import { PersonDeletedEvent } from '../events/impl';

@Injectable()
export class DeletePersonStreamSaga extends DeleteStreamSaga {
  constructor() {
    super([PersonDeletedEvent], 'persons');
  }
}
