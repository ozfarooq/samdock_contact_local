import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeadQualifiedEvent } from '@daypaio/domain-events/deals/impl/lead-qualified-event';
import { PRelatedLeadQualifiedCommand } from '../commands/impl/related-lead-qualified.command';

export class PRelatedLeadQualifiedSaga {

  @Saga()
  pRelatedLeadQualified = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadQualifiedEvent),
      map(
        (event: LeadQualifiedEvent) => {
          const { _leadID, meta } = event;
          return new PRelatedLeadQualifiedCommand(_leadID,  meta);
        },
      ),
    );
  }

}
