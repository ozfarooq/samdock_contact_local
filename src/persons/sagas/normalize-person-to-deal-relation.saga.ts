import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  PersonRelatedToDealEvent,
  PersonUnrelatedFromDealEvent,
} from '@daypaio/domain-events/deals';
import { RelateDealToPersonCommand } from '../commands/impl/relate-deal-to-person.command';
import { UnrelateDealFromPersonCommand } from '../commands/impl/unrelate-deal-from-person.command';

export class NormalizePersonToDealRelationSaga {
  @Saga()
  relationCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonRelatedToDealEvent),
      map(
        (event: PersonRelatedToDealEvent) => {
          const { _dealID, _personID, meta } = event;
          return new RelateDealToPersonCommand(_personID, _dealID,  meta);
        },
      ),
    );
  }

  @Saga()
  relationDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(PersonUnrelatedFromDealEvent),
      map(
        (event: PersonUnrelatedFromDealEvent) => {
          const { _dealID, _personID, meta } = event;
          return new UnrelateDealFromPersonCommand(_personID, _dealID,  meta);
        },
      ),
    );
  }

}
