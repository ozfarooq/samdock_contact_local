import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PRelatedLeadUnqualifiedCommand } from '../commands/impl/related-lead-unqualified.command';
import { LeadUnqualifiedEvent } from '@daypaio/domain-events/deals';

export class PRelatedLeadUnqualifiedSaga {

  @Saga()
  pRelatedLeadQualified = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadUnqualifiedEvent),
      map(
        (event: LeadUnqualifiedEvent) => {
          const { _dealID, meta } = event;
          return new PRelatedLeadUnqualifiedCommand(_dealID,  meta);
        },
      ),
    );
  }

}
