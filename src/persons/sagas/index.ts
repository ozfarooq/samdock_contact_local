import { DeletePersonStreamSaga } from './delete-person-stream.saga';
import { NormalizeOrganizationRelationSaga } from './normalize-organization-relation.saga';
import { NormalizePersonToDealRelationSaga } from './normalize-person-to-deal-relation.saga';
import { NormalizeContactPersonDealRelationSaga } from './normalize-contact-person-deal-relation.saga';
import { PRelatedDealLostSaga } from './related-deal-lost.saga';
import { PRelatedDealWonSaga } from './related-deal-won.saga';
import { PRelatedDealStageChangedSaga } from './related-deal-status-changed.saga';
import { PersonCreatedFromDraftSaga } from './person-created-from-draft.saga';
import { NormalizeOrganizationRoleChangedSaga } from './normalize-organization-role-relation.saga';
import { NormalizePersonToLeadRelationSaga } from './normalize-person-to-lead-relation.saga';
import { PRelatedDealPromotedOrDemotedSaga } from './related-deal-promoted-or-demoted.saga';
import { PRelatedLeadQualifiedSaga } from './related-lead-qualified.saga';
import { PRelatedTaskSaga } from './related-task.saga';
import { NormalizeContactPersonLeadRelationSaga } from './normalize-contact-person-lead-relation.saga';
import { PRelatedLeadUnqualifiedSaga } from './related-lead-unqualified.saga';

export const PersonSagas = [
  DeletePersonStreamSaga,
  NormalizeOrganizationRelationSaga,
  NormalizePersonToDealRelationSaga,
  NormalizePersonToLeadRelationSaga,
  NormalizeContactPersonDealRelationSaga,
  PRelatedDealLostSaga,
  PRelatedDealWonSaga,
  PRelatedDealStageChangedSaga,
  PersonCreatedFromDraftSaga,
  NormalizeOrganizationRoleChangedSaga,
  PRelatedDealPromotedOrDemotedSaga,
  PRelatedLeadQualifiedSaga,
  PRelatedTaskSaga,
  NormalizeContactPersonLeadRelationSaga,
  PRelatedLeadUnqualifiedSaga,
];
