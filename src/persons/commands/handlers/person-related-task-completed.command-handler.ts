import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRelatedTaskCompletedCommand }
  from '../impl/person-related-task-completed.command';
import { PersonRelatedTaskCompletedEvent } from '@daypaio/domain-events/persons';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(PersonRelatedTaskCompletedCommand)
export class PersonRelatedTaskCompletedCommandHandler
  implements ICommandHandler<PersonRelatedTaskCompletedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: PersonRelatedTaskCompletedCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: PersonRelatedTaskCompletedCommandHandler...',
    );
    const { _personID, _taskID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(
      new PersonRelatedTaskCompletedEvent(_personID, _taskID, meta),
    );
    personAggregate.commit();
  }
}
