import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UnrelateOrganizationFromPersonCommand } from '../impl/unrelate-organization-from-person.command';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(UnrelateOrganizationFromPersonCommand)
export class UnrelateOrganizationFromPersonCommandHandler
  implements ICommandHandler<UnrelateOrganizationFromPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateOrganizationFromPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelateOrganizationFromPersonCommand...');
    const { _personID, _organizationID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.unrelateFromOrganization(_organizationID, meta);
    personAggregate.commit();
  }
}
