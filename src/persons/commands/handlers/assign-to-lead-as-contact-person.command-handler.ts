import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { AssignToLeadAsContactPersonCommand } from '../impl/assign-to-lead-as-contact-person.command';

@CommandHandler(AssignToLeadAsContactPersonCommand)
export class AssignToLeadAsContactPersonCommandHandler
  implements ICommandHandler<AssignToLeadAsContactPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AssignToLeadAsContactPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: AssignToDealAsContactPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.assignToLeadAsContactPerson(_dealID, meta);
    personAggregate.commit();
  }
}
