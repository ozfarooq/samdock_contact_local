import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { DraftCreatedForPersonEvent } from '@daypaio/domain-events/persons';
import { DraftCreatedForPersonCommand } from '../impl/draft-created-for-person.command';

@CommandHandler(DraftCreatedForPersonCommand)
export class DraftCreatedForPersonCommandHandler
  implements ICommandHandler<DraftCreatedForPersonCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: DraftCreatedForPersonCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: DraftCreatedForPersonCommandHandler...',
    );
    const { _id, data, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id }),
    );
    personAggregate.apply(new DraftCreatedForPersonEvent(_id, data, meta));
    personAggregate.commit();
  }
}
