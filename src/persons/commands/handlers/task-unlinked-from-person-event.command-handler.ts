import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TaskUnlinkedFromPersonCommand }
  from '../impl/task-unlinked-from-person.command';
import { TaskUnlinkedFromPersonEvent } from '@daypaio/domain-events/persons';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(TaskUnlinkedFromPersonCommand)
export class TaskUnlinkedFromPersonCommandHandler
  implements ICommandHandler<TaskUnlinkedFromPersonCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: TaskUnlinkedFromPersonCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: TaskUnlinkedFromPersonCommandHandler...',
    );
    const { _personID, _taskID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(
      new TaskUnlinkedFromPersonEvent(_personID, _taskID, meta),
    );
    personAggregate.commit();
  }
}
