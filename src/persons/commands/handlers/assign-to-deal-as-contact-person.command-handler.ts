import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { AssignToDealAsContactPersonCommand } from '../impl/assign-to-deal-as-contact-person.command';

@CommandHandler(AssignToDealAsContactPersonCommand)
export class AssignToDealAsContactPersonCommandHandler
  implements ICommandHandler<AssignToDealAsContactPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AssignToDealAsContactPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: AssignToDealAsContactPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.assignToDealAsContactPerson(_dealID, meta);
    personAggregate.commit();
  }
}
