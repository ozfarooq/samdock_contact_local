import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RelateDealToPersonCommand } from '../impl/relate-deal-to-person.command';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(RelateDealToPersonCommand)
export class RelateDealToPersonCommandHandler
  implements ICommandHandler<RelateDealToPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateDealToPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: RelateDealToPersonCommand...');
    const { _personID, _dealID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.relateDeal(_dealID, meta);
    personAggregate.commit();
  }
}
