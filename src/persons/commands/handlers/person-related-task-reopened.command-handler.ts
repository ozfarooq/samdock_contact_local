import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRelatedTaskReopenedCommand }
  from '../impl/person-related-task-reopened.command';
import { PersonRelatedTaskReopenedEvent } from '@daypaio/domain-events/persons';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(PersonRelatedTaskReopenedCommand)
export class PersonRelatedTaskReopenedCommandHandler
  implements ICommandHandler<PersonRelatedTaskReopenedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: PersonRelatedTaskReopenedCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: PersonRelatedTaskReopenedCommandHandler...',
    );
    const { _personID, _taskID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(
      new PersonRelatedTaskReopenedEvent(_personID, _taskID, meta),
    );
    personAggregate.commit();
  }
}
