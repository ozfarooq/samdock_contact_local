import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonAggregate } from '../../models/person.aggregate';
import { PRelatedDealDemotedToLeadCommand } from '../impl/related-deal-demoted-to-lead.command';
import { PersonRelatedDealDemotedToLeadEvent } from '@daypaio/domain-events/persons';

@CommandHandler(PRelatedDealDemotedToLeadCommand)
export class PRelatedDealDemotedToLeadCommandHandler
  implements ICommandHandler<PRelatedDealDemotedToLeadCommand> {
  private logger: Logger;
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: PRelatedDealDemotedToLeadCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _dealID, meta } = command;
    const persons = await this.personRepository.browseByDeals(_dealID, meta);
    for (const person of persons) {
      const aggregate = this.publisher.mergeObjectContext(
        new PersonAggregate(person),
      );
      aggregate.apply(new PersonRelatedDealDemotedToLeadEvent(person._id, _dealID, meta));
      aggregate.commit();
    }
  }
}
