import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonAggregate } from '../../models/person.aggregate';
import { RelateLeadToPersonCommand } from '../impl/relate-lead-to-person.command';
import { LeadRelatedToPersonEvent } from '@daypaio/domain-events/persons';

@CommandHandler(RelateLeadToPersonCommand)
export class RelateLeadToPersonCommandHandler
  implements ICommandHandler<RelateLeadToPersonCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateLeadToPersonCommand) {
    this.logger.log('COMMAND TRIGGERED: RelateDealToPersonCommand...');
    const { _personID, _leadID, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate({ _id: _personID }),
    );
    personAggregate.apply(new LeadRelatedToPersonEvent(_personID, _leadID, meta));
    personAggregate.commit();
  }
}
