import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DeletePersonsByQueryCommand } from '../impl/delete-persons-by-query.command';
import { EventPublisher } from 'nestjs-eventstore';
import { PersonAggregate } from '../../models/person.aggregate';

@CommandHandler(DeletePersonsByQueryCommand)
export class DeletePersonsByQueryCommandHandler
  implements ICommandHandler<DeletePersonsByQueryCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeletePersonsByQueryCommand) {
    this.logger.log('COMMAND TRIGGERED: DeletePersonsByQuery...');
    const { query, meta } = command;
    const personAggregate = this.publisher.mergeObjectContext(
      new PersonAggregate(),
    );
    personAggregate.deleteByQuery(query, meta);
    personAggregate.commit();
  }
}
