import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AssignToLeadAsContactPersonCommand implements ICommand {
  constructor(
    public readonly _personID: string,
    public readonly _dealID: string,
    public readonly meta: EventMetaData,
  ) {}
}
