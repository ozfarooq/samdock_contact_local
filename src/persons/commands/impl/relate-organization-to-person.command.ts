import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class RelateOrganizationToPersonCommand implements ICommand {
  constructor(
    public readonly _personID: string,
    public readonly _organizationID: string,
    public readonly meta: EventMetaData,
  ) {}
}
