import { ICommand } from '@nestjs/cqrs';
import { IPerson } from '../../models/person.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditPersonCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly person: Partial<IPerson>,
    public readonly meta: EventMetaData,
  ) {}
}
