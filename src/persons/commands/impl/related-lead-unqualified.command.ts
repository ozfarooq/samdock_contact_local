import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class PRelatedLeadUnqualifiedCommand implements ICommand {
  constructor(
    public readonly _leadID: string,
    public readonly meta: EventMetaData,
  ) { }
}
