import { ICommand } from '@nestjs/cqrs';
import { IPerson } from '../../models/person.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeletePersonsByQueryCommand implements ICommand {
  constructor(
    public readonly query,
    public readonly meta: EventMetaData,
  ) {}
}
