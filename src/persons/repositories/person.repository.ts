import { BreadRepository } from '../../shared/bread.respository';
import { Person } from '../models/person.model';
import { InjectModel } from 'nestjs-typegoose';
import { NotFoundException, InternalServerErrorException } from '@nestjs/common';
import {
  EmployerRelation,
  DealRelation,
  ConsentState,
} from '@daypaio/domain-events/persons';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { Subject } from 'rxjs';
import { DeleteStreamCommand } from '../../shared/commands/impl/delete-stream.command';
import { CommandBus } from '@nestjs/cqrs';

export class PersonRepository extends BreadRepository<Person> {

  public importResult = {
    success$: new Subject(),
    errors$: new Subject(),
  };

  constructor(
    @InjectModel(Person) personModel: Person,
    private commandBus: CommandBus,
  ) {
    super(personModel);
  }

  async relateToOrganization(
    _personID: string,
    _organizationID: string,
    meta: EventMetaData,
  ) {
    const relation: EmployerRelation = {
      _organizationID,
      isContactPerson: false,
      role: null,
    };
    await this.model.findOneAndUpdate(
      {
        _tenantID: meta._tenantID,
        _id: _personID,
        'employers._organizationID': { $ne: _organizationID },
      },
      {
        $push: { employers: { $each: [relation], $position: 0 } },
      },
    );
  }

  async unrelateFromOrganization(
    _personID: string,
    _organizationID: string,
    meta: EventMetaData,
  ) {
    await this.model.findOneAndUpdate(
      {
        _tenantID: meta._tenantID,
        _id: _personID,
      },
      {
        $pull: { employers: { _organizationID } },
      },
    );
  }

  async unrelateAllFromOrganization(
    _organizationID: string,
    meta: EventMetaData,
  ) {
    await this.model.updateMany(
      {
        _tenantID: meta._tenantID,
      },
      {
        $pull: { employers: { _organizationID } },
      },
    );
  }

  async relateToDeal(_personID: string, _dealID: string, meta: EventMetaData) {
    const relation: DealRelation = {
      _dealID,
      isContactPerson: false,
    };
    await this.model.findOneAndUpdate(
      {
        _tenantID: meta._tenantID,
        _id: _personID,
        'deals._dealID': { $ne: _dealID },
      },
      {
        $push: { deals: { $each: [relation], $position: 0 } },
      },
    );
  }

  async unrelateFromDeal(
    _personID: string,
    _dealID: string,
    meta: EventMetaData,
  ) {
    await this.model.findOneAndUpdate(
      {
        _tenantID: meta._tenantID,
        _id: _personID,
      },
      {
        $pull: { deals: { _dealID } },
      },
    );
  }

  async assignToDealAsContactPerson(
    _personID: string,
    _dealID: string,
    meta: EventMetaData,
  ) {
    const relation: DealRelation = {
      _dealID,
      isContactPerson: true,
    };

    await this.unrelateFromDeal(_personID, _dealID, meta);

    await this.model.findOneAndUpdate(
      {
        _tenantID: meta._tenantID,
        _id: _personID,
      },
      {
        $push: { deals: { $each: [relation], $position: 0 } },
      },
    );

  }

  async removeFromDealAsContactPerson(
    _personID: string,
    _dealID: string,
    meta: EventMetaData,
  ) {
    await this.unrelateFromDeal(_personID, _dealID, meta);
  }

  async browseByDeals(_dealID: string, meta: EventMetaData) {
    return this.model.find({
      _tenantID: meta._tenantID,
      'deals._dealID': _dealID,
    }).lean();
  }

  async find(query, meta: EventMetaData) {
    return this.model.find({
      _tenantID: meta._tenantID,
      ...query,
    }).lean();
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE');
    data._tenantID = meta._tenantID;
    console.table(data);
    try {
      const result = await this.model.create(data);
      if (result.importID) this.importResult.success$.next(result);
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      if (data.importID) this.importResult.errors$.next({ error, importID: data.importID });
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async changeEmployerRelationRole(
    _personID:  string,
    _organizationID: string,
    role: string, meta: EventMetaData,
  ) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID,
          _id: _personID,
          'employers._organizationID': _organizationID,
        },
        { $set: { 'employers.$.role': role } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'changeEmployeeRelationRole',
        _personID,
        _organizationID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async deleteByQuery(query, meta: EventMetaData): Promise<void> {
    this.logger.verbose('DELETE BY QUERY');
    console.table({ query });
    try {
      let ids = await this.model
      .find({ _tenantID: meta._tenantID, ...query })
      .select('_id').lean().exec();

      if (!ids.length) {
        throw new NotFoundException();
      }

      ids = ids.map(item => item._id);
      const result = await this.model.deleteMany({
        _tenantID: meta._tenantID,
        _id: { $in: ids },
      });
      ids.forEach(id => this.commandBus.execute(new DeleteStreamCommand(`persons-${id}`)));
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        return;
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${this.model.modelName} was found with query: ${query} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'deleteByQuery');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async grantConsent(personsID: string, email: string, ip: string, meta: EventMetaData) {
    try {
      await this.model.findOneAndUpdate(
        { _tenantID: meta._tenantID, _id: personsID, 'consents.email': email },
        {
          $set: {
            'consents.$.state': ConsentState.Granted,
            'consents.$.grantedTimestamp': meta.timestamp,
            'consents.$.ip': ip,
          },
        },
      );
    } catch (error) {
      throw error;
    }
  }

  async updateLastActivityTime(id: string, meta: EventMetaData) {
    const query = { _id: id };
    const update = {
      $set: {
        lastActivity: meta.timestamp,
      },
    };

    try {
      await this.model.findOneAndUpdate(query, update);
    } catch (error) {
      this.logger.verbose('Update latest activity failed');
      throw error;
    }
  }

  async edit(id: string, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    console.table({ data, _id: id });
    try {
      let result;
      if (data.customFields) {
        const key = Object.keys(data.customFields)[0];
        const value: any = Object.values(data.customFields)[0];
        const property = `customFields.${key}`;
        delete data.customFields;
        data[property] = value;
        if (!value || value?.length === 0) {
          // remove custom field from object
          result = await this.model.updateOne(
            { _tenantID: meta._tenantID, _id: id },
            { $unset: { [property]: 1 } },
          );
        }
      }

      if (!result) {
        result = await this.model.updateOne(
          { _tenantID: meta._tenantID, _id: id },
          { $set: data },
        );
      }

      if (result === undefined || result === null) {
        throw new InternalServerErrorException(
          `Failed editing model for ${this.model.modelName}: result: ${result}`,
        );
      }

      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, data);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

}
