import { prop, index } from '@typegoose/typegoose';
import { IPerson } from './person.interface';
import {
  IPersonAddress,
  Gender,
  EmployerRelation,
  DealRelation,
  IConsent,
  ConsentState,
} from '@daypaio/domain-events/persons';
import { v4 } from 'uuid';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { IIntegrations } from '@daypaio/domain-events/shared/interfaces/integrations.interface';

export class PersonAddress implements IPersonAddress {
  @prop({ default: v4() })
  _id!: string;

  @ApiPropertyOptional({
    description: 'The street name of the address (string:alpha)',
  })
  @prop()
  street: string;

  @ApiPropertyOptional({
    description: 'The house number (two digits and char)',
  })
  @prop()
  number: string;

  @ApiPropertyOptional({
    description: 'The post code consisting of 5 digits',
  })
  @prop()
  postcode: string;

  @ApiPropertyOptional({
    description: 'The city name (string:alpha)',
  })
  @prop()
  city: string;

  @ApiPropertyOptional({
    description: 'The name of the country (string:alpha)',
  })
  @prop()
  country: string;
}

class EmployerRelationModel implements EmployerRelation {
  @prop()
  isContactPerson: boolean;

  @prop()
  _organizationID: string;

  @prop()
  role?: string;
}

class Consent implements IConsent {
  @prop()
  state: ConsentState;

  @prop()
  email: string;

  @prop()
  ip: string;

  @prop()
  isMarketingConsent: boolean;

  @prop()
  requestedTimestamp: number;

  @prop()
  grantedTimestamp: number;

  @prop()
  revokedTimestamp: number;
}

class DealRelationModel implements DealRelation {
  @prop()
  isContactPerson: boolean;

  @prop()
  _dealID: string;
}

@index({ _tenantID: 1 })
@index({ _tenantID: 1, _id: 1 })
export class Person implements IPerson {
  @ApiProperty({
    description: 'the related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the person',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiPropertyOptional({
    description: 'The email of the person',
  })
  @prop()
  email: string;

  @ApiPropertyOptional({
    description: 'The first name of the person',
  })
  @prop()
  firstName: string;

  @ApiProperty({
    description: 'The last name of the person',
  })
  @prop()
  lastName: string;

  @ApiProperty({
    description: 'The gender of the person',
  })
  @prop()
  gender: Gender;

  @ApiProperty({
    description: 'The prefix of the person`s name',
  })
  @prop()
  namePrefix: string;

  @ApiProperty({
    description: 'The suffix of the person`s name',
  })
  @prop()
  nameSuffix: string;

  @ApiPropertyOptional({
    description: 'The phone number of the person',
  })
  @prop()
  phoneNumber: string;

  @ApiPropertyOptional({
    description: 'The mobile phone number of the person',
  })
  @prop()
  mobileNumber: string;

  @ApiPropertyOptional({
    description: 'The private phone number of the person',
  })
  @prop()
  privateNumber: string;

  @ApiPropertyOptional({
    description: 'The office phone number of the person',
  })
  @prop()
  officeNumber: string;

  @ApiPropertyOptional({
    description: 'The home phone number of the person',
  })
  @prop()
  homeNumber: string;

  @ApiPropertyOptional({
    description: 'The fax number of the person',
  })
  @prop()
  faxNumber: string;

  @ApiPropertyOptional({
    description: 'The website of the person',
  })
  @prop()
  website: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  linkedin: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  xing: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  facebook: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  twitter: string;

  @ApiPropertyOptional({
    description: 'The social media links of the person',
  })
  @prop()
  instagram: string;

  @ApiPropertyOptional({
    description: 'The birthday of the person',
  })
  @prop()
  birthday: string;

  @ApiPropertyOptional({
    description: 'The created at timestamp of the person',
  })

  @prop()
  lastActivity: number;

  @prop()
  createdAt: Date;

  @ApiPropertyOptional({
    description: 'Import id',
  })
  @prop()
  importID: string;

  @ApiPropertyOptional({
    description: 'The address of the person',
  })
  @prop()
  address: PersonAddress;

  @ApiPropertyOptional({
    description: 'The organizations the person is working for',
  })
  @prop({ type: EmployerRelationModel, _id: false })
  employers: EmployerRelationModel[];

  @ApiPropertyOptional({
    description: 'The deal relations of the person',
  })
  @prop({ type: DealRelationModel, _id: false })
  deals: DealRelationModel[];

  @ApiPropertyOptional({
    description: 'The doi consents of the person',
  })
  @prop({ type: Consent, _id: false })
  consents: Consent[];

  @ApiPropertyOptional({
    description: 'Custom fields for each person',
  })
  @prop({ _id: false })
  customFields?: EntityCustomFields;

  @ApiPropertyOptional({
    description: 'Integrations info of the person',
  })
  @prop({ _id: false })
  integrations?: IIntegrations;

  constructor(data?: Partial<Person>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
