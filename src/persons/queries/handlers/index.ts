import { ReadPersonHandler } from './read-person.query-handler';
import { BrowsePersonHandler } from './browse-person.query-handler';
import { FindPersonHandler } from './find-person.query-handler';

export const PersonQueryHandlers = [
  ReadPersonHandler,
  BrowsePersonHandler,
  FindPersonHandler,
];
