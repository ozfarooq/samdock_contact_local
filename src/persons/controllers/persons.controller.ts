import {
  Controller,
  Get,
  Param,
  Logger,
  Patch,
  Body,
  Post,
  Delete,
  ValidationPipe,
} from '@nestjs/common';
import { PersonsService } from '../services/persons.service';
import { Person } from '../models/person.model';
import { BreadController } from '../../shared/controllers/bread.controller';
import {
  ApiTags,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiBody,
} from '@nestjs/swagger';
import {
  GetUser,
  LoggedInUser,
} from '../../shared/decorators/get-user.decorator';
import { IPerson } from '../models/person.interface';
import { PersonDTO } from '../dtos/person.dto';

@ApiTags('persons')
@ApiBearerAuth()
@Controller('persons')
export class PersonsController implements BreadController<IPerson> {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: PersonsService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse Persons',
    description: 'Get all persons',
  })
  @ApiOkResponse({
    description: 'The persons have successfully been queried',
    type: Person,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<IPerson[]> {
    this.logger.log(`BROWSE user:${user._id}`);
    return await this.service.browse(user.generateMeta());
  }

  @Delete()
  @ApiOperation({
    summary: 'Bulk Delete Persons',
    description: 'Delete a list of persons',
  })
  @ApiOkResponse({
    description: 'The persons deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async bulkDelete(
    @Body('ids') ids: string[],
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`BULK DELETE id:${ids} user:${user._id}`);
    return await this.service.deleteByQuery({ _id: { $in: ids } }, user.generateMeta());
  }

  @Get('/:id')
  @ApiOperation({
    summary: 'Read Person',
    description: 'Get a single person',
  })
  @ApiOkResponse({
    description: 'The person has successfully been found',
    type: Person,
  })
  @ApiNotFoundResponse({ description: 'The person was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<IPerson> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return await this.service.read(id, user.generateMeta());
  }

  @Patch('/:id')
  @ApiBody({
    type: PersonDTO,
  })
  @ApiOperation({
    summary: 'Edit Persons',
    description: 'Edit single person',
  })
  @ApiOkResponse({
    description: 'The person editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body(new ValidationPipe({ skipUndefinedProperties: true }))
    person: Partial<PersonDTO>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    await this.service.edit(id, person, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Person',
    description: 'Create a single person',
  })
  @ApiOkResponse({
    description: 'The person adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() person: PersonDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${person._id} user:${user._id}`);
    await this.service.add(person, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete Person',
    description: 'Delete a single person',
  })
  @ApiOkResponse({
    description: 'The person deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    await this.service.delete(id, user.generateMeta());
  }
}
