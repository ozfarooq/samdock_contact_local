import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { plainToClassFromExist, Type } from 'class-transformer';
import {
  IsString,
  IsEmail,
  IsAlpha,
  IsOptional,
  ValidateNested,
  MinLength,
  MaxLength,
  IsISO8601,
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
  isEmpty,
  IsBoolean,
  IsDefined,
  IsArray,
  IsEmpty,
} from 'class-validator';
import { IPersonAddress, IPerson, IConsent, Gender } from '../events/impl';

export function isNotEmptyOr(property: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      propertyName,
      name: 'isNotEmptyOr',
      target: object.constructor,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const relatedPropertyNames = args.constraints[0].split(',');
          const relatedValues = relatedPropertyNames.map(key => (args.object as any)[key]);
          if (relatedValues.some(item => item && typeof item === 'string')) return true;
          return !isEmpty(value) && typeof value === 'string';
        },
      },
    });
  };
}

export class PersonAddressDTO implements IPersonAddress {
  @IsOptional()
  @IsString()
  street: string;

  @IsOptional()
  @IsString()
  number: string;

  @IsOptional()
  @IsString()
  postcode: string;

  @IsOptional()
  @IsString()
  city: string;

  @IsOptional()
  @IsString()
  @IsAlpha()
  @MinLength(2)
  @MaxLength(2)
  country: string;
}

export class PersonEmployerDTO {
  @IsBoolean()
  isContactPerson: boolean;

  @IsString()
  @IsDefined()
  _organizationID: string;

  @IsOptional()
  @IsString()
  role?: string;
}
export interface EntityCustomFieldErrors {
  [key: string]: string;
}

export class PersonDTO implements IPerson {

  constructor(data?: any) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsOptional()
  @IsEmail()
  @isNotEmptyOr('firstName,lastName', {
    message: 'Email or firstName or lastName should not be empty',
  })
  email: string;

  @isNotEmptyOr('email,lastName', {
    message: 'Email or firstName or lastName should not be empty',
  })
  firstName: string;

  @isNotEmptyOr('email,firstName', {
    message: 'Email or firstName or lastName should not be empty',
  })
  lastName: string;

  @IsString()
  @IsOptional()
  gender: Gender;

  @IsString()
  @IsOptional()
  namePrefix: string;

  @IsString()
  @IsOptional()
  nameSuffix: string;

  @IsString()
  @IsOptional()
  mobileNumber: string;

  @IsString()
  @IsOptional()
  phoneNumber: string;

  @IsString()
  @IsOptional()
  officeNumber: string;

  @IsString()
  @IsOptional()
  homeNumber: string;

  @IsString()
  @IsOptional()
  privateNumber: string;

  @IsString()
  @IsOptional()
  faxNumber: string;

  @IsString()
  @IsOptional()
  website: string;

  @IsString()
  @IsOptional()
  linkedin: string;

  @IsString()
  @IsOptional()
  xing: string;

  @IsString()
  @IsOptional()
  twitter: string;

  @IsString()
  @IsOptional()
  facebook: string;

  @IsString()
  @IsOptional()
  instagram: string;

  @IsISO8601()
  @IsOptional()
  birthday: string;

  @ValidateNested()
  @IsOptional()
  address: PersonAddressDTO;

  @IsString()
  @IsOptional()
  importID: string;

  @IsOptional()
  consents: IConsent[];

  @IsOptional()
  customFields?: EntityCustomFields;

  @IsEmpty()
  @IsOptional()
  customFieldsErrors?: EntityCustomFieldErrors;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PersonEmployerDTO)
  employers?: PersonEmployerDTO[];
}
