import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ActivityAddedEvent } from '@daypaio/domain-events/activities';
import { PersonRepository } from './../../repositories/person.repository';
import { OrganizationRepository } from './../../../organizations/repositories/organization.repository';

@EventsHandler(ActivityAddedEvent)
export class ActivityAddedEventHandler
  implements IEventHandler<ActivityAddedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private repositoryPerson: PersonRepository,
    private repositoryOrganization: OrganizationRepository,
  ) { }
  async handle(event: ActivityAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { activity, meta } = event;
    try {
      if (activity._personIDs?.length) {
        await Promise.all(
        activity._personIDs.map(id =>  this.repositoryPerson.updateLastActivityTime(id, meta)));
      }
      if (activity._organizationIDs?.length) {
        await Promise.all(
        activity._organizationIDs.map(id =>
          this.repositoryOrganization.updateLastActivityTime(id, meta),
        ));
      }
    } catch (error) {
      this.logger.error('Failed to update persons or organization');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
