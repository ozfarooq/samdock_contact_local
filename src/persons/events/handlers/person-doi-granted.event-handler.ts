import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { PersonDOIGrantedEvent, IConsent } from '../impl';

@EventsHandler(PersonDOIGrantedEvent)
export class PersonDOIGrantedEventHandler implements IEventHandler<PersonDOIGrantedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: PersonRepository) {}
  async handle(event: PersonDOIGrantedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, email, ip, meta } = event;

    try {
      await this.repository.grantConsent(_id, email, ip, meta);
    } catch (error) {
      this.logger.error(error.message);
      this.logger.debug(error.stack);
    }
  }
}
