import { OrganizationRepository } from './../../../organizations/repositories/organization.repository';
import { Person } from '../../models/person.model';
import { EventBus, CqrsModule } from '@nestjs/cqrs';
import { AddPersonEventHandler } from './add-person.event-handler';
import { EditPersonEventHandler } from './edit-person.event-handler';
import { DeletePersonEventHandler } from './delete-person.event-handler';
import { TestingModule, Test } from '@nestjs/testing';
import { PersonEventHandlers } from '.';
import { PersonRepository } from '../../repositories/person.repository';
import {
  PersonAddedEvent,
  PersonEditedEvent,
  PersonDeletedEvent,
} from '../impl';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PersonsService } from '../../../persons/services/persons.service';
import { OrganizationsService } from '../../../organizations/services/organizations.service';
import { InboundSourcesService } from '../../../inbound-sources/services/inbound-sources.service';
import { DraftsService } from '../../../drafts/services/drafts.service';
import { CustomFieldsService } from '../../../custom-fields/services/custom-fields.service';
import { ContactViewsService } from '../../../contact-views/services/contact-views.service';

const mockPersonRepository = () => ({
  add: jest.fn(),
  edit: jest.fn(),
  delete: jest.fn(),
});
const mockOrganizationRepository = () => ({
  add: jest.fn(),
  edit: jest.fn(),
  delete: jest.fn(),
});
const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockPersons(): Person[] {
  const firstPerson = new Person({
    _id: 'org1',
    firstName: 'T',
    lastName: 'Alam',
    email: 't@example.com',
    phoneNumber: '12345',
  });

  const secondPerson = new Person({
    _id: 'org2',
    firstName: 'J',
    lastName: 'Grosch',
    email: 'j@example.com',
    phoneNumber: '12345',
  });

  const arrayOfPersons: Person[] = [];
  arrayOfPersons.push(firstPerson);
  arrayOfPersons.push(secondPerson);

  return arrayOfPersons;
}

describe('PersonEventHandlers', () => {
  const mockPersons: Person[] = createMockPersons();
  let eventBus: EventBus;
  let repo;

  let addPersonEventHandler: AddPersonEventHandler;
  let editPersonEventHandler: EditPersonEventHandler;
  let deletePersonEventHandler: DeletePersonEventHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...PersonEventHandlers,
        PersonsService,
        OrganizationsService,
        InboundSourcesService,
        DraftsService,
        CustomFieldsService,
        ContactViewsService,
        {
          provide: PersonRepository,
          useFactory: mockPersonRepository,
        },
        {
          provide: OrganizationRepository,
          useFactory: mockOrganizationRepository,
        },
      ],
    }).compile();
    eventBus = module.get<EventBus>(EventBus);
    repo = module.get<PersonRepository>(PersonRepository);

    addPersonEventHandler = module.get<AddPersonEventHandler>(
      AddPersonEventHandler,
    );
    editPersonEventHandler = module.get<EditPersonEventHandler>(
      EditPersonEventHandler,
    );
    deletePersonEventHandler = module.get<DeletePersonEventHandler>(
      DeletePersonEventHandler,
    );
  });

  describe(' for AddPersonEventHandler', () => {
    it('should successfully return an array of persons', async () => {
      repo.add.mockResolvedValue(null);
      eventBus.register([AddPersonEventHandler]);
      expect(repo.add).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonAddedEvent(mockPersons[0]._id, mockPersons[0], mockMeta),
      );
      expect(repo.add).toHaveBeenCalledWith(mockPersons[0], mockMeta);
    });

    it('should log the error if repo.add() fails', async () => {
      const logger = (addPersonEventHandler as any).logger;
      repo.add.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([AddPersonEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.add).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonAddedEvent(mockPersons[0]._id, mockPersons[0], mockMeta),
      );
      expect(repo.add).toHaveBeenCalledWith(mockPersons[0], mockMeta);
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe(' for EditOrganizationEventHandler', () => {
    it('should successfully repo.updatedById() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([EditPersonEventHandler]);
      expect(repo.edit).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonEditedEvent(mockPersons[0]._id, mockPersons[0], mockMeta),
      );
      expect(repo.edit).toHaveBeenCalledWith(
        mockPersons[0]._id,
        mockPersons[0],
        mockMeta,
      );
    });

    it('should log the error if repo.edit() fails', async () => {
      const logger = (editPersonEventHandler as any).logger;
      repo.edit.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([EditPersonEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.edit).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonEditedEvent(mockPersons[0]._id, mockPersons[0], mockMeta),
      );
      expect(repo.edit).toHaveBeenCalledWith(
        mockPersons[0]._id,
        mockPersons[0],
        mockMeta,
      );
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe(' for DeleteOrganizationEventHandler', () => {
    it('should successfully repo.delete() without any errors', async () => {
      repo.edit.mockResolvedValue(null);
      eventBus.register([DeletePersonEventHandler]);
      expect(repo.delete).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonDeletedEvent(mockPersons[0]._id, mockMeta),
      );
      expect(repo.delete).toHaveBeenCalledWith(mockPersons[0]._id, mockMeta);
    });

    it('should log the error if repo.delete() fails', async () => {
      const logger = (deletePersonEventHandler as any).logger;
      repo.delete.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      eventBus.register([DeletePersonEventHandler]);
      jest.spyOn(logger, 'error');
      expect(repo.delete).not.toHaveBeenCalled();
      await eventBus.publish(
        new PersonDeletedEvent(mockPersons[0]._id, mockMeta),
      );
      expect(repo.delete).toHaveBeenCalledWith(mockPersons[0]._id, mockMeta);
      expect(logger.error).toHaveBeenCalledTimes(1);
    });
  });
});
