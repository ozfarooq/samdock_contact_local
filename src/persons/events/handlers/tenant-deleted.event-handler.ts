import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TenantDeletedEvent } from '@daypaio/domain-events/tenants';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { PersonsService } from '../../../persons/services/persons.service';
import { OrganizationsService } from '../../../organizations/services/organizations.service';
import { DraftsService } from '../../../drafts/services/drafts.service';
import { InboundSourcesService } from '../../../inbound-sources/services/inbound-sources.service';
import { CustomFieldsService } from '../../../custom-fields/services/custom-fields.service';
import { ContactViewsService } from '../../../contact-views/services/contact-views.service';

@EventsHandler(TenantDeletedEvent)
export class TenantDeletedEventHandler
  implements IEventHandler<TenantDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private personsService: PersonsService,
    private organizationsService: OrganizationsService,
    private draftsService: DraftsService,
    private inboundSourcesService: InboundSourcesService,
    private customFieldsService: CustomFieldsService,
    private contactViewsService: ContactViewsService,
  ) { }
  async handle(event: TenantDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id } = event;
    try {
      const meta = new EventMetaData(_id, null);
      await Promise.all([
        this.personsService.deleteAll(meta),
        this.organizationsService.deleteAll(meta),
        this.draftsService.deleteAll(meta),
        this.inboundSourcesService.deleteAll(meta),
        this.customFieldsService.deleteAll(meta),
        this.contactViewsService.deleteAll(meta),
      ]);
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
