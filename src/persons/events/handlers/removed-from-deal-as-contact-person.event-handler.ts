import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { RemovedFromDealAsContactPersonEvent, RemovedFromLeadAsContactPersonEvent } from '../impl';

@EventsHandler(RemovedFromDealAsContactPersonEvent, RemovedFromLeadAsContactPersonEvent)
export class RemovedFromDealAsContactPersonEventHandler implements
  IEventHandler<RemovedFromDealAsContactPersonEvent | RemovedFromLeadAsContactPersonEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: PersonRepository) {}
  async handle(event: RemovedFromDealAsContactPersonEvent | RemovedFromLeadAsContactPersonEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`);
    const { _personID, _dealID, meta } = event;
    try {
      const result = await this.repository.removeFromDealAsContactPerson(
          _personID,
          _dealID,
          meta,
        );
      return result;
    } catch (error) {
      this.logger.error(`Failed to update person of id: ${_personID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
