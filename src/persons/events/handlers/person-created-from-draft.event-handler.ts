import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { PersonRepository } from '../../repositories/person.repository';
import { Gender, PersonCreatedFromDraftEvent } from '../impl';
import { Person, PersonAddress } from '../../models/person.model';

@EventsHandler(PersonCreatedFromDraftEvent)
export class PersonCreatedFromDraftEventHandler implements
IEventHandler<PersonCreatedFromDraftEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: PersonRepository) {}
  async handle(event: PersonCreatedFromDraftEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    const person = new Person({ _id: data._id, lastName : data.lastName });
    person.email = data.email;
    person.firstName = data.firstName;
    person.birthday = data.birthday as string;
    person.gender = data.gender as Gender;
    person.namePrefix = data.namePrefix;
    person.nameSuffix = data.nameSuffix;
    person.phoneNumber = data.phoneNumber;
    person.mobileNumber = data.mobileNumber;
    person.officeNumber = data.officeNumber;
    person.privateNumber = data.privateNumber;
    person.homeNumber = data.homeNumber;
    person.faxNumber = data.faxNumber;
    person.website = data.website;
    person.instagram = data.instagram;
    person.linkedin = data.linkedin;
    person.facebook = data.facebook;
    person.twitter = data.twitter;
    person.xing = data.xing;

    person.createdAt = new Date(meta.timestamp);
    if (
      !!data.street ||
      !!data.number ||
      !!data.postcode ||
      !!data.city
    ) {
      person.address = new PersonAddress();
      person.address.street = data.street;
      person.address.number = data.number;
      person.address.postcode = data.postcode;
      person.address.city = data.city;
    }

    if (data.consent) {
      person.consents = [data.consent];
    }

    try {
      await this.repository.add(person, meta);
    } catch (error) {
      this.logger.error(`Failed to create person of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
