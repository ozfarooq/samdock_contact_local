import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { Person } from '../persons/models/person.model';
import { Organization } from '../organizations/models/organization.model';

export class SyncContactsIndexes1625659351378 implements MigrationInterface {

  private persons: mongoose.Model<InstanceType<any>>;
  private organizations: mongoose.Model<InstanceType<any>>;

  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.persons = getModelForClass(Person);
    this.organizations = getModelForClass(Organization);
    this.logger = new Logger(`${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    try {
      await this.persons.syncIndexes();

      await this.organizations.syncIndexes();

      this.logger.log('success up');
    } catch (_error) {}
  }

  public async down(): Promise<void> {}

}
