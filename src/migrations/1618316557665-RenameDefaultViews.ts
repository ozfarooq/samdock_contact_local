import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { ContactView } from '../contact-views/models/contact-view.model';

export class RenameDefaultViews1618316557665 implements MigrationInterface {
  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.model = getModelForClass(ContactView);
    this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    await this.model.updateMany(
      { name: 'Persons' }, { $set: { name: 'Personen' } },
    );
    await this.model.updateMany(
      { name: 'Organizations' }, { $set: { name: 'Unternehmen' } },
    );
  }

  public async down(): Promise<void> {
  }

}
