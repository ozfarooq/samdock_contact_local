import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import * as esClient from 'node-eventstore-client';
import { v4 } from 'uuid';
import {
  UserAddedEvent,
  UserDeletedEvent,
} from '@daypaio/domain-events/users';
import { ContactView } from '../contact-views/models/contact-view.model';
import { ContactViewEntityEnum, DEFAULT_VIEW } from '../contact-views/dtos/contact-view.dto';

export class PopulateDefaultViewsToExistingUsers1618185206637 implements MigrationInterface {
  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;
  esConnection: esClient.EventStoreNodeConnection;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.model = getModelForClass(ContactView);
    this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    const connectionSettings = {
      defaultUserCredentials: {
        username: process.env.EVENT_STORE_USERNAME,
        password: process.env.EVENT_STORE_PASSWORD,
      },
    };

    const tcpEndpoint = {
      host: process.env.EVENT_STORE_TCP_HOST || 'localhost',
      port: parseInt(process.env.EVENT_STORE_TCP_PORT, 10) || 1113,
    };

    this.esConnection = esClient.createConnection(connectionSettings, tcpEndpoint);

    await this.esConnection.connect();

    const contactViews: ContactView[] = await this.model.find().lean();

    const streams = await Promise.all([
      this.readEvents('$et-UserAddedEvent'),
      this.readEvents('$et-UserDeletedEvent'),
    ]);

    const events = [...streams[0], ...streams[1]].sort((a, b) => {
      return a.event?.created?.getTime() - b.event?.created?.getTime();
    });

    this.logger.log(`${events.length} deleting events found`);

    const contactViewsToAdd = [];
    const usersDeleted = [];

    const eventHandlers = {
      UserAddedEvent,
      UserDeletedEvent,
    };

    for (const event of events) {
      if (!event.originalEvent?.eventStreamId || !event.originalEvent?.data) {
        this.logger.log(event);
        continue;
      }

      if (
        (event.link !== null && !event.isResolved) ||
        !event.event ||
        !event.event.isJson
      ) {
        this.logger.error('Received event that could not be resolved!');
        continue;
      }
      const handler = eventHandlers[event.event.eventType];
      if (!handler) {
        this.logger.error('Received event that could not be handled!');
        continue;
      }
      const data = Object.values(JSON.parse(event.event.data.toString()));

      const parsed:
        | UserAddedEvent
        | UserDeletedEvent = new eventHandlers[event.event.eventType](...data);

      if (!parsed.meta?._tenantID) {
        continue;
      }

      if (parsed instanceof UserAddedEvent && parsed.meta._tenantID) {
        if (!this.isAlreadyAdded(parsed._id, contactViews)) {
          contactViewsToAdd.push(
            this.createDefaultViewFor(
              ContactViewEntityEnum.Person,
              parsed._id,
              parsed.meta._tenantID,
            ),
          );
          contactViewsToAdd.push(
            this.createDefaultViewFor(
              ContactViewEntityEnum.Organization,
              parsed._id,
              parsed.meta._tenantID,
            ),
          );
        }
      } else if (
        parsed instanceof UserDeletedEvent
      ) {
        usersDeleted.push(parsed._id);
      }
    }

    await this.model.insertMany(contactViewsToAdd);
    await this.model.deleteMany({ createdBy: { $in: usersDeleted } });
  }

  public async down(): Promise<void> {
  }

  readEvents(streamName: string): Promise<esClient.ResolvedEvent[]> {
    const events: esClient.ResolvedEvent[] = [];

    return new Promise((resolve) => {
      this.esConnection.subscribeToStreamFrom(
        streamName,
        null,
        true,
        (_sub, event) => {
          events.push(event);
        },
        () => resolve(events),
      );
    });
  }

  isAlreadyAdded(userID: string, contactViews: ContactView[]) {
    return contactViews.some((view) => {
      return view.createdBy === userID &&
        (view.name === 'Persons' || view.name === 'Organizations');
    });
  }

  createDefaultViewFor(entity: ContactViewEntityEnum, _id: string, _tenantID: string) {
    const view = { ...DEFAULT_VIEW };
    view._id = v4();
    view.name = `${entity[0].toUpperCase()}${entity.substring(1)}s`;
    view.entity = entity;
    view.isShared = false;
    view.createdAt = Date.now();
    view.createdBy = _id;
    view._tenantID = _tenantID;
    return view;
  }
}
