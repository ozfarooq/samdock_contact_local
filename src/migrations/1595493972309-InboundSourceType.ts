import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { InboundSource } from '../inbound-sources/models/inbound-source.model';
import { Logger } from '@nestjs/common';
import { InboundSourceType } from '@daypaio/domain-events/inbound-sources';
import { getModelForClass } from '@typegoose/typegoose';

export class InboundSourceType1595493972309 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  constructor() {
    setupDotenv();
    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.model = getModelForClass(InboundSource);
    this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);
  }

  public async up(): Promise<void> {
    await this.model.updateMany(
      { type: { $exists: false } },
      { $set: { type: InboundSourceType.Api } },
    ).exec();
    this.logger.log('success up');
  }

  public async down(): Promise<void> {
    await this.model.updateMany({}, { $unset: { type: '' } }).exec();
    this.logger.log('success down');
  }

}
