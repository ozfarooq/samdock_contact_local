import { BreadRepository } from '../../shared/bread.respository';
import { ContactDraft } from '../models/draft.model';
import { InjectModel } from 'nestjs-typegoose';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ConsentState } from '@daypaio/domain-events/persons';
import { ReturnModelType } from '@typegoose/typegoose';

export class DraftsRepository extends BreadRepository<ContactDraft> {
  constructor(@InjectModel(ContactDraft) draftsModel: ReturnModelType<typeof ContactDraft>) {
    super(draftsModel);
  }

  async grantConsent(personsID: string, email: string, ip: string, meta: EventMetaData) {
    try {
      await this.model.findOneAndUpdate(
        { _tenantID: meta._tenantID, _id: personsID },
        {
          $set: {
            'consent.state': ConsentState.Granted,
            'consent.grantedTimestamp': meta.timestamp,
            'consent.ip': ip,
          },
        },
      );
    } catch (error) {
      throw error;
    }
  }
}
