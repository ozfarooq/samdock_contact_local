import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class BrowseDraftQuery implements IQuery {
  constructor(public meta: EventMetaData) {}
}
