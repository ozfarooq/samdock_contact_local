import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ReadDraftQuery } from '../impl/read-draft.query';
import { ContactDraft } from '../../../drafts/models/draft.model';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../../drafts/repositories/drafts.repository';

@QueryHandler(ReadDraftQuery)
export class ReadDraftQueryHandler implements IQueryHandler<ReadDraftQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repository: DraftsRepository) {}

  async execute(query: ReadDraftQuery): Promise<ContactDraft> {
    this.logger.log(`Reading Contact Drafts: ${query.id}`);
    try {
      const { id, meta } = query;
      const result = await this.repository.read(id, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to read contact draft with id: ${query.id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
