import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DraftDeletedEvent } from '@daypaio/domain-events/contact-drafts';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../repositories/drafts.repository';

@EventsHandler(DraftDeletedEvent)
export class ContactDraftDeletedEventHandler implements IEventHandler<DraftDeletedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repo: DraftsRepository) {}
  async handle(event: DraftDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.repo.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete draft of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
