import { ContactDraftAddedEventHandler } from './contact-draft-added.event-handler';
import { ContactDraftDeletedEventHandler } from './contact-draft-deleted.event-handler';
import { ContactDraftVerifiedEventHandler } from './contact-draft-verified.event-handler';
import { DraftDOIGrantedEventHandler } from './draft-doi-granted.event-handler';
import { DraftDOIMailSentEventHandler } from './draft-doi-mail-sent.event-handler';

export const ContactDraftsEventHandlers = [
  ContactDraftAddedEventHandler,
  ContactDraftVerifiedEventHandler,
  ContactDraftDeletedEventHandler,
  DraftDOIGrantedEventHandler,
  DraftDOIMailSentEventHandler,
];
