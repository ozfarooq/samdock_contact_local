import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftDOIMailSentEvent } from '@daypaio/domain-events/contact-drafts';
import { MailerService } from '@nestjs-modules/mailer';
import { DraftsRepository } from '../../../drafts/repositories/drafts.repository';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IConsent, ConsentState } from '@daypaio/domain-events/persons';

@EventsHandler(DraftDOIMailSentEvent)
export class DraftDOIMailSentEventHandler implements IEventHandler<DraftDOIMailSentEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private mailerService: MailerService, private draftRepository: DraftsRepository) {}

  async handle(event: DraftDOIMailSentEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, mail, doiMailSettings, meta } = event;
    try {
      await this.mailerService.sendMail({
        to: mail,
        from: `"${doiMailSettings.mail.companyName}" <noreply@samdock.app>`,
        subject: doiMailSettings.mail.subject,
        template: 'doi-email',
        context: {
          ...doiMailSettings.mail,
          logoURL: doiMailSettings.mail.logoURL || null,
          grantUrl: `https://${process.env.APP_URL}/api/contacts/doi/${_id}/grant?email=${mail}&redirectUrl=${doiMailSettings.redirectUrl}&verify=${doiMailSettings.autoVerify}&_tenantID=${meta._tenantID}`,
        },
      });

      this.draftRepository.edit(_id, { consent: this.generateConsent(mail, meta) }, meta);
    } catch (error) {
      this.logger.error(error.message);
      this.logger.debug(error.stack);
    }
  }

  generateConsent(email: string, meta: EventMetaData): IConsent {
    return {
      email,
      ip: null,
      state: ConsentState.Requested,
      grantedTimestamp: null,
      requestedTimestamp: meta.timestamp,
      revokedTimestamp: null,
      isMarketingConsent: true,
    };
  }
}
