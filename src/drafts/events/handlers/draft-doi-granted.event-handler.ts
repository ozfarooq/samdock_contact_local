import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftsRepository } from '../../repositories/drafts.repository';
import { DraftDOIGrantedEvent } from '@daypaio/domain-events/contact-drafts';

@EventsHandler(DraftDOIGrantedEvent)
export class DraftDOIGrantedEventHandler implements IEventHandler<DraftDOIGrantedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: DraftsRepository) {}
  async handle(event: DraftDOIGrantedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, email, ip, meta } = event;

    try {
      this.repository.grantConsent(_id, email, ip, meta);
    } catch (error) {
      this.logger.error(error.message);
      this.logger.debug(error.stack);
    }
  }

}
