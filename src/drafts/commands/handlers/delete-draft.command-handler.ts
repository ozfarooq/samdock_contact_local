import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DraftAggregate } from '../../models/draft.aggregate';
import { DraftDeletedEvent } from '@daypaio/domain-events/contact-drafts';
import { DeleteDraftCommand } from '../impl/delete-draft.command';

@CommandHandler(DeleteDraftCommand)
export class DeleteDraftCommandHandler
  implements ICommandHandler<DeleteDraftCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteDraftCommand) {
    this.logger.log('Deleting draft');
    const { _id, meta } = command;
    const draftAggregate = this.publisher.mergeObjectContext(
      new DraftAggregate({ _id }),
    );
    const event = new DraftDeletedEvent(_id, meta);
    draftAggregate.apply(event);
    draftAggregate.commit();
  }
}
