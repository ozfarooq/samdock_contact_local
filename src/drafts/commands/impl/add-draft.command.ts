import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IContactDraft } from '@daypaio/domain-events/contact-drafts';

export class AddDraftCommand implements ICommand {
  constructor(
    public readonly draft: Partial<IContactDraft>,
    public readonly source: string,
    public readonly meta: EventMetaData,
  ) {}
}
