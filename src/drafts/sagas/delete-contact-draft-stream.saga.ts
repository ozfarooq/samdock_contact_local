import { Injectable } from '@nestjs/common';
import { DeleteStreamSaga } from '../../shared/sagas/delete-stream.saga';
import {
  DraftDeletedEvent,
  DraftVerifiedEvent,
} from '@daypaio/domain-events/contact-drafts';

@Injectable()
export class ContactDraftStreamSaga extends DeleteStreamSaga {
  constructor() {
    super([DraftDeletedEvent, DraftVerifiedEvent], 'contact_drafts');
  }
}
