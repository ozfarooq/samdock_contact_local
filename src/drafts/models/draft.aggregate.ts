import { AggregateRoot } from '@nestjs/cqrs';
import { IContactDraft, DraftDOIGrantedEvent } from '@daypaio/domain-events/contact-drafts';
import { plainToClassFromExist } from 'class-transformer';
import { IConsent } from '@daypaio/domain-events/persons';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DraftAggregate extends AggregateRoot implements IContactDraft {
  _id: string;

  email: string;

  firstName: string;

  lastName: string;

  phoneNumber: string;

  companyName: string;

  city: string;

  source: string;

  consent: IConsent;

  constructor(data?: Partial<IContactDraft>) {
    super();

    plainToClassFromExist(this, data);
  }

  grantDOI(ip: string, email: string, meta: EventMetaData) {
    this.apply(new DraftDOIGrantedEvent(this._id, ip, email, meta));
  }
}
