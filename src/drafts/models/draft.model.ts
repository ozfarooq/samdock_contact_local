import { IContactDraft } from '@daypaio/domain-events/contact-drafts';
import { prop } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Gender, IConsent } from '@daypaio/domain-events/persons';

export class ContactDraft implements IContactDraft {
  @ApiProperty({
    description: 'the related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the contact draft',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiPropertyOptional({
    description: 'The first name of the contact draft',
  })
  @prop()
  firstName: string;

  @ApiPropertyOptional({
    description: 'The last name of the contact draft',
  })
  @prop()
  lastName: string;

  @ApiPropertyOptional({
    description: 'The email of the person of draft',
  })
  @prop()
  email: string;

  @ApiPropertyOptional({
    description: 'The gender of the person of draft',
  })
  @prop()
  gender: Gender;

  @ApiPropertyOptional({
    description: 'The nameSuffix of the person of draft',
  })
  @prop()
  nameSuffix: string;

  @ApiPropertyOptional({
    description: 'The namePrefix of the person of draft',
  })
  @prop()
  namePrefix: string;

  @ApiPropertyOptional({
    description: 'The birthday of the person of draft',
  })
  @prop()
  birthday: string;

  @ApiPropertyOptional({
    description: 'The street of the person of contact draft',
  })
  @prop()
  street: string;

  @ApiPropertyOptional({
    description: 'The number of the person of contact draft',
  })
  @prop()
  number: string;

  @ApiPropertyOptional({
    description: 'The postcode of the person of contact draft',
  })
  @prop()
  postcode: string;

  @ApiPropertyOptional({
    description: 'The city of the person of contact draft',
  })
  @prop()
  city: string;

  @ApiPropertyOptional({
    description: 'The phone number of the person of contact draft',
  })
  @prop()
  phoneNumber: string;

  @ApiPropertyOptional({
    description: 'The mobile number of the person of contact draft',
  })
  @prop()
  mobileNumber: string;

  @ApiPropertyOptional({
    description: 'The office number of the person of contact draft',
  })
  @prop()
  officeNumber: string;

  @ApiPropertyOptional({
    description: 'The home number of the person of contact draft',
  })
  @prop()
  homeNumber: string;

  @ApiPropertyOptional({
    description: 'The private number of the person of contact draft',
  })
  @prop()
  privateNumber: string;

  @ApiPropertyOptional({
    description: 'The fax number of the person of contact draft',
  })
  @prop()
  faxNumber: string;

  @ApiPropertyOptional({
    description: 'The instagram of the person of contact draft',
  })
  @prop()
  instagram: string;

  @ApiPropertyOptional({
    description: 'The linkedin of the person of contact draft',
  })
  @prop()
  linkedin: string;

  @ApiPropertyOptional({
    description: 'The facebook of the person of contact draft',
  })
  @prop()
  facebook: string;

  @ApiPropertyOptional({
    description: 'The twitter of the person of contact draft',
  })
  @prop()
  twitter: string;

  @ApiPropertyOptional({
    description: 'The xing of the person of contact draft',
  })
  @prop()
  xing: string;

  @ApiPropertyOptional({
    description: 'The company name of the contact draft',
  })
  @prop()
  companyName: string;

  @ApiPropertyOptional({
    description: 'The companyEmail name of the contact draft',
  })
  @prop()
  companyEmail: string;

  @ApiPropertyOptional({
    description: 'The tax id of the organization of the contact draft',
  })
  @prop()
  companyTaxID: string;

  @ApiPropertyOptional({
    description: 'The street of the organization of contact draft',
  })
  @prop()
  companyStreet: string;

  @ApiPropertyOptional({
    description: 'The number of the organization of contact draft',
  })
  @prop()
  companyNumber: string;

  @ApiPropertyOptional({
    description: 'The postcode of the organization of contact draft',
  })
  @prop()
  companyPostcode: string;

  @ApiPropertyOptional({
    description: 'The city of the organization of contact draft',
  })
  @prop()
  companyCity: string;

  @ApiPropertyOptional({
    description: 'The phone number name of the organization of the contact draft',
  })
  @prop()
  companyPhoneNumber: string;

  @ApiPropertyOptional({
    description: 'The fax number name of the organization of the contact draft',
  })
  @prop()
  companyFaxNumber: string;

  @ApiPropertyOptional({
    description: 'The website of the organization of the contact draft',
  })
  @prop()
  companyWebsite: string;

  @ApiPropertyOptional({
    description: 'The Instagram of the organization of the contact draft',
  })
  @prop()
  companyInstagram: string;

  @ApiPropertyOptional({
    description: 'The Linkedin of the organization of the contact draft',
  })
  @prop()
  companyLinkedin: string;

  @ApiPropertyOptional({
    description: 'The Facebook of the organization of the contact draft',
  })
  @prop()
  companyFacebook: string;

  @ApiPropertyOptional({
    description: 'The Twitter of the organization of the contact draft',
  })
  @prop()
  companyTwitter: string;

  @ApiPropertyOptional({
    description: 'The Xing of the organization of the contact draft',
  })
  @prop()
  companyXing: string;

  @ApiPropertyOptional({
    description: 'free text field for lead requirements',
  })
  @prop()
  leadRequirements: string;

  @ApiPropertyOptional({
    description: 'The source of the contact draft',
  })
  @prop()
  source: string;

  @ApiPropertyOptional({
    description: 'The created at timestamp of the draft',
  })
  @prop()
  createdAt: number;

  @ApiPropertyOptional({
    description: 'The verified at timestamp of the draft',
  })
  @prop()
  verifiedAt: number;

  @ApiPropertyOptional({
    description: 'Whether the verification of the draft should create a lead',
  })
  @prop()
  createLead: boolean;

  @ApiPropertyOptional({
    description: 'the default editor that will be assigned to the draft',
  })
  @prop()
  editor: string;

  @ApiPropertyOptional({
    description: 'doi consent of the draft',
  })
  @prop()
  consent: IConsent;

  constructor(data?: Partial<ContactDraft>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
