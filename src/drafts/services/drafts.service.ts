import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { BreadService } from '../../shared/services/bread.service';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { AddDraftCommand } from '../commands/impl/add-draft.command';
import { BrowseDraftQuery } from '../queries/impl/browse-draft.query';
import { ReadDraftQuery } from '../queries/impl/read-draft.query';
import { DeleteDraftCommand } from '../commands/impl/delete-draft.command';
import { VerifyDraftCommand } from '../commands/impl/verify-draft.command';
import { ContactDraftDTO } from '../dtos/contact-draft.dto';

@Injectable()
export class DraftsService extends BreadService<ContactDraftDTO> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData) : Promise<ContactDraftDTO[]> {
    return await this.executeQuery(new BrowseDraftQuery(meta));
  }

  async addBySource(draft: Partial<ContactDraftDTO>, source: string, meta: EventMetaData) {
    return await this.executeCommand(new AddDraftCommand(draft, source, meta));
  }

  async add(draft: Partial<ContactDraftDTO>, meta: EventMetaData) {
    return await this.executeCommand({});
  }

  async read(id: string, meta: EventMetaData): Promise<ContactDraftDTO> {
    return await this.executeQuery(new ReadDraftQuery(id, meta));
  }

  async edit(id: string, draft: ContactDraftDTO, meta: EventMetaData) {
    return this.executeCommand({});
  }

  async verify(id: string, createLead: boolean, meta: EventMetaData) {
    return this.executeCommand(new VerifyDraftCommand(id, createLead, meta));
  }

  async delete(id: string, meta: EventMetaData) {
    return await this.executeCommand(new DeleteDraftCommand(id, meta));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeleteDraftCommand(id, meta));
    }));
  }
}
