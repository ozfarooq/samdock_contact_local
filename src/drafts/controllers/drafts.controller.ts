import {
  Controller,
  Logger,
  Body,
  Post,
  Get,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBody,
  ApiForbiddenResponse,
} from '@nestjs/swagger';
import { DraftsService } from '../services/drafts.service';
import { ContactDraft } from '../models/draft.model';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { ContactDraftDTO } from '../dtos/contact-draft.dto';
import { VerifyDraftDTO } from '../dtos/verify-draft.dto';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { v4 } from 'uuid';

@ApiTags('drafts')
@ApiBearerAuth()
@Controller('drafts')
export class DraftsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: DraftsService) {}

  @Get()
  @ApiOperation({
    summary: 'Browse all contact drafts',
    description: 'Get all contact drafts',
  })
  @ApiOkResponse({
    description: 'The contact drafts have successfully been queried',
    type: ContactDraft,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<ContactDraftDTO[]> {
    this.logger.log(`BROWSE user:${user._id}`);
    return await this.service.browse(user.generateMeta());
  }

  @Post(':source')
  @ApiOperation({
    summary: 'Add Draft',
    description: 'Create a single draft',
  })
  @ApiOkResponse({
    description: 'The contact draft adding event has successfully been triggered',
  })
  @ApiForbiddenResponse({ description: 'Cannot post to a disabled sorce' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(@Param('source') source: string, @Body() draft: ContactDraftDTO): Promise<void> {
    draft._id = draft._id || v4();
    this.logger.log(`CREATE id:${draft._id}`);
    await this.service.addBySource(draft, source, new EventMetaData(null, null));
    return;
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Read a contact draft',
    description: 'Get a single contact draft',
  })
  @ApiOkResponse({
    description: 'The contact draft has successfully been found',
    type: ContactDraft,
  })
  @ApiNotFoundResponse({ description: 'The contact draft was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<ContactDraftDTO> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return await this.service.read(id, user.generateMeta());
  }

  @Patch(':id/verify')
  @ApiOperation({
    summary: 'Verify a draft',
    description: 'Verify a draft',
  })
  @ApiBody({
    type: VerifyDraftDTO,
  })
  @ApiOkResponse({
    description: 'The draft verification event has successfully been delivered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async verify(
    @Param('id') id: string,
    @Body() verifyDraft: VerifyDraftDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`VERIFY id:${id} user:${user._id}`);
    await this.service.verify(id, verifyDraft.createLead, user.generateMeta());
  }

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete a draft',
    description: 'Delete a single draft',
  })
  @ApiOkResponse({
    description: 'The draft deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    await this.service.delete(id, user.generateMeta());
  }

}
