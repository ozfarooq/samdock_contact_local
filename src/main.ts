import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupDotenv } from './dotenv.setup';
import { Logger, ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as hbs from 'hbs';

async function bootstrap() {
  const logger = new Logger('bootstrap');

  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const domainName = process.env.DOMAIN?.toLowerCase() || 'contacts';
  app.setGlobalPrefix(`api/${domainName}`);

  const options = new DocumentBuilder()
    .setTitle(domainName)
    .setDescription(`${domainName} Microservice`)
    .setVersion('0.1.0')
    .setBasePath(`/api/${domainName}/`)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`/api/${domainName}/docs`, app, document);
  app.enableCors();

  app.setBaseViewsDir(join(__dirname, 'templates'));
  app.setViewEngine('hbs');
  hbs.handlebars.registerHelper('json', (context: any) => {
    return JSON.stringify(context);
  });

  const port = process.env.PORT || 3000;
  await app.listen(port);

  logger.log(`Application listening on port ${port}`);
}

setupDotenv();
bootstrap();
