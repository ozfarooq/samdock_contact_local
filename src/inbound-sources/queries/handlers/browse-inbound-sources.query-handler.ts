import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { BrowseInBoundSourceQuery } from '../impl/browse-inbound-source.query';
import { Logger } from '@nestjs/common';
import { InboundSourcesRepository } from '../../repositories/inbound-sources.repository';
import { InboundSource } from '../../models/inbound-source.model';

@QueryHandler(BrowseInBoundSourceQuery)
export class BrowseInboundSourcesQueryHandler implements IQueryHandler<BrowseInBoundSourceQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: InboundSourcesRepository) { }

  async execute(query: BrowseInBoundSourceQuery): Promise<InboundSource[]> {
    this.logger.log('Query triggered');
    const { meta } = query;
    try {
      return await this.repo.browse(meta);
    } catch (error) {
      this.logger.error('Failed to browse on inbound sources');
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
