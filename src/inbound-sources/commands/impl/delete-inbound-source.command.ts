import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteInboundSourceCommand implements ICommand {
  constructor(
      public readonly _id: string,
      public readonly meta: EventMetaData,
    ) { }
}
