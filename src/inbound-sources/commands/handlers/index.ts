import { AddInboundSourcesCommandHandler } from './add-inbound-sources.command-handler';
import { DeleteInboundSourcesCommandHandler } from './delete-inbound-sources.command-handler';
import { EditInboundSourcesCommandHandler } from './edit-inbound-sources.command-handler';

export const InboundSourcesCommandHandlers = [
  AddInboundSourcesCommandHandler,
  DeleteInboundSourcesCommandHandler,
  EditInboundSourcesCommandHandler,
];
