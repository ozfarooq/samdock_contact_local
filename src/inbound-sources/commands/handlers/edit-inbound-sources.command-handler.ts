import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger, ForbiddenException } from '@nestjs/common';
import { EditInboudSourceCommand } from '../impl/edit-inbound-source.command';
import { InboundSourceAggregate } from '../../models/inbound-source.aggregate';
import { InboundSourceEditedEvent } from '../../events/impl';
import { IInboundSourceStatus } from '@daypaio/domain-events/inbound-sources';

@CommandHandler(EditInboudSourceCommand)
export class EditInboundSourcesCommandHandler
  implements ICommandHandler<EditInboudSourceCommand> {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: EditInboudSourceCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, inboundSource, meta } = command;

    const allowedStatuses = Object.values(IInboundSourceStatus);
    if (!!inboundSource.status && !allowedStatuses.includes(inboundSource.status)) {
      throw new ForbiddenException(`The status must be an enum of ${JSON.stringify(allowedStatuses)}`);
    }

    const inboundSourceAggregate = this.publisher.mergeObjectContext(
      new InboundSourceAggregate({ _id }),
    );
    inboundSourceAggregate.apply(
      new InboundSourceEditedEvent(
        _id,
        inboundSource,
        meta,
      ),
    );
    inboundSourceAggregate.commit();
  }
}
