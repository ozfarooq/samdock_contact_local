import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddInboundSourceCommand } from '../impl/add-inbound-source.command';
import { Logger, ForbiddenException } from '@nestjs/common';
import { InboundSourceAggregate } from '../../models/inbound-source.aggregate';
import { InboundSourceAddedEvent } from '../../events/impl';
import { IInboundSourceStatus } from '@daypaio/domain-events/inbound-sources';

@CommandHandler(AddInboundSourceCommand)
export class AddInboundSourcesCommandHandler
    implements ICommandHandler<AddInboundSourceCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: AddInboundSourceCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { inboundSource, meta } = command;

    const allowedStatuses = Object.values(IInboundSourceStatus);
    if (!inboundSource.status || !allowedStatuses.includes(inboundSource.status)) {
      throw new ForbiddenException(`The status must be an enum of ${JSON.stringify(allowedStatuses)}`);
    }

    inboundSource.submissionTimestamps = [];

    const inboundSourceAggregate = this.publisher.mergeObjectContext(
      new InboundSourceAggregate({ _id: inboundSource._id }),
    );
    inboundSourceAggregate.apply(
      new InboundSourceAddedEvent(
        inboundSource._id,
        inboundSource,
        meta,
      ),
    );
    inboundSourceAggregate.commit();
  }
}
