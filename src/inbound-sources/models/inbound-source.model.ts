import { prop } from '@typegoose/typegoose';
import { IInboundSource, IInboundSourceStatus, IDOIMailInfo, InboundSourceType, IFormSettings } from '@daypaio/domain-events/inbound-sources';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class InboundSource implements IInboundSource {
  @ApiProperty({
    description: 'the id of the inbound source',
    required: true,
  })
  @prop({ required: true })
  _id: string;

  @ApiProperty({
    description: 'the id of the tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiPropertyOptional({
    description: 'the type of the inbound source(API/Form)',
  })
  @prop()
  type: InboundSourceType;

  @ApiPropertyOptional({
    description: 'the name of the inbound source',
  })
  @prop()
  name: string;

  @ApiPropertyOptional({
    description: 'the status of the inbound source',
  })
  @prop()
  status: IInboundSourceStatus;

  @ApiPropertyOptional({
    description: 'whether verifying the inbound source should create a lead',
  })
  @prop()
  createLead: boolean;

  @ApiPropertyOptional({
    description: 'the default assignee of the lead if a draft is verified',
  })
  @prop()
  editor: string;

  @ApiProperty({
    description: 'the name of the inbound source',
    required: true,
  })
  @prop()
  createdAt: number;

  @prop({ type: Number })
  submissionTimestamps: number[];

  @prop()
  doi: {
    shouldRequest: boolean;
    redirectUrl: string;
    autoVerify: boolean;
    mail: IDOIMailInfo;
  };

  @prop()
  formSettings: IFormSettings;

}
