import { IInboundSource, IInboundSourceStatus, IDOISettings, InboundSourceType, IFormSettings } from '@daypaio/domain-events/inbound-sources';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';

export class InboundSourceAggregate extends AggregateRoot implements IInboundSource {
  _id: string;
  name: string;
  status: IInboundSourceStatus;
  createLead: boolean;
  editor: string;
  doi: IDOISettings;
  type: InboundSourceType;
  formSettings: IFormSettings;

  constructor(data?: Partial<IInboundSource>) {
    super();
    plainToClassFromExist(this, data);
  }
}
