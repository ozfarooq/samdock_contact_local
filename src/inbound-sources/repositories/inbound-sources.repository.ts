import { InboundSource } from '../models/inbound-source.model';
import { BreadRepository } from '../../shared/bread.respository';
import { InjectModel } from 'nestjs-typegoose';
import { NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ReturnModelType } from '@typegoose/typegoose';

export class InboundSourcesRepository extends BreadRepository<InboundSource>{
  constructor(
    @InjectModel(InboundSource) inboundSourcesModel: ReturnModelType<typeof InboundSource>,
  ) {
    super(inboundSourcesModel);
  }

  async readById(id: string): Promise<InboundSource> {
    this.logger.verbose('FIND BY ID ONLY');
    console.table({ id });
    try {
      const result = await this.model.findOne(
        { _id: id },
      ).lean();
      if (result === null) {
        throw new NotFoundException();
      }
      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async updateSubmissionsData(id: string): Promise<void> {
    this.logger.verbose('UPDATE SUBMISSIONS');
    console.table({ id });
    try {
      const result = await this.model.updateOne(
        { _id: id },
        { $push: { submissionTimestamps: Date.now() } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updateSubmissionsData', id);
      this.logger.verbose(message.verbose);
      throw error;
    }

  }

  async resetCounter(id: string, meta: EventMetaData): Promise<void> {
    this.logger.verbose('RESET COUNTER');
    console.table({ id });
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: id },
        { $set: { submissionTimestamps: [] } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'resetCounter', id, null);
      this.logger.verbose(message.verbose);
      throw error;
    }

  }
}
