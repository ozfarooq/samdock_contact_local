import { InboundSourceAddedEvent } from '../impl';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { InboundSourcesRepository } from '../../repositories/inbound-sources.repository';

@EventsHandler(InboundSourceAddedEvent)
export class InboundSourceAddedEventHandler
  implements IEventHandler<InboundSourceAddedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: InboundSourcesRepository) { }

  async handle(event: InboundSourceAddedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, inboundSource, meta } = event;

    try {
      await this.repository.add(
        { ...inboundSource, createdAt: meta.timestamp },
        meta,
      );
    } catch (error) {
      this.logger.error(`Failed to create inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
