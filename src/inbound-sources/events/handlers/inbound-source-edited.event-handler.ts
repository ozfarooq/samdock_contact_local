import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { InboundSourceEditedEvent } from '../impl';
import { Logger } from '@nestjs/common';
import { InboundSourcesRepository } from '../../repositories/inbound-sources.repository';
import { IInboundSourceStatus } from '@daypaio/domain-events/inbound-sources';

@EventsHandler(InboundSourceEditedEvent)
export class InboundSourceEditedEventHandler
  implements IEventHandler<InboundSourceEditedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: InboundSourcesRepository) { }

  async handle(event: InboundSourceEditedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, inboundSource, meta } = event;

    try {
      if (inboundSource?.status === IInboundSourceStatus.ACTIVE) {
        const previousStatus: IInboundSourceStatus = (await this.repository.read(_id, meta)).status;
        if (previousStatus === IInboundSourceStatus.TESTING) {
          await this.repository.resetCounter(_id, meta);
        }
      }

      await this.repository.edit(_id, inboundSource, meta);
    } catch (error) {
      this.logger.error(`Failed to edit inbound source of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
