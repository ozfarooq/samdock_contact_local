import { Module } from '@nestjs/common';
import { OrganizationsController } from './controllers/organizations.controller';
import { OrganizationsService } from './services/organizations.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { OrganizationEventHandlers } from './events/handlers';
import { OrganizationQueryHandlers } from './queries/handlers';
import { OrganizationCommandHandlers } from './commands/handlers';
import { OrganizationRepository } from './repositories/organization.repository';
import { Organization, OrganizationAddress } from './models/organization.model';
import { OrganizationBranchController } from './controllers/branch/organization-branch.controller';
import { OrganizationContactController } from './controllers/contacts/organization-contact.controller';
import { OrganizationSagas } from './sagas';
import { OrganizationActivitiesController } from './controllers/organization-activities/organization-activities.controller';
import { OrganizationActivitiesService } from './services/organization-activities.service';
import { GoogleCloudStorageService } from '../shared/services/google-cloud-storage/google-cloud-storage.service';

@Module({
  imports: [TypegooseModule.forFeature([Organization, OrganizationAddress])],
  controllers: [
    OrganizationsController,
    OrganizationBranchController,
    OrganizationContactController,
    OrganizationActivitiesController,
  ],
  providers: [
    OrganizationsService,
    OrganizationActivitiesService,
    OrganizationRepository,
    GoogleCloudStorageService,
    ...OrganizationQueryHandlers,
    ...OrganizationEventHandlers,
    ...OrganizationSagas,
    ...OrganizationCommandHandlers,
  ],
  exports: [
    OrganizationRepository,
    OrganizationsService,
  ],
})
export class OrganizationsModule {}
