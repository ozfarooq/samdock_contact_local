import { Controller, Post, Param, Patch, Body, Delete } from '@nestjs/common';
import { OrganizationBranchDTO } from './../../dto/organization-branch.dto';
import { OrganizationsService } from './../../services/organizations.service';
import { GetUser, LoggedInUser } from './../../../shared/decorators/get-user.decorator';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('organizations branches')
@ApiBearerAuth()
@Controller('organizations/:organizationID/branches')
export class OrganizationBranchController {

  constructor(private organizationsService:  OrganizationsService) {}

  @Post('')
  addBranchToOrganization(
    @Param('organizationID') organizationID: string,
    @Body() branch: OrganizationBranchDTO,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.addBranch(organizationID, branch, user.generateMeta());
  }

  @Patch(':branchID')
  editBranchOfOrganization(
    @Param('branchID') branchID: string,
    @Param('organizationID') organizationID: string,
    @Body() branch: Partial<OrganizationBranchDTO>,
    @GetUser() user: LoggedInUser,
  ) {
    return this.organizationsService.editBranch(
      organizationID,
      branchID,
      branch,
      user.generateMeta(),
    );
  }

  @Delete(':branchID')
  deleteBranchOfOrganization(
    @Param('branchID') branchID: string,
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
  ) {
    this.organizationsService.deleteBranch(organizationID, branchID, user.generateMeta());
  }
}
