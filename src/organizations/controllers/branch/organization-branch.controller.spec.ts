import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationBranchController } from './organization-branch.controller';
import { OrganizationsService } from './../../services/organizations.service';

const mockOrganizationService = () => ({});

describe('Organization Branch Controller', () => {
  let controller: OrganizationBranchController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrganizationBranchController],
      providers: [
        { provide: OrganizationsService, useValue: mockOrganizationService },
      ],
    }).compile();

    controller = module.get<OrganizationBranchController>(OrganizationBranchController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
