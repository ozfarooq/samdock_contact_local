import { Controller, Get, Param } from '@nestjs/common';
import { GetUser, LoggedInUser } from '../../../shared/decorators/get-user.decorator';
import { OrganizationActivitiesService } from '../../services/organization-activities.service';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('organizations activities')
@ApiBearerAuth()
@Controller('organizations/:id/activities')
export class OrganizationActivitiesController {
  constructor(private readonly service: OrganizationActivitiesService) {}

  @Get()
  browseActivities(
    @GetUser() user: LoggedInUser,
    @Param('id') organizationID: string,
  ) {
    return this.service.browseActivities(organizationID, user.generateMeta());
  }

}
