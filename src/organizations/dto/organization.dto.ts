import {
  IsString,
  IsNumber,
  IsEmail,
  MinLength,
  MaxLength,
  Matches,
  IsAlpha,
  IsOptional,
  ValidateNested,
  IsDefined,
  IsNotEmpty,
  IsUrl,
  IsBoolean,
  IsEmpty,
} from 'class-validator';
import { plainToClassFromExist } from 'class-transformer';
import { OrganizationBranchDTO } from './organization-branch.dto';
import { IOrganizationAddress, IOrganization } from '../events/impl';
import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { EntityCustomFieldErrors } from '../../persons/dtos/person.dto';

export class OrganizationAddressDTO implements IOrganizationAddress {

  @IsOptional()
  @IsString()
  street: string;

  @IsOptional()
  @IsString()
  number: string;

  @IsOptional()
  @IsString()
  postcode: string;

  @IsOptional()
  @IsString()
  city: string;

  @IsOptional()
  @IsString()
  @IsAlpha()
  @MinLength(2)
  @MaxLength(2)
  country: string;

}

export class OrganizationDTO implements IOrganization {

  constructor(data?: any) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsOptional()
  phoneNumber: string;

  @IsString()
  @IsOptional()
  faxNumber: string;

  @IsString()
  @IsOptional()
  linkedin: string;

  @IsString()
  @IsOptional()
  xing: string;

  @IsString()
  @IsOptional()
  twitter: string;

  @IsString()
  @IsOptional()
  facebook: string;

  @IsString()
  @IsOptional()
  instagram: string;

  @IsString()
  @IsOptional()
  vatRegNo: string;

  @IsString()
  @IsUrl()
  @IsOptional()
  website: string;

  @IsString()
  @IsOptional()
  logo: string;

  @IsString()
  @IsOptional()
  importID: string;

  @ValidateNested()
  @IsOptional()
  address: OrganizationAddressDTO;

  @IsOptional()
  @ValidateNested()
  branches: OrganizationBranchDTO[];

  @IsOptional()
  customFields?: EntityCustomFields;

  @IsEmpty()
  @IsOptional()
  customFieldsErrors?: EntityCustomFieldErrors;

  @IsOptional()
  @IsBoolean()
  isCustomer: boolean;

}
