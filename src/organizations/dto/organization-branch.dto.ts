import { plainToClassFromExist } from 'class-transformer';
import { OrganizationAddressDTO } from './organization.dto';
import { IsString, IsNotEmpty, IsDefined, ValidateNested, IsOptional } from 'class-validator';
import { IOrganizationBranch } from '../events/impl';

export class OrganizationBranchDTO implements IOrganizationBranch {

  constructor(data?: Partial<IOrganizationBranch>) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  name: string;

  @ValidateNested()
  @IsOptional()
  address: OrganizationAddressDTO;

}
