import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { FindOrganizationQuery } from '../impl/find-organization.query';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { Organization } from '../../models/organization.model';

@QueryHandler(FindOrganizationQuery)
export class FindOrganizationHandler
  implements IQueryHandler<FindOrganizationQuery> {
  private logger: Logger;
  constructor(private repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(findOrganizationQuery: FindOrganizationQuery): Promise<Organization[]> {
    this.logger.log('QUERY TRIGGERED: FindOrganizationHandler...');
    try {
      const { query, meta } = findOrganizationQuery;
      return await this.repository.find(query, meta);
    } catch (error) {
      this.logger.error(`Failed to find organizations by query ${findOrganizationQuery}`);
      this.logger.log(error.stack);
      throw error;
    }
  }
}
