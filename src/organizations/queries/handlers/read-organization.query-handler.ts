import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadOrganizationQuery } from '../impl/read-organization.query';
import { OrganizationRepository } from '../../repositories/organization.repository';

@QueryHandler(ReadOrganizationQuery)
export class ReadOrganizationHandler
  implements IQueryHandler<ReadOrganizationQuery> {
  private logger: Logger;
  constructor(private repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(query: ReadOrganizationQuery) {
    this.logger.log('QUERY TRIGGERED: ReadOrganizationHandler...');

    const { id, meta } = query;
    try {
      const result = await this.repository.read(id, meta);
      return result;
    } catch (error) {
      this.logger.error(`Failed to read organization of id ${id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      throw error;
    }
  }
}
