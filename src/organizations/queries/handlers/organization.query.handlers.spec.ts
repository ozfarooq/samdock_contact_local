import { TestingModule, Test } from '@nestjs/testing';
import { CqrsModule, QueryBus } from '@nestjs/cqrs';
import { OrganizationQueryHandlers } from '.';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { BrowseOrganizationQuery } from '../impl/browse-organization.query';
import { BrowseOrganizationHandler } from './browse-organization.query-handler';
import { Organization } from '../../models/organization.model';
import { NotFoundException } from '@nestjs/common';
import { ReadOrganizationHandler } from './read-organization.query-handler';
import { ReadOrganizationQuery } from '../impl/read-organization.query';
import { EventMetaData } from '@daypaio/domain-events/shared';

const mockOrganizationRepository = () => ({
  browse: jest.fn(),
  read: jest.fn(),
});

const mockMeta = new EventMetaData('mocktenant', 'mock123');

function createMockOrganizations(): Organization[] {
  const firstOrganization = new Organization({
    _id: 'org1',
    name: 'T',
    email: 't@example.com',
    phoneNumber: '12345',
  });

  const secondOrganization = new Organization({
    _id: 'org2',
    name: 'J',
    email: 'j@example.com',
    phoneNumber: '12345',
  });

  const arrayOfPersons: Organization[] = [];
  arrayOfPersons.push(firstOrganization);
  arrayOfPersons.push(secondOrganization);

  return arrayOfPersons;
}

describe('OrganizationQueryHandlers', () => {
  const mockOrganizations: Organization[] = createMockOrganizations();
  let queryBus: QueryBus;
  let repo;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ...OrganizationQueryHandlers,
        {
          provide: OrganizationRepository,
          useFactory: mockOrganizationRepository,
        },
      ],
    }).compile();
    queryBus = module.get<QueryBus>(QueryBus);
    repo = module.get<OrganizationRepository>(OrganizationRepository);
  });

  describe(' for BrowseQueryHandler', () => {
    it('should successfully return an array of organizations', async () => {
      repo.browse.mockResolvedValue(mockOrganizations);
      queryBus.register([BrowseOrganizationHandler]);
      expect(repo.browse).not.toHaveBeenCalled();
      const result = await queryBus.execute(
        new BrowseOrganizationQuery(mockMeta),
      );
      expect(repo.browse).toHaveBeenCalledWith(mockMeta);
      expect(result).toEqual(mockOrganizations);
    });

    it('should throw InternatServerException on the failure of repository.browse()', async () => {
      repo.browse.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([BrowseOrganizationHandler]);
      expect(repo.browse).not.toHaveBeenCalled();
      expect(
        queryBus.execute(new BrowseOrganizationQuery(mockMeta)),
      ).rejects.toThrow(Error);
      expect(repo.browse).toHaveBeenCalledWith(mockMeta);
    });
  });

  describe(' for ReadQueryHandler', () => {
    it('should successfully return an organization on repo.read()', async () => {
      repo.read.mockResolvedValue(mockOrganizations[0]);
      queryBus.register([ReadOrganizationHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      const result = await queryBus.execute(
        new ReadOrganizationQuery(mockOrganizations[0]._id, mockMeta),
      );
      expect(repo.read).toHaveBeenCalledWith(mockOrganizations[0]._id, mockMeta);
      expect(result).toEqual(mockOrganizations[0]);
    });

    it('should throw NotFoundException on repo.read()', async () => {
      repo.read.mockReturnValue(
        Promise.reject(
          new NotFoundException('This is an auto generated error'),
        ),
      );
      queryBus.register([ReadOrganizationHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(
          new ReadOrganizationQuery(mockOrganizations[0]._id, mockMeta),
        ),
      ).rejects.toThrow(NotFoundException);
      expect(repo.read).toHaveBeenCalledWith(mockOrganizations[0]._id, mockMeta);
    });

    it('should throw InternalServerException on repo.read() failing', async () => {
      repo.read.mockReturnValue(
        Promise.reject(new Error('This is an auto generated error')),
      );
      queryBus.register([ReadOrganizationHandler]);
      expect(repo.read).not.toHaveBeenCalled();
      expect(
        queryBus.execute(
          new ReadOrganizationQuery(mockOrganizations[0]._id, mockMeta),
        ),
      ).rejects.toThrow(Error);
      expect(repo.read).toHaveBeenCalledWith(mockOrganizations[0]._id, mockMeta);
    });
  });
});
