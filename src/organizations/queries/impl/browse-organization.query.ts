import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class BrowseOrganizationQuery implements IQuery {
  constructor(public meta: EventMetaData) {}
}
