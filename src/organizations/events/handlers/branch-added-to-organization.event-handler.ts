import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { BranchAddedToOrganizationEvent } from '../impl';

@EventsHandler(BranchAddedToOrganizationEvent)
export class BranchAddedToOrganizationEventHandler
  implements IEventHandler<BranchAddedToOrganizationEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: BranchAddedToOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, data, meta } = event;
    try {
      await this.repository.addBranch(_organizationID, data, meta);
    } catch (error) {
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
