import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import {
  OrganizationRepository,
} from '../../../organizations/repositories/organization.repository';
import { PersonDeletedEvent } from '@daypaio/domain-events/persons';

@EventsHandler(PersonDeletedEvent)
export class ODeletePersonEventHandler
  implements IEventHandler<PersonDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private organizationRepository: OrganizationRepository,
  ) {}
  async handle(event: PersonDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      this.organizationRepository.unrelatePersonFromAll(_id, meta);
    } catch (error) {
      this.logger.error(`Cannot delete person of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event possibly
    }
  }
}
