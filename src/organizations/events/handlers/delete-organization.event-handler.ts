import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationDeletedEvent } from '../impl';

@EventsHandler(OrganizationDeletedEvent)
export class DeleteOrganizationEventHandler
  implements IEventHandler<OrganizationDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) {}
  async handle(event: OrganizationDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.repository.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete organization of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
