import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationAddedEvent } from '../impl';

@EventsHandler(OrganizationAddedEvent)
export class AddOrganizationEventHandler
  implements IEventHandler<OrganizationAddedEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: OrganizationRepository) {}
  async handle(event: OrganizationAddedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    try {
      await this.repository.add(data, meta);
    } catch (error) {
      this.logger.error(`Failed to create organization of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }
}
