import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from './../../repositories/organization.repository';
import { DealUnrelatedFromOrganizationEvent } from '../impl';

@EventsHandler(DealUnrelatedFromOrganizationEvent)
export class DealUnrelatedFromOrganizationEventHandler
  implements IEventHandler<DealUnrelatedFromOrganizationEvent> {
  private logger: Logger;
  constructor(private readonly repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async handle(event: DealUnrelatedFromOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _dealID, meta } = event;
    try {
      await this.repository.unrelateDeal(_organizationID, _dealID, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
