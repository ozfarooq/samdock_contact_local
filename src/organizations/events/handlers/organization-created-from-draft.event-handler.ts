import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationCreatedFromDraftEvent } from '../impl';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { Organization, OrganizationAddress } from '../../models/organization.model';

@EventsHandler(OrganizationCreatedFromDraftEvent)
export class OrganizationCreatedFromDraftEventHandler
  implements IEventHandler<OrganizationCreatedFromDraftEvent> {
  private logger = new Logger(this.constructor.name);

  constructor(private repository: OrganizationRepository) {}
  async handle(event: OrganizationCreatedFromDraftEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, data, meta } = event;
    const organization = new Organization({ _id, name: data.companyName });
    organization.email = data.companyEmail;
    organization.vatRegNo = data.companyTaxID;
    organization.phoneNumber = data.companyPhoneNumber;
    organization.faxNumber = data.companyFaxNumber;
    organization.website = data.companyWebsite;
    organization.instagram = data.companyInstagram;
    organization.linkedin = data.companyLinkedin;
    organization.facebook = data.companyFacebook;
    organization.twitter = data.companyTwitter;
    organization.xing = data.companyXing;
    organization.createdAt = new Date(meta.timestamp);

    if (
      !!data.companyStreet ||
      !!data.companyNumber ||
      !!data.companyPostcode ||
      !!data.companyCity
    ) {
      organization.address = new OrganizationAddress();
      organization.address.street = data.companyStreet;
      organization.address.number = data.companyNumber;
      organization.address.postcode = data.companyPostcode;
      organization.address.city = data.companyCity;
    }

    try {
      await this.repository.add(organization, meta);
    } catch (error) {
      this.logger.error(`Failed to create organization of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
    }
  }
}
