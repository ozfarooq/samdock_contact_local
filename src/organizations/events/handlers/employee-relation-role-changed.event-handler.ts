import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { EmployeeRelationRoleChangedEvent } from '../impl';

@EventsHandler(EmployeeRelationRoleChangedEvent)
export class EmployeeRelationRoleChangedEventHandler
  implements IEventHandler<EmployeeRelationRoleChangedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(private repository: OrganizationRepository) { }
  async handle(event: EmployeeRelationRoleChangedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID,
      _employeeID,
      role,
      meta } = event;
    try {
      await this.repository.changeEmployeeRelationRole(
        _organizationID,
        _employeeID,
        role,
        meta,
      );
    } catch (error) {
      this.logger.error(`Failed to change role of person ${_employeeID} in ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
