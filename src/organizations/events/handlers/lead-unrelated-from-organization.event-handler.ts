import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from './../../repositories/organization.repository';
import { LeadUnrelatedFromOrganizationEvent } from '../impl';

@EventsHandler(LeadUnrelatedFromOrganizationEvent)
export class LeadUnrelatedFromOrganizationEventHandler
  implements IEventHandler<LeadUnrelatedFromOrganizationEvent> {
  private logger: Logger;
  constructor(private readonly repository: OrganizationRepository) {
    this.logger = new Logger(this.constructor.name);
  }

  async handle(event: LeadUnrelatedFromOrganizationEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name})}`);
    const { _organizationID, _leadID, meta } = event;
    try {
      await this.repository.unrelateDeal(_organizationID, _leadID, meta);
    } catch (error) {
      this.logger.error(`Failed to edit organization of id ${_organizationID}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // Retry event
    }
  }
}
