import {
  OrganizationAddedEvent,
  OrganizationEditedEvent,
  OrganizationDeletedEvent,
  IOrganizationBranch,
  BranchAddedToOrganizationEvent,
  BranchOfOrganizationEditedEvent,
  BranchOfOrganizationDeletedEvent,
  PersonRelatedToOrganizationEvent,
  ContactPersonAssignedEvent,
  PersonUnrelatedFromOrganizationEvent,
  DealRelatedToOrganizationEvent,
  DealUnrelatedFromOrganizationEvent,
  EmployeeRelation,
  OrganizationDeletedByQueryEvent,
} from '@daypaio/domain-events/organizations';

import {
  IOrganization,
  IOrganizationAddress,
} from './organization.interface';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { EntityCustomFields } from '@daypaio/domain-events/custom-fields';
import { IIntegrations } from '@daypaio/domain-events/shared/interfaces/integrations.interface';

export class OrganizationAggregate extends AggregateRoot implements IOrganization {

  _id: string;

  email: string;

  name: string;

  vatRegNo: string;

  phoneNumber: string;

  faxNumber: string;

  linkedin: string;

  xing: string;

  facebook: string;

  twitter: string;

  instagram: string;

  website: string;

  logo: string;

  importID: string;

  address: IOrganizationAddress;

  branches: IOrganizationBranch[];

  deals: string[];

  employees?: EmployeeRelation[];

  createdAt: Date = new Date();

  customFields?: EntityCustomFields;

  isCustomer: boolean;

  integrations?: IIntegrations;

  constructor(data?: Partial<IOrganization>) {
    super();
    plainToClassFromExist(this, data);
  }

  add(meta: EventMetaData) {
    this.apply(new OrganizationAddedEvent(this._id, this, meta));
  }

  edit(meta: EventMetaData) {
    this.apply(new OrganizationEditedEvent(this._id, this, meta));
  }

  delete(meta: EventMetaData) {
    this.apply(new OrganizationDeletedEvent(this._id, meta));
  }

  deleteByQuery(query, meta: EventMetaData) {
    this.apply(new OrganizationDeletedByQueryEvent(query, meta));
  }

  addBranch(branch: IOrganizationBranch, meta: EventMetaData) {
    this.apply(new BranchAddedToOrganizationEvent(this._id, branch, meta));
  }

  editBranch(branchID: string, branch: Partial<IOrganizationBranch>, meta: EventMetaData) {
    this.apply(new BranchOfOrganizationEditedEvent(this._id, branchID, branch, meta));
  }

  deleteBranch(branchID: string, meta: EventMetaData) {
    this.apply(new BranchOfOrganizationDeletedEvent(this._id, branchID, meta));
  }

  relatePerson(personID: string, meta: EventMetaData) {
    this.apply(new PersonRelatedToOrganizationEvent(this._id, personID, meta));
  }

  unrelatePerson(personID: string, meta: EventMetaData) {
    this.apply(new PersonUnrelatedFromOrganizationEvent(this._id, personID, meta));
  }

  assignContactPerson(personID: string, meta: EventMetaData) {
    this.apply(new ContactPersonAssignedEvent(this._id, personID, meta));
  }

  relateDeal(dealID: string, meta: EventMetaData) {
    this.apply(new DealRelatedToOrganizationEvent(this._id, dealID, meta));
  }

  unrelateDeal(dealID: string, meta: EventMetaData) {
    this.apply(new DealUnrelatedFromOrganizationEvent(this._id, dealID, meta));
  }

}
