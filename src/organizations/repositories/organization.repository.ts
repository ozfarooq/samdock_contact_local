import { BreadRepository } from '../../shared/bread.respository';
import { Organization } from '../models/organization.model';
import { InjectModel } from 'nestjs-typegoose';
import { NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { IOrganizationBranch, EmployeeRelation } from '../events/impl';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { Subject } from 'rxjs';
import { DeleteStreamCommand } from '../../shared/commands/impl/delete-stream.command';
import { CommandBus } from '@nestjs/cqrs';
import { ReturnModelType } from '@typegoose/typegoose';

export class OrganizationRepository extends BreadRepository<Organization> {

  public importResult = {
    success$: new Subject(),
    errors$: new Subject(),
  };

  constructor(
    @InjectModel(Organization) organizationModel: ReturnModelType<typeof Organization>,
    private commandBus: CommandBus,
  ) {
    super(organizationModel);
  }

  async addBranch(organizationID: string, branch: IOrganizationBranch, meta: EventMetaData) {
    this.logger.verbose('ADD BRANCH');
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: organizationID },
        { $push: { branches: { $each: [branch], $position: 0 } } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', organizationID, branch);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async editBranch(
    organizationID: string,
    branchID: string,
    branch: Partial<IOrganizationBranch>,
    meta: EventMetaData,
  ) {
    this.logger.verbose('EDIT BRANCH');
    try {
      const update = {};

      if (branch.name) {
        update['branches.$.name'] = branch.name;
      }

      if (branch.address) {
        update['branches.$.address'] = branch.address;
      }

      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: organizationID, 'branches._id': branchID },
        { $set: update },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', organizationID, branch);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async deleteBranch(_organizationID: string, _branchID: string, meta: EventMetaData) {
    this.logger.verbose('DELETE BRANCH');
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: _organizationID },
        { $pull: { branches: { _id: _branchID } } },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'delete', _organizationID, _branchID);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async relatePerson(_organizationID: string, _personID: string, meta: EventMetaData) {
    try {
      const relation: EmployeeRelation = {
        _personID,
        role: null,
      };
      await this.model.updateOne(
        {
          _tenantID: meta._tenantID,
          _id: _organizationID,
          'employees._personID': { $ne: _personID },
        },
        { $push: { employees: relation } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'relatePerson', _organizationID, _personID);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async unrelatePerson(_organizationID: string, _personID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: _organizationID },
        { $pull: { employees: { _personID } } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'unrelatePerson',
        _organizationID,
        _personID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async unrelatePersonFromAll(_personID: string, meta: EventMetaData) {
    try {
      await this.model.updateMany(
        { _tenantID: meta._tenantID },
        { $pull: { employees: { _personID } } },
      );
      await this.model.updateMany(
        { _tenantID: meta._tenantID, contactPerson: _personID },
        { contactPerson: null },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'unrelatePerson',
        _personID,
      );
      this.logger.error(message.verbose);
      throw error;
    }
  }

  async assignContactPerson(_organizationID: string, _personID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: _organizationID },
        { contactPerson: _personID },
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'relatePerson', _organizationID, _personID);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async relateDeal(_organizationID: string, _dealID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: _organizationID, deals: { $ne: _dealID } },
        { $push: { deals: { $each: [_dealID], $position: 0 } } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'relateDeal', _organizationID, _dealID);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async unrelateDeal(_organizationID: string, _dealID: string, meta: EventMetaData) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: _organizationID },
        { $pull: { deals: _dealID } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'relateDeal', _organizationID, _dealID);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseByDeals(_dealID: string, meta: EventMetaData) {
    return this.model.find({
      _tenantID: meta._tenantID,
      deals: _dealID,
    }).lean();
  }

  async find(query, meta: EventMetaData) {
    return this.model.find({
      _tenantID: meta._tenantID,
      ...query,
    }).lean();
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE');
    data._tenantID = meta._tenantID;
    console.table(data);
    try {
      const result = await this.model.create(data);
      if (result.importID) this.importResult.success$.next(result);
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      if (data.importID) this.importResult.errors$.next({ error, importID: data.importID });
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async changeEmployeeRelationRole(
    _organizationID:  string,
    _employeeID: string,
    role: string, meta: EventMetaData,
  ) {
    try {
      await this.model.updateOne(
        { _tenantID: meta._tenantID,
          _id: _organizationID,
          'employees._personID': _employeeID,
        },
        { $set: { 'employees.$.role': role } },
      );
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'changeEmployeeRelationRole',
        _organizationID,
        _employeeID,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async deleteByQuery(query, meta: EventMetaData): Promise<void> {
    this.logger.verbose('DELETE BY QUERY');
    console.table({ query });
    try {
      let ids = await this.model
        .find({ _tenantID: meta._tenantID, ...query })
        .select('_id').lean().exec();

      if (!ids.length) {
        throw new NotFoundException();
      }

      ids = ids.map(item => item._id);
      const result = await this.model.deleteMany({
        _tenantID: meta._tenantID,
        _id: { $in: ids },
      });
      ids.forEach(id => this.commandBus.execute(new DeleteStreamCommand(`organizations-${id}`)));
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        return;
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${this.model.modelName} was found with query: ${query} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'deleteByQuery');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async updateLastActivityTime(id: string, meta: EventMetaData) {
    const query = { _id : id };
    const update = {
      $set: {
        lastActivity: meta.timestamp,
      },
    };

    try {
      await this.model.findOneAndUpdate(query, update);
    } catch (error) {
      this.logger.verbose('Update latest activity failed');
      throw error;
    }
  }

  async edit(id: string, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    console.table({ data, _id: id });
    try {
      let result;
      if (data.customFields) {
        const key = Object.keys(data.customFields)[0];
        const value: any = Object.values(data.customFields)[0];
        const property = `customFields.${key}`;
        delete data.customFields;
        data[property] = value;
        if (!value || value?.length === 0) {
          // remove custom field from object
          result = await this.model.updateOne(
            { _tenantID: meta._tenantID, _id: id },
            { $unset: { [property]: 1 } },
          );
        }
      }

      if (!result) {
        result = await this.model.updateOne(
          { _tenantID: meta._tenantID, _id: id },
          { $set: data },
        );
      }

      if (result === undefined || result === null) {
        throw new InternalServerErrorException(
          `Failed editing model for ${this.model.modelName}: result: ${result}`,
        );
      }

      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, data);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }
}
