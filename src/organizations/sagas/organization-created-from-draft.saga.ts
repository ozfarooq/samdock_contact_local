import { Saga, ICommand } from '@nestjs/cqrs';
import { Observable, Subject } from 'rxjs';
import { DraftVerifiedEvent } from '@daypaio/domain-events/contact-drafts';
import { filter } from 'rxjs/operators';
import { DraftCreatedForOrganizationCommand } from '../commands/impl/draft-created-for-organization.command';
import { OrganizationCreatedFromDraftCommand } from '../commands/impl/organization-created-from-draft.command';
import { AssignContactPersonCommand } from '../commands/impl/assign-contact-person.command';
import { RelatePersonToOrganizationCommand } from '../commands/impl/relate-person-to-organization.command';
import { ContactDraftDTO } from '../../drafts/dtos/contact-draft.dto';

export class OrganizationCreatedFromDraftSaga {

  @Saga()
  organizationCreatedFromDraft = (events$: Observable<any>): Observable<ICommand> => {
    const subject: Subject<ICommand> = new Subject();
    events$.pipe(
      filter(event =>
        event instanceof DraftVerifiedEvent && !!event.data.companyName,
      ),
    ).subscribe((event: DraftVerifiedEvent) => {
      const { _id, data, meta } = event;
      data.verifiedAt = meta.timestamp;
      subject.next(new DraftCreatedForOrganizationCommand(_id, data, {
        _tenantID: meta._tenantID,
        _userID: undefined,
        timestamp: data.createdAt,
      }));
      subject.next(new OrganizationCreatedFromDraftCommand(_id, data, {
        _tenantID: meta._tenantID,
        _userID: meta._userID,
        timestamp: data.verifiedAt,
      }));

      if (!(new ContactDraftDTO(data)).hasAnyPersonField) {

        return subject.asObservable();
      }

      subject.next(new RelatePersonToOrganizationCommand(_id, _id, {
        _tenantID: meta._tenantID,
        _userID: meta._userID,
        timestamp: data.verifiedAt,
      }));
      subject.next(new AssignContactPersonCommand(_id, _id, {
        _tenantID: meta._tenantID,
        _userID: meta._userID,
        timestamp: data.verifiedAt,
      }));
    });

    return subject.asObservable();
  }
}
