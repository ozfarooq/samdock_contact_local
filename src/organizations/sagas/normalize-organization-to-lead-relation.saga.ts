import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganizationRelatedToLeadEvent, OrganizationUnrelatedFromLeadEvent } from '@daypaio/domain-events/deals';
import { RelateLeadToOrganizationCommand } from '../commands/impl/relate-lead-to-organization.command';
import { UnrelateLeadFromOrganizationCommand } from '../commands/impl/unrelate-lead-from-organization.command';

export class NormalizeOrganizationToLeadRelationSaga {
  @Saga()
  relationCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OrganizationRelatedToLeadEvent),
      map(
        (event: OrganizationRelatedToLeadEvent) => {
          const { _leadID, _organizationID, meta } = event;
          return new RelateLeadToOrganizationCommand(_organizationID, _leadID,  meta);
        },
      ),
    );
  }

  @Saga()
  relationDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OrganizationUnrelatedFromLeadEvent),
      map(
        (event: OrganizationUnrelatedFromLeadEvent) => {
          const { _leadID, _organizationID, meta } = event;
          return new UnrelateLeadFromOrganizationCommand(_organizationID, _leadID,  meta);
        },
      ),
    );
  }

}
