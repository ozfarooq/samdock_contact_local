import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeadPromotedToDealEvent, DealDemotedToLeadEvent } from '@daypaio/domain-events/deals';
import { ORelatedLeadPromotedToDealCommand } from '../commands/impl/related-lead-promoted-to-deal.command';
import { ORelatedDealDemotedToLeadCommand } from '../commands/impl/related-deal-demoted-to-lead.command';

export class ORelatedDealPromotedOrDemotedSaga {

  @Saga()
  oLeadPromotedToDeal = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadPromotedToDealEvent),
      map(
        (event: LeadPromotedToDealEvent) => {
          const { _dealID, meta } = event;
          return new ORelatedLeadPromotedToDealCommand(_dealID, meta);
        },
      ),
    );
  }

  @Saga()
  oDealDemotedToLead = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealDemotedToLeadEvent),
      map(
        (event: DealDemotedToLeadEvent) => {
          const { _dealID, meta } = event;
          return new ORelatedDealDemotedToLeadCommand(_dealID, meta);
        },
      ),
    );
  }

}
