import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RelateDealToOrganizationCommand } from '../commands/impl/relate-deal-to-organization.command';
import { UnrelateDealFromOrganizationCommand } from '../commands/impl/unrelate-deal-from-organization.command';
import { OrganizationRelatedToDealEvent, OrganizationUnrelatedFromDealEvent } from '@daypaio/domain-events/deals';

export class NormalizeOrganizationToDealRelationSaga {
  @Saga()
  relationCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OrganizationRelatedToDealEvent),
      map(
        (event: OrganizationRelatedToDealEvent) => {
          const { _dealID, _organizationID, meta } = event;
          return new RelateDealToOrganizationCommand(_organizationID, _dealID,  meta);
        },
      ),
    );
  }

  @Saga()
  relationDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OrganizationUnrelatedFromDealEvent),
      map(
        (event: OrganizationUnrelatedFromDealEvent) => {
          const { _dealID, _organizationID, meta } = event;
          return new UnrelateDealFromOrganizationCommand(_organizationID, _dealID,  meta);
        },
      ),
    );
  }

}
