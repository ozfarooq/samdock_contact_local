import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeadQualifiedEvent } from '@daypaio/domain-events/deals/impl/lead-qualified-event';
import { ORelatedLeadQualifiedCommand } from '../commands/impl/related-lead-qualified.command';

export class ORelatedLeadQualifiedSaga {

  @Saga()
  oRelatedLeadQualified = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadQualifiedEvent),
      map(
        (event: LeadQualifiedEvent) => {
          const { _leadID, meta } = event;
          return new ORelatedLeadQualifiedCommand(_leadID,  meta);
        },
      ),
    );
  }

}
