import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RelatedDealLostCommand } from '../commands/impl/related-deal-lost.command';
import { DealLostEvent } from '@daypaio/domain-events/deals';

export class RelatedDealLostSaga {

  @Saga()
  relatedDealLost = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DealLostEvent),
      map(
        (event: DealLostEvent) => {
          const { _dealID, stage, meta } = event;
          return new RelatedDealLostCommand(_dealID, stage,  meta);
        },
      ),
    );
  }

}
