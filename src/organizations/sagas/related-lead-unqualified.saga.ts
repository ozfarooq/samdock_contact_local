import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeadUnqualifiedEvent } from '@daypaio/domain-events/deals';
import { ORelatedLeadUnqualifiedCommand } from '../commands/impl/related-lead-unqualified.command';

export class ORelatedLeadUnqualifiedSaga {

  @Saga()
  oRelatedLeadUnqualified = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LeadUnqualifiedEvent),
      map(
        (event: LeadUnqualifiedEvent) => {
          const { _dealID, meta } = event;
          return new ORelatedLeadUnqualifiedCommand(_dealID,  meta);
        },
      ),
    );
  }

}
