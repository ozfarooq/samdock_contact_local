import { RelatedDealWonSaga } from './related-deal-won.saga';
import { RelatedDealLostSaga } from './related-deal-lost.saga';
import { NormalizeOrganizationToDealRelationSaga } from './normalize-organization-to-deal-relation.saga';
import { RelatedDealStageChangedSaga } from './related-deal-status-changed.saga';
import { OrganizationCreatedFromDraftSaga } from './organization-created-from-draft.saga';
import { NormalizeOrganizationToLeadRelationSaga } from './normalize-organization-to-lead-relation.saga';
import { ORelatedDealPromotedOrDemotedSaga } from './related-deal-promoted-or-demoted.saga';
import { ORelatedLeadQualifiedSaga } from './related-lead-qualified.saga';
import { ORelatedTaskSaga } from './related-task.saga';
import { ORelatedLeadUnqualifiedSaga } from './related-lead-unqualified.saga';

export const OrganizationSagas = [
  RelatedDealWonSaga,
  RelatedDealLostSaga,
  RelatedDealStageChangedSaga,
  NormalizeOrganizationToDealRelationSaga,
  NormalizeOrganizationToLeadRelationSaga,
  OrganizationCreatedFromDraftSaga,
  ORelatedDealPromotedOrDemotedSaga,
  ORelatedLeadQualifiedSaga,
  ORelatedLeadUnqualifiedSaga,
  ORelatedTaskSaga,
];
