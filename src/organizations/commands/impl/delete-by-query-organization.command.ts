import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteByQueryOrganizationCommand implements ICommand {
  constructor(
    public readonly query,
    public readonly meta: EventMetaData,
  ) {}
}
