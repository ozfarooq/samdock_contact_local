import { ICommand } from '@nestjs/cqrs';
import { IOrganizationBranch } from '@daypaio/domain-events/organizations';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AddBranchToOrganizationCommand implements ICommand {
  constructor(
    public readonly organizationID: string,
    public readonly branch: IOrganizationBranch,
    public readonly meta: EventMetaData,
  ) {}
}
