import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class TaskUnlinkedFromOrganizationCommand implements ICommand {
  constructor(
    public _organizationID: string,
    public _taskID: string,
    public meta: EventMetaData,
  ) { }
}
