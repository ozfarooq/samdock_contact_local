import { ICommand } from '@nestjs/cqrs';
import { IOrganization } from '../../models/organization.interface';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class AddOrganizationCommand implements ICommand {
  constructor(
    public readonly organization: IOrganization,
    public readonly meta: EventMetaData,
  ) {}
}
