import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ChangeEmployeeRelationRoleCommand implements ICommand {
  constructor(
    public _organizationID: string,
    public _employeeID: string,
    public role: string,
    public meta: EventMetaData,
  ) { }
}
