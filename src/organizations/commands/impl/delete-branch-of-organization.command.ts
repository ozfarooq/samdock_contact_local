import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteBranchOfOrganizationCommand implements ICommand {
  constructor(
    public readonly organizationID: string,
    public readonly branchID: string,
    public readonly meta: EventMetaData,
  ) {}
}
