import { ICommand } from '@nestjs/cqrs';
import { IOrganizationBranch } from '@daypaio/domain-events/organizations';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class EditBranchOfOrganizationCommand implements ICommand {
  constructor(
    public readonly organizationID: string,
    public readonly branchID: string,
    public readonly branch: Partial<IOrganizationBranch>,
    public readonly meta: EventMetaData,
  ) {}
}
