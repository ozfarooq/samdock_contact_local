import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EditOrganizationCommand } from '../impl/edit-organization.command';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(EditOrganizationCommand)
export class EditOrganizationCommandHandler
  implements ICommandHandler<EditOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: EditOrganizationCommandHandler...');
    const { id, organization, meta } = command;
    organization._id = id;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate(organization),
    );
    organizationAggregate.edit(meta);
    organizationAggregate.commit();
  }
}
