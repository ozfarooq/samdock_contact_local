import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TaskLinkedToOrganizationCommand }
  from '../impl/task-linked-to-organization.command';
import { TaskLinkedToOrganizationEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(TaskLinkedToOrganizationCommand)
export class TaskLinkedToOrganizationCommandHandler
  implements ICommandHandler<TaskLinkedToOrganizationCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: TaskLinkedToOrganizationCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: TaskLinkedToOrganizationCommandHandler...',
    );
    const { _organizationID, _taskID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.apply(
      new TaskLinkedToOrganizationEvent(_organizationID, _taskID, meta),
    );
    organizationAggregate.commit();
  }
}
