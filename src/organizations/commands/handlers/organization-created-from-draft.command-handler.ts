import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationCreatedFromDraftEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { OrganizationCreatedFromDraftCommand } from '../impl/organization-created-from-draft.command';

@CommandHandler(OrganizationCreatedFromDraftCommand)
export class OrganizationCreatedFromDraftCommandHandler
  implements ICommandHandler<OrganizationCreatedFromDraftCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: OrganizationCreatedFromDraftCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: OrganizationCreatedFromDraftCommandHandler...',
    );
    const { _id, data, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id }),
    );
    organizationAggregate.apply(new OrganizationCreatedFromDraftEvent(_id, data, meta));
    organizationAggregate.commit();
  }
}
