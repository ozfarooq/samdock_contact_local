import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { RelatePersonToOrganizationCommand } from '../impl/relate-person-to-organization.command';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { Organization } from '../../models/organization.model';

@CommandHandler(RelatePersonToOrganizationCommand)
export class RelatePersonToOrganizationCommandHandler
  implements ICommandHandler<RelatePersonToOrganizationCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly publisher: EventPublisher,
    private repo: OrganizationRepository,
  ) { }

  checkFirstLinkedPerson(org: Organization) {
    if (!org?.employees) {
      return true;
    }
    if (org?.employees.length < 1) {
      return true;
    }
    return false;
  }

  async execute(command: RelatePersonToOrganizationCommand) {
    this.logger.log(`Command triggered ${command.constructor.name}`);
    const { _organizationID, _personID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.relatePerson(_personID, meta);

    organizationAggregate.commit();
  }
}
