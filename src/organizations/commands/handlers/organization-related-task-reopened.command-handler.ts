import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRelatedTaskReopenedCommand }
  from '../impl/organization-related-task-reopened.command';
import { OrganizationRelatedTaskReopenedEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(OrganizationRelatedTaskReopenedCommand)
export class OrganizationRelatedTaskReopenedCommandHandler
  implements ICommandHandler<OrganizationRelatedTaskReopenedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: OrganizationRelatedTaskReopenedCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: OrganizationRelatedTaskReopenedCommandHandler...',
    );
    const { _organizationID, _taskID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.apply(
      new OrganizationRelatedTaskReopenedEvent(_organizationID, _taskID, meta),
    );
    organizationAggregate.commit();
  }
}
