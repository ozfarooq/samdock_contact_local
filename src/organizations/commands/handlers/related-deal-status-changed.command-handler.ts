import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { RelatedDealLostCommand } from '../impl/related-deal-lost.command';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationRelatedDealStageChangedEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { RelatedDealStageChangedCommand } from '../impl/related-deal-status-changed.command';

@CommandHandler(RelatedDealStageChangedCommand)
export class RelatedDealStageChangedCommandHandler
  implements ICommandHandler<RelatedDealStageChangedCommand> {
  private logger: Logger;
  constructor(
    private readonly organizationRepository: OrganizationRepository,
    private readonly publisher: EventPublisher,
  ) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelatedDealLostCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _dealID, stage, meta } = command;
    const organizations = await this.organizationRepository.browseByDeals(_dealID, meta);
    for (const organization of organizations) {
      const aggregate = this.publisher.mergeObjectContext(
        new OrganizationAggregate(organization),
      );
      const event = new OrganizationRelatedDealStageChangedEvent(
        organization._id,
        _dealID,
        stage,
        meta,
      );
      aggregate.apply(event);
      aggregate.commit();
    }
  }
}
