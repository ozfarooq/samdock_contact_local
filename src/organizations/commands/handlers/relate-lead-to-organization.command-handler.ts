import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { RelateLeadToOrganizationCommand } from '../impl/relate-lead-to-organization.command';
import { LeadRelatedToOrganizationEvent } from '@daypaio/domain-events/organizations';

@CommandHandler(RelateLeadToOrganizationCommand)
export class RelateLeadToOrganizationCommandHandler
  implements ICommandHandler<RelateLeadToOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: RelateLeadToOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: DealRelatedToOrganizationCommand...');
    const { organizationID, leadID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.apply(new LeadRelatedToOrganizationEvent(organizationID, leadID, meta));
    organizationAggregate.commit();
  }
}
