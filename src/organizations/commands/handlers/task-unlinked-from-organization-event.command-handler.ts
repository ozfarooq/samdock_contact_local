import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TaskUnlinkedFromOrganizationCommand }
  from '../impl/task-unlinked-from-organization.command';
import { TaskUnlinkedFromOrganizationEvent } from '@daypaio/domain-events/organizations';
import { OrganizationAggregate } from '../../models/organization.aggregate';

@CommandHandler(TaskUnlinkedFromOrganizationCommand)
export class TaskUnlinkedFromOrganizationCommandHandler
  implements ICommandHandler<TaskUnlinkedFromOrganizationCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: TaskUnlinkedFromOrganizationCommand) {
    this.logger.log(
      'COMMAND TRIGGERED: TaskUnlinkedFromOrganizationCommandHandler...',
    );
    const { _organizationID, _taskID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.apply(
      new TaskUnlinkedFromOrganizationEvent(_organizationID, _taskID, meta),
    );
    organizationAggregate.commit();
  }
}
