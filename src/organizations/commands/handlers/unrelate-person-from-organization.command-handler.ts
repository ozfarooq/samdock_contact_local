import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { UnrelatePersonFromOrganizationCommand } from '../impl/unrelate-person-from-organization.command';

@CommandHandler(UnrelatePersonFromOrganizationCommand)
export class UnrelatePersonFromOrganizationCommandHandler
  implements ICommandHandler<UnrelatePersonFromOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelatePersonFromOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelatePersonToOrganizationCommand...');
    const { _organizationID, _personID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.unrelatePerson(_personID, meta);
    organizationAggregate.commit();
  }
}
