import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { AddBranchToOrganizationCommand } from '../impl/add-branch-to-organization.command';

@CommandHandler(AddBranchToOrganizationCommand)
export class AddBranchToOrganizationCommandHandler
  implements ICommandHandler<AddBranchToOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: AddBranchToOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: AddBranchToOrganizationCommandHandler...');
    const { organizationID, branch, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.addBranch(branch, meta);
    organizationAggregate.commit();
  }
}
