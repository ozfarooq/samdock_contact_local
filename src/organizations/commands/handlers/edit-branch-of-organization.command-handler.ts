import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { EditBranchOfOrganizationCommand } from '../impl/edit-branch-of-organization.command';

@CommandHandler(EditBranchOfOrganizationCommand)
export class EditBranchOfOrganizationCommandHandler
  implements ICommandHandler<EditBranchOfOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: EditBranchOfOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: EditBranchOfOrganizationCommandHandler...');
    const { organizationID, branchID, branch, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.editBranch(branchID, branch, meta);
    organizationAggregate.commit();
  }
}
