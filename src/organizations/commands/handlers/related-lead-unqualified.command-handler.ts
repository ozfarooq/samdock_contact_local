import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationRepository } from '../../repositories/organization.repository';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { OrganizationRelatedLeadUnqualifiedEvent } from '@daypaio/domain-events/organizations';
import { ORelatedLeadUnqualifiedCommand } from '../impl/related-lead-unqualified.command';

@CommandHandler(ORelatedLeadUnqualifiedCommand)
export class ORelatedLeadUnqualifiedCommandHandler
  implements ICommandHandler<ORelatedLeadUnqualifiedCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(
    private readonly organizationRepository: OrganizationRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: ORelatedLeadUnqualifiedCommand) {
    this.logger.log('COMMAND TRIGGERED pers');
    const { _leadID, meta } = command;
    const organizations = await this.organizationRepository.browseByDeals(_leadID, meta);
    for (const organization of organizations) {
      const aggregate = this.publisher.mergeObjectContext(
        new OrganizationAggregate(organization),
      );
      aggregate.apply(new OrganizationRelatedLeadUnqualifiedEvent(organization._id, _leadID, meta));
      aggregate.commit();
    }
  }
}
