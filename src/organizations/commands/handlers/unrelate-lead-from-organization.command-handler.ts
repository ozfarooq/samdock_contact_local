import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { UnrelateLeadFromOrganizationCommand } from '../impl/unrelate-lead-from-organization.command';
import { LeadUnrelatedFromOrganizationEvent } from '@daypaio/domain-events/organizations';

@CommandHandler(UnrelateLeadFromOrganizationCommand)
export class UnrelateLeadFromOrganizationCommandHandler
  implements ICommandHandler<UnrelateLeadFromOrganizationCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: UnrelateLeadFromOrganizationCommand) {
    this.logger.log('COMMAND TRIGGERED: UnrelateDealFromOrganizationCommand...');
    const { organizationID, leadID, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: organizationID }),
    );
    organizationAggregate.apply(
      new LeadUnrelatedFromOrganizationEvent(organizationID, leadID, meta),
    );
    organizationAggregate.commit();
  }
}
