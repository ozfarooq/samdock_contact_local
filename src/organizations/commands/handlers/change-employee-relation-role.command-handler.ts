import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { OrganizationAggregate } from '../../models/organization.aggregate';
import { ChangeEmployeeRelationRoleCommand } from '../impl/change-employee-relation-role.command';
import { EmployeeRelationRoleChangedEvent } from '@daypaio/domain-events/organizations';

@CommandHandler(ChangeEmployeeRelationRoleCommand)
export class ChangeEmployeeRelationRoleCommandHandler
  implements ICommandHandler<ChangeEmployeeRelationRoleCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: ChangeEmployeeRelationRoleCommand) {
    this.logger.log('COMMAND TRIGGERED: ChangeEmployeeRelationRoleCommand...');
    const { _organizationID, _employeeID, role, meta } = command;
    const organizationAggregate = this.publisher.mergeObjectContext(
      new OrganizationAggregate({ _id: _organizationID }),
    );
    organizationAggregate.apply(
        new EmployeeRelationRoleChangedEvent(
            _organizationID,
            _employeeID,
            role,
            meta,
        ),
    );
    organizationAggregate.commit();
  }
}
