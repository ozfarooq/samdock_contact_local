import { Injectable } from '@nestjs/common';
import { BreadService } from '../../shared/services/bread.service';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { BrowseOrganizationQuery } from '../queries/impl/browse-organization.query';
import { FindOrganizationQuery } from '../queries/impl/find-organization.query';
import { ReadOrganizationQuery } from '../queries/impl/read-organization.query';
import { EditOrganizationCommand } from '../commands/impl/edit-organization.command';
import { AddOrganizationCommand } from '../commands/impl/add-organization.command';
import { DeleteOrganizationCommand } from '../commands/impl/delete-organization.command';
import { DeleteByQueryOrganizationCommand } from '../commands/impl/delete-by-query-organization.command';
import { IOrganization } from '../models/organization.interface';
import {
  AddBranchToOrganizationCommand,
} from '../commands/impl/add-branch-to-organization.command';
import {
  EditBranchOfOrganizationCommand,
} from '../commands/impl/edit-branch-of-organization.command';
import { DeleteBranchOfOrganizationCommand } from '../commands/impl/delete-branch-of-organization.command';
import { RelatePersonToOrganizationCommand } from '../commands/impl/relate-person-to-organization.command';
import { AssignContactPersonCommand } from '../commands/impl/assign-contact-person.command';
import { UnrelatePersonFromOrganizationCommand } from '../commands/impl/unrelate-person-from-organization.command';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IOrganizationBranch } from '../events/impl';
import { ChangeEmployeeRelationRoleCommand } from '../commands/impl/change-employee-relation-role.command';
import { Organization } from '../models/organization.model';

@Injectable()
export class OrganizationsService extends BreadService<IOrganization> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  async browse(meta: EventMetaData): Promise<IOrganization[]> {
    return await this.executeQuery(new BrowseOrganizationQuery(meta));
  }

  async read(id: string, meta: EventMetaData): Promise<IOrganization> {
    return await this.executeQuery(new ReadOrganizationQuery(id, meta));
  }

  async edit(
    id: string,
    object: Partial<IOrganization>,
    meta: EventMetaData,
  ): Promise<void> {
    return await this.executeCommand(
      new EditOrganizationCommand(id, object, meta),
    );
  }

  async add(object: IOrganization, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(
      new AddOrganizationCommand(object, meta),
    );
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteOrganizationCommand(id, meta));
  }

  async bulkDelete(ids: string[], meta: EventMetaData): Promise<void> {
    await Promise.all(ids.map(async (id) => {
      return await this.executeCommand(new DeleteOrganizationCommand(id, meta));
    }));
  }

  async deleteAll(meta: EventMetaData): Promise<void> {
    const items = await this.browse(meta);
    const ids = items.map(item => item._id);
    return this.bulkDelete(ids, meta);
  }

  async addBranch(
    organizationID: string,
    branch: IOrganizationBranch,
    meta: EventMetaData,
  ): Promise<void> {
    return this.executeCommand(new AddBranchToOrganizationCommand(organizationID, branch, meta));
  }

  async editBranch(
    organizationID: string,
    branchID: string,
    branch: Partial<IOrganizationBranch>,
    meta: EventMetaData,
  ): Promise<void> {
    return this.executeCommand(
      new EditBranchOfOrganizationCommand(organizationID, branchID, branch, meta),
    );
  }

  async deleteBranch(
    organizationID: string,
    branchID: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new DeleteBranchOfOrganizationCommand(organizationID, branchID, meta),
    );
  }

  async relatePerson(
    organizationID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new RelatePersonToOrganizationCommand(organizationID, personID, meta),
    );
  }

  async assignContactPerson(
    organizationID: string,
    personID: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new AssignContactPersonCommand(organizationID, personID, meta),
    );
  }

  async unrelatePerson(
      organizationID: string,
      personID: string,
      meta: EventMetaData,
    ) {
    return this.executeCommand(
        new UnrelatePersonFromOrganizationCommand(organizationID, personID, meta),
      );
  }

  async changeRoleOfEmployee(
    organizationID: string,
    employeeID: string,
    role: string,
    meta: EventMetaData,
  ) {
    return this.executeCommand(
      new ChangeEmployeeRelationRoleCommand(organizationID, employeeID, role, meta),
    );
  }

  async find(query, meta: EventMetaData): Promise<Organization[]> {
    return await this.executeQuery(new FindOrganizationQuery(query, meta));
  }

  async deleteByQuery(query, meta: EventMetaData): Promise<void> {
    return await this.executeCommand(new DeleteByQueryOrganizationCommand(query, meta));
  }

}
