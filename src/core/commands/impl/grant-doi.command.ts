import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class GrantDOICommand implements ICommand {
  constructor(
    public readonly _entityID: string,
    public readonly redirectURL: string,
    public readonly verify: boolean,
    public readonly ip: string,
    public readonly email: string,
    public readonly meta: EventMetaData,
  ) { }
}
