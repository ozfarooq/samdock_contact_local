import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ContactViewEntity } from '@daypaio/domain-events/contact-views';

export class DeleteContactViewCommand implements ICommand {
  constructor(
      public readonly _id: string,
      public readonly meta: EventMetaData,
    ) { }
}
