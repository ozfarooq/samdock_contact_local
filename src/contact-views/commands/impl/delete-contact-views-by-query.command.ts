import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteContactViewsByQueryCommand implements ICommand {
  constructor(
    public readonly query: any,
    public readonly meta: EventMetaData,
  ) {}
}
