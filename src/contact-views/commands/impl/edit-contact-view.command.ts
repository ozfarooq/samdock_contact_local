import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IContactView } from '@daypaio/domain-events/contact-views';

export class EditContactViewCommand implements ICommand {
  constructor(
    public readonly _id: string,
    public readonly contactView: Partial<IContactView>,
    public readonly meta: EventMetaData,
  ) { }
}
