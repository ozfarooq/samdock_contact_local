import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EditContactViewCommand } from '../impl/edit-contact-view.command';
import { ContactViewAggregate } from '../../models/contact-view.aggregate';
import { ContactViewEditedEvent } from '@daypaio/domain-events/contact-views';

@CommandHandler(EditContactViewCommand)
export class EditContactViewsCommandHandler
  implements ICommandHandler<EditContactViewCommand> {

  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: EditContactViewCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, contactView, meta } = command;

    const contactViewAggregate = this.publisher.mergeObjectContext(
      new ContactViewAggregate({ _id }),
    );
    contactViewAggregate.apply(
      new ContactViewEditedEvent(
        _id,
        contactView,
        meta,
      ),
    );
    contactViewAggregate.commit();
  }
}
