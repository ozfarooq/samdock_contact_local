import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewAggregate } from '../../models/contact-view.aggregate';
import { AddContactViewCommand } from '../impl/add-contact-view.command';
import { ContactViewAddedEvent } from '../../events/impl';

@CommandHandler(AddContactViewCommand)
export class AddContactViewsCommandHandler
    implements ICommandHandler<AddContactViewCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) {}

  async execute(command: AddContactViewCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, contactView, meta } = command;

    const contactViewAggregate = this.publisher.mergeObjectContext(
      new ContactViewAggregate({ _id: contactView._id }),
    );
    contactViewAggregate.apply(
      new ContactViewAddedEvent(
        _id,
        contactView,
        meta,
      ),
    );
    contactViewAggregate.commit();
  }
}
