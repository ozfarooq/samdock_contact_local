import { AddContactViewsCommandHandler } from './add-contact-views.command-handler';
import { DeleteContactViewsByQueryCommandHandler } from './delete-contact-views-by-query.command-handler';
import { DeleteContactViewsCommandHandler } from './delete-contact-views.command-handler';
import { EditContactViewsCommandHandler } from './edit-contact-views.command-handler';

export const ContactViewsCommandHandlers = [
  AddContactViewsCommandHandler,
  DeleteContactViewsCommandHandler,
  EditContactViewsCommandHandler,
  DeleteContactViewsByQueryCommandHandler,
];
