import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewAggregate } from '../../models/contact-view.aggregate';
import { DeleteContactViewCommand } from '../impl/delete-contact-view.command';
import { ContactViewDeletedEvent } from '@daypaio/domain-events/contact-views';

@CommandHandler(DeleteContactViewCommand)
export class DeleteContactViewsCommandHandler
  implements ICommandHandler<DeleteContactViewCommand> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private readonly publisher: EventPublisher) { }

  async execute(command: DeleteContactViewCommand) {
    this.logger.log('COMMAND TRIGGERED');
    const { _id, meta } = command;
    const contactViewAggregate = this.publisher.mergeObjectContext(
      new ContactViewAggregate({ _id }),
    );
    contactViewAggregate.apply(
      new ContactViewDeletedEvent(
        _id,
        meta,
      ),
    );
    contactViewAggregate.commit();
  }
}
