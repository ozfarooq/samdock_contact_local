import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { EventPublisher } from 'nestjs-eventstore';
import { ContactViewAggregate } from '../../models/contact-view.aggregate';
import { DeleteContactViewsByQueryCommand } from '../impl/delete-contact-views-by-query.command';

@CommandHandler(DeleteContactViewsByQueryCommand)
export class DeleteContactViewsByQueryCommandHandler
  implements ICommandHandler<DeleteContactViewsByQueryCommand> {
  private logger: Logger;
  constructor(private readonly publisher: EventPublisher) {
    this.logger = new Logger(this.constructor.name);
  }

  async execute(command: DeleteContactViewsByQueryCommand) {
    this.logger.log('COMMAND TRIGGERED: DeleteContactViewsByQuery...');
    const { query, meta } = command;
    const contactViewAggregate = this.publisher.mergeObjectContext(
      new ContactViewAggregate(),
    );
    contactViewAggregate.deleteByQuery(query, meta);
    contactViewAggregate.commit();
  }
}
