import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserAddedEvent } from '@daypaio/domain-events/users';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ContactViewsService } from '../../services/contact-views.service';

@EventsHandler(UserAddedEvent)
export class UserAddedEventHandler
  implements IEventHandler<UserAddedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private contactViewsService: ContactViewsService,
  ) { }
  async handle(event: UserAddedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, user, meta } = event;
    try {
      await this.contactViewsService.addDefaults(user, meta);
    } catch (error) {
      this.logger.error(`Failed to create default views for user with ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
