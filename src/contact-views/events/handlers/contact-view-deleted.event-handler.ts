import { ContactViewDeletedEvent } from '../impl';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ContactViewsRepository } from '../../repositories/contact-views.repository';

@EventsHandler(ContactViewDeletedEvent)
export class ContactViewDeletedEventHandler
  implements IEventHandler<ContactViewDeletedEvent> {

  private logger = new Logger(this.constructor.name);

  constructor(private repository: ContactViewsRepository) { }

  async handle(event: ContactViewDeletedEvent): Promise<void> {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;
    try {
      await this.repository.delete(_id, meta);
    } catch (error) {
      this.logger.error(`Failed to delete contact view of id: ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO: retry event
    }
  }

}
