import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { ContactViewsController } from './controllers/contact-views.controller';
import { ContactViewsEventHandlers } from './events/handlers';
import { ContactViewsQueryHandlers } from './queries/handlers';
import { ContactViewsRepository } from './repositories/contact-views.repository';
import { ContactViewsService } from './services/contact-views.service';
import { ContactViewsCommandHandlers } from './commands/handlers';
import { ContactView } from './models/contact-view.model';
import { ContactViewsSaga } from './sagas';

@Module({
  imports: [
    TypegooseModule.forFeature([
      {
        typegooseClass: ContactView,
        schemaOptions: {
          collection: 'contact-views',
        },
      },
    ]),
  ],
  controllers: [
    ContactViewsController,
  ],
  providers: [
    ContactViewsService,
    ContactViewsRepository,
    ...ContactViewsCommandHandlers,
    ...ContactViewsQueryHandlers,
    ...ContactViewsEventHandlers,
    ...ContactViewsSaga,
  ],
  exports: [
    ContactViewsRepository,
    ContactViewsService,
  ],
})
export class ContactViewsModule { }
