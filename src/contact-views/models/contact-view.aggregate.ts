import { IContactView, ContactViewEntity, ContactViewsDeletedByQueryEvent } from '@daypaio/domain-events/contact-views';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';

export class ContactViewAggregate extends AggregateRoot implements IContactView {
  _id: string;
  _tenantID: string;
  _userID: string;
  name: string;
  isShared: boolean;
  isLocked: boolean;
  entity: ContactViewEntity;
  filters?: any;
  columns?: any;

  constructor(data?: Partial<IContactView>) {
    super();
    plainToClassFromExist(this, data);
  }

  deleteByQuery(query, meta: EventMetaData) {
    this.apply(new ContactViewsDeletedByQueryEvent(query, meta));
  }
}
