import { BrowseContactViewsQueryHandler } from './browse-contact-views.query-handler';
import { ReadContactViewsQueryHandler } from './read-contact-views.query-handler';

export const ContactViewsQueryHandlers = [
  BrowseContactViewsQueryHandler,
  ReadContactViewsQueryHandler,
];
