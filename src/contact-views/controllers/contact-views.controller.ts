import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { Controller, Logger, Get, Param, Patch, Body, Post, Delete } from '@nestjs/common';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { AddEditContactViewDTO, ContactViewDTO } from '../dtos/contact-view.dto';
import { ContactViewsService } from '../services/contact-views.service';
import { IContactView } from '@daypaio/domain-events/contact-views';
import { BreadController } from '../../shared/controllers/bread.controller';

@ApiTags('contact views')
@ApiBearerAuth()
@Controller('contact-views')
export class ContactViewsController implements BreadController<IContactView> {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: ContactViewsService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse Contact views',
    description: 'Get all contact views',
  })
  @ApiOkResponse({
    description: 'The records have successfully been queried',
    type: ContactViewDTO,
    isArray: true,
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async browse(@GetUser() user: LoggedInUser): Promise<ContactViewDTO[]> {
    this.logger.log(`BROWSE user:${user._id}`);
    return this.service.browse(user.generateMeta());
  }

  @Post()
  @ApiBody({ type: AddEditContactViewDTO })
  @ApiOperation({
    summary: 'Add a Contact View',
    description: 'Create a contact view',
  })
  @ApiOkResponse({
    description:
      'The Contact view adding event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async add(
    @Body() contactView: AddEditContactViewDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`CREATE id:${contactView._id} user:${user._id}`);
    return this.service.add(contactView, user.generateMeta());
  }

  @Get('/:id')
  @ApiOperation({
    summary: 'Read an contact view',
    description: 'Get an contact view by id',
  })
  @ApiOkResponse({
    description: 'The contact view has successfully been found',
    type: ContactViewDTO,
  })
  @ApiNotFoundResponse({ description: 'The contact view was not found' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<ContactViewDTO> {
    this.logger.log(`READ id:${id} user:${user._id}`);
    return this.service.read(id, user.generateMeta());
  }

  @Patch('/:id')
  @ApiBody({
    type: AddEditContactViewDTO,
  })
  @ApiOperation({
    summary: 'Edit contact view',
    description: 'Edit an contact view, id is not required in the body',
  })
  @ApiOkResponse({
    description:
      'The contact view editing event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async edit(
    @Param('id') id: string,
    @Body() contactView: Partial<AddEditContactViewDTO>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`EDIT id:${id} user:${user._id}`);
    return this.service.edit(id, contactView, user.generateMeta());
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete a contact view',
    description: 'Delete a contact view',
  })
  @ApiOkResponse({
    description:
      'The contact view deleting event has successfully been triggered',
  })
  @ApiUnauthorizedResponse({ description: 'Unauthorized error' })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  async delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log(`DELETE id:${id} user:${user._id}`);
    return this.service.delete(id, user.generateMeta());
  }

}
