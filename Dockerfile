FROM node:14-alpine

WORKDIR /var/www

COPY . .

CMD ["npm", "run", "start:prod"]
